(ns cognitect.aws.s3.specs2)

(in-ns 'cognitect.aws.s3.specs)

(s/def
  :cognitect.aws.s3/TargetGrant
  (s/keys :opt-un [:cognitect.aws.s3.TargetGrant/Permission :cognitect.aws.s3.TargetGrant/Grantee]))

(s/def
  :cognitect.aws.s3/ReplicationRuleAndOperator
  (s/keys
    :opt-un
    [:cognitect.aws.s3.ReplicationRuleAndOperator/Prefix
     :cognitect.aws.s3.ReplicationRuleAndOperator/Tags]))

(s/def :cognitect.aws.s3/MissingMeta (s/spec int? :gen #(gen/choose Long/MIN_VALUE Long/MAX_VALUE)))

(s/def :cognitect.aws.s3/ObjectLockLegalHoldStatus (s/spec string? :gen #(s/gen #{"ON" "OFF"})))

(s/def :cognitect.aws.s3/AllowedHeader string?)

(s/def :cognitect.aws.s3/Parts (s/coll-of :cognitect.aws.s3/Part))

(s/def
  :cognitect.aws.s3/ListObjectVersionsOutput
  (s/keys
    :opt-un
    [:cognitect.aws.s3.ListObjectVersionsOutput/Versions
     :cognitect.aws.s3.ListObjectVersionsOutput/Prefix
     :cognitect.aws.s3.ListObjectVersionsOutput/EncodingType
     :cognitect.aws.s3.ListObjectVersionsOutput/NextKeyMarker
     :cognitect.aws.s3.ListObjectVersionsOutput/IsTruncated
     :cognitect.aws.s3.ListObjectVersionsOutput/CommonPrefixes
     :cognitect.aws.s3.ListObjectVersionsOutput/VersionIdMarker
     :cognitect.aws.s3.ListObjectVersionsOutput/NextVersionIdMarker
     :cognitect.aws.s3.ListObjectVersionsOutput/Delimiter
     :cognitect.aws.s3.ListObjectVersionsOutput/Name
     :cognitect.aws.s3.ListObjectVersionsOutput/MaxKeys
     :cognitect.aws.s3.ListObjectVersionsOutput/KeyMarker
     :cognitect.aws.s3.ListObjectVersionsOutput/DeleteMarkers]))

(s/def :cognitect.aws.s3/CopySourceVersionId string?)

(s/def
  :cognitect.aws.s3/PutBucketWebsiteRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.PutBucketWebsiteRequest/Bucket
     :cognitect.aws.s3.PutBucketWebsiteRequest/WebsiteConfiguration]
    :opt-un
    [:cognitect.aws.s3.PutBucketWebsiteRequest/ContentMD5]))

(s/def
  :cognitect.aws.s3/WebsiteConfiguration
  (s/keys
    :opt-un
    [:cognitect.aws.s3.WebsiteConfiguration/RoutingRules
     :cognitect.aws.s3.WebsiteConfiguration/ErrorDocument
     :cognitect.aws.s3.WebsiteConfiguration/RedirectAllRequestsTo
     :cognitect.aws.s3.WebsiteConfiguration/IndexDocument]))

(s/def :cognitect.aws.s3/CopySourceRange string?)

(s/def
  :cognitect.aws.s3/GetBucketInventoryConfigurationOutput
  (s/keys :opt-un [:cognitect.aws.s3.GetBucketInventoryConfigurationOutput/InventoryConfiguration]))

(s/def
  :cognitect.aws.s3/Redirect
  (s/keys
    :opt-un
    [:cognitect.aws.s3.Redirect/ReplaceKeyPrefixWith
     :cognitect.aws.s3.Redirect/Protocol
     :cognitect.aws.s3.Redirect/ReplaceKeyWith
     :cognitect.aws.s3.Redirect/HttpRedirectCode
     :cognitect.aws.s3.Redirect/HostName]))

(s/def
  :cognitect.aws.s3/GetBucketAclRequest
  (s/keys :req-un [:cognitect.aws.s3.GetBucketAclRequest/Bucket]))

(s/def
  :cognitect.aws.s3/StorageClass
  (s/spec
    string?
    :gen
    #(s/gen
      #{"DEEP_ARCHIVE" "GLACIER" "ONEZONE_IA" "STANDARD_IA" "STANDARD" "INTELLIGENT_TIERING"
        "REDUCED_REDUNDANCY"})))

(s/def :cognitect.aws.s3/Start (s/spec int? :gen #(gen/choose Long/MIN_VALUE Long/MAX_VALUE)))

(s/def
  :cognitect.aws.s3/GetBucketAccelerateConfigurationOutput
  (s/keys :opt-un [:cognitect.aws.s3.GetBucketAccelerateConfigurationOutput/Status]))

(s/def :cognitect.aws.s3/VersionIdMarker string?)

(s/def :cognitect.aws.s3/Setting boolean?)

(s/def
  :cognitect.aws.s3/DeleteObjectOutput
  (s/keys
    :opt-un
    [:cognitect.aws.s3.DeleteObjectOutput/VersionId
     :cognitect.aws.s3.DeleteObjectOutput/DeleteMarker
     :cognitect.aws.s3.DeleteObjectOutput/RequestCharged]))

(s/def :cognitect.aws.s3/ParquetInput (s/keys))

(s/def :cognitect.aws.s3/ResponseContentDisposition string?)

(s/def :cognitect.aws.s3/CreationDate inst?)

(s/def
  :cognitect.aws.s3/ServerSideEncryptionConfiguration
  (s/keys :req-un [:cognitect.aws.s3.ServerSideEncryptionConfiguration/Rules]))

(s/def
  :cognitect.aws.s3/InventoryEncryption
  (s/keys
    :opt-un
    [:cognitect.aws.s3.InventoryEncryption/SSEKMS :cognitect.aws.s3.InventoryEncryption/SSES3]))

(s/def :cognitect.aws.s3/TagCount (s/spec int? :gen #(gen/choose Long/MIN_VALUE Long/MAX_VALUE)))

(s/def :cognitect.aws.s3/MultipartUploadList (s/coll-of :cognitect.aws.s3/MultipartUpload))

(s/def :cognitect.aws.s3/RecordsEvent (s/keys :opt-un [:cognitect.aws.s3.RecordsEvent/Payload]))

(s/def
  :cognitect.aws.s3/GetObjectAclOutput
  (s/keys
    :opt-un
    [:cognitect.aws.s3.GetObjectAclOutput/Grants
     :cognitect.aws.s3.GetObjectAclOutput/Owner
     :cognitect.aws.s3.GetObjectAclOutput/RequestCharged]))

(s/def :cognitect.aws.s3/FilterRuleValue string?)

(s/def
  :cognitect.aws.s3/GetBucketLocationRequest
  (s/keys :req-un [:cognitect.aws.s3.GetBucketLocationRequest/Bucket]))

(s/def
  :cognitect.aws.s3/GetBucketRequestPaymentRequest
  (s/keys :req-un [:cognitect.aws.s3.GetBucketRequestPaymentRequest/Bucket]))

(s/def
  :cognitect.aws.s3/GetBucketTaggingOutput
  (s/keys :req-un [:cognitect.aws.s3.GetBucketTaggingOutput/TagSet]))

(s/def
  :cognitect.aws.s3/ListBucketInventoryConfigurationsRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.ListBucketInventoryConfigurationsRequest/Bucket]
    :opt-un
    [:cognitect.aws.s3.ListBucketInventoryConfigurationsRequest/ContinuationToken]))

(s/def :cognitect.aws.s3/Expiration string?)

(s/def
  :cognitect.aws.s3/PutObjectAclOutput
  (s/keys :opt-un [:cognitect.aws.s3.PutObjectAclOutput/RequestCharged]))

(s/def
  :cognitect.aws.s3/AnalyticsFilter
  (s/keys
    :opt-un
    [:cognitect.aws.s3.AnalyticsFilter/And
     :cognitect.aws.s3.AnalyticsFilter/Prefix
     :cognitect.aws.s3.AnalyticsFilter/Tag]))

(s/def
  :cognitect.aws.s3/Part
  (s/keys
    :opt-un
    [:cognitect.aws.s3.Part/PartNumber
     :cognitect.aws.s3.Part/ETag
     :cognitect.aws.s3.Part/LastModified
     :cognitect.aws.s3.Part/Size]))

(s/def
  :cognitect.aws.s3/ReplicationTime
  (s/keys
    :req-un
    [:cognitect.aws.s3.ReplicationTime/Status :cognitect.aws.s3.ReplicationTime/Time]))

(s/def :cognitect.aws.s3/InventoryFormat (s/spec string? :gen #(s/gen #{"CSV" "ORC" "Parquet"})))

(s/def :cognitect.aws.s3/QueueConfigurationList (s/coll-of :cognitect.aws.s3/QueueConfiguration))

(s/def :cognitect.aws.s3/Policy string?)

(s/def :cognitect.aws.s3/AbortRuleId string?)

(s/def
  :cognitect.aws.s3/RequestProgress
  (s/keys :opt-un [:cognitect.aws.s3.RequestProgress/Enabled]))

(s/def :cognitect.aws.s3/Expires inst?)

(s/def
  :cognitect.aws.s3/GetBucketLifecycleConfigurationOutput
  (s/keys :opt-un [:cognitect.aws.s3.GetBucketLifecycleConfigurationOutput/Rules]))

(s/def
  :cognitect.aws.s3/ListMultipartUploadsOutput
  (s/keys
    :opt-un
    [:cognitect.aws.s3.ListMultipartUploadsOutput/Bucket
     :cognitect.aws.s3.ListMultipartUploadsOutput/Uploads
     :cognitect.aws.s3.ListMultipartUploadsOutput/Prefix
     :cognitect.aws.s3.ListMultipartUploadsOutput/EncodingType
     :cognitect.aws.s3.ListMultipartUploadsOutput/NextKeyMarker
     :cognitect.aws.s3.ListMultipartUploadsOutput/UploadIdMarker
     :cognitect.aws.s3.ListMultipartUploadsOutput/MaxUploads
     :cognitect.aws.s3.ListMultipartUploadsOutput/IsTruncated
     :cognitect.aws.s3.ListMultipartUploadsOutput/CommonPrefixes
     :cognitect.aws.s3.ListMultipartUploadsOutput/Delimiter
     :cognitect.aws.s3.ListMultipartUploadsOutput/KeyMarker
     :cognitect.aws.s3.ListMultipartUploadsOutput/NextUploadIdMarker]))

(s/def
  :cognitect.aws.s3/DeleteBucketInventoryConfigurationRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.DeleteBucketInventoryConfigurationRequest/Bucket
     :cognitect.aws.s3.DeleteBucketInventoryConfigurationRequest/Id]))

(s/def :cognitect.aws.s3.JSONInput/Type :cognitect.aws.s3/JSONType)

(s/def :cognitect.aws.s3.CompleteMultipartUploadRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.CompleteMultipartUploadRequest/Key :cognitect.aws.s3/ObjectKey)

(s/def
  :cognitect.aws.s3.CompleteMultipartUploadRequest/MultipartUpload
  :cognitect.aws.s3/CompletedMultipartUpload)

(s/def
  :cognitect.aws.s3.CompleteMultipartUploadRequest/UploadId
  :cognitect.aws.s3/MultipartUploadId)

(s/def :cognitect.aws.s3.CompleteMultipartUploadRequest/RequestPayer :cognitect.aws.s3/RequestPayer)

(s/def :cognitect.aws.s3.ObjectLockRetention/Mode :cognitect.aws.s3/ObjectLockRetentionMode)

(s/def :cognitect.aws.s3.ObjectLockRetention/RetainUntilDate :cognitect.aws.s3/Date)

(s/def :cognitect.aws.s3.ReplicationRuleFilter/Prefix :cognitect.aws.s3/Prefix)

(s/def :cognitect.aws.s3.ReplicationRuleFilter/Tag :cognitect.aws.s3/Tag)

(s/def :cognitect.aws.s3.ReplicationRuleFilter/And :cognitect.aws.s3/ReplicationRuleAndOperator)

(s/def :cognitect.aws.s3.PutObjectTaggingOutput/VersionId :cognitect.aws.s3/ObjectVersionId)

(s/def :cognitect.aws.s3.PutPublicAccessBlockRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.PutPublicAccessBlockRequest/ContentMD5 :cognitect.aws.s3/ContentMD5)

(s/def
  :cognitect.aws.s3.PutPublicAccessBlockRequest/PublicAccessBlockConfiguration
  :cognitect.aws.s3/PublicAccessBlockConfiguration)

(s/def :cognitect.aws.s3.DeleteBucketTaggingRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.PutBucketRequestPaymentRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.PutBucketRequestPaymentRequest/ContentMD5 :cognitect.aws.s3/ContentMD5)

(s/def
  :cognitect.aws.s3.PutBucketRequestPaymentRequest/RequestPaymentConfiguration
  :cognitect.aws.s3/RequestPaymentConfiguration)

(s/def
  :cognitect.aws.s3.SourceSelectionCriteria/SseKmsEncryptedObjects
  :cognitect.aws.s3/SseKmsEncryptedObjects)

(s/def
  :cognitect.aws.s3.ListBucketAnalyticsConfigurationsRequest/Bucket
  :cognitect.aws.s3/BucketName)

(s/def
  :cognitect.aws.s3.ListBucketAnalyticsConfigurationsRequest/ContinuationToken
  :cognitect.aws.s3/Token)

(s/def :cognitect.aws.s3.PutBucketTaggingRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.PutBucketTaggingRequest/ContentMD5 :cognitect.aws.s3/ContentMD5)

(s/def :cognitect.aws.s3.PutBucketTaggingRequest/Tagging :cognitect.aws.s3/Tagging)

(s/def :cognitect.aws.s3.RestoreObjectRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.RestoreObjectRequest/Key :cognitect.aws.s3/ObjectKey)

(s/def :cognitect.aws.s3.RestoreObjectRequest/VersionId :cognitect.aws.s3/ObjectVersionId)

(s/def :cognitect.aws.s3.RestoreObjectRequest/RestoreRequest :cognitect.aws.s3/RestoreRequest)

(s/def :cognitect.aws.s3.RestoreObjectRequest/RequestPayer :cognitect.aws.s3/RequestPayer)

(s/def :cognitect.aws.s3.LambdaFunctionConfiguration/Id :cognitect.aws.s3/NotificationId)

(s/def
  :cognitect.aws.s3.LambdaFunctionConfiguration/LambdaFunctionArn
  :cognitect.aws.s3/LambdaFunctionArn)

(s/def :cognitect.aws.s3.LambdaFunctionConfiguration/Events :cognitect.aws.s3/EventList)

(s/def
  :cognitect.aws.s3.LambdaFunctionConfiguration/Filter
  :cognitect.aws.s3/NotificationConfigurationFilter)

(s/def :cognitect.aws.s3.TopicConfigurationDeprecated/Id :cognitect.aws.s3/NotificationId)

(s/def :cognitect.aws.s3.TopicConfigurationDeprecated/Events :cognitect.aws.s3/EventList)

(s/def :cognitect.aws.s3.TopicConfigurationDeprecated/Event :cognitect.aws.s3/Event)

(s/def :cognitect.aws.s3.TopicConfigurationDeprecated/Topic :cognitect.aws.s3/TopicArn)

(s/def :cognitect.aws.s3.CORSRule/AllowedHeaders :cognitect.aws.s3/AllowedHeaders)

(s/def :cognitect.aws.s3.CORSRule/AllowedMethods :cognitect.aws.s3/AllowedMethods)

(s/def :cognitect.aws.s3.CORSRule/AllowedOrigins :cognitect.aws.s3/AllowedOrigins)

(s/def :cognitect.aws.s3.CORSRule/ExposeHeaders :cognitect.aws.s3/ExposeHeaders)

(s/def :cognitect.aws.s3.CORSRule/MaxAgeSeconds :cognitect.aws.s3/MaxAgeSeconds)

(s/def :cognitect.aws.s3.StatsEvent/Details :cognitect.aws.s3/Stats)

(s/def :cognitect.aws.s3.RoutingRule/Condition :cognitect.aws.s3/Condition)

(s/def :cognitect.aws.s3.RoutingRule/Redirect :cognitect.aws.s3/Redirect)

(s/def :cognitect.aws.s3.ScanRange/Start :cognitect.aws.s3/Start)

(s/def :cognitect.aws.s3.ScanRange/End :cognitect.aws.s3/End)

(s/def :cognitect.aws.s3.InventoryConfiguration/Destination :cognitect.aws.s3/InventoryDestination)

(s/def :cognitect.aws.s3.InventoryConfiguration/IsEnabled :cognitect.aws.s3/IsEnabled)

(s/def :cognitect.aws.s3.InventoryConfiguration/Filter :cognitect.aws.s3/InventoryFilter)

(s/def :cognitect.aws.s3.InventoryConfiguration/Id :cognitect.aws.s3/InventoryId)

(s/def
  :cognitect.aws.s3.InventoryConfiguration/IncludedObjectVersions
  :cognitect.aws.s3/InventoryIncludedObjectVersions)

(s/def
  :cognitect.aws.s3.InventoryConfiguration/OptionalFields
  :cognitect.aws.s3/InventoryOptionalFields)

(s/def :cognitect.aws.s3.InventoryConfiguration/Schedule :cognitect.aws.s3/InventorySchedule)

(s/def :cognitect.aws.s3.PutObjectLegalHoldOutput/RequestCharged :cognitect.aws.s3/RequestCharged)

(s/def
  :cognitect.aws.s3.DeleteBucketAnalyticsConfigurationRequest/Bucket
  :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.DeleteBucketAnalyticsConfigurationRequest/Id :cognitect.aws.s3/AnalyticsId)

(s/def
  :cognitect.aws.s3.ListBucketMetricsConfigurationsOutput/IsTruncated
  :cognitect.aws.s3/IsTruncated)

(s/def
  :cognitect.aws.s3.ListBucketMetricsConfigurationsOutput/ContinuationToken
  :cognitect.aws.s3/Token)

(s/def
  :cognitect.aws.s3.ListBucketMetricsConfigurationsOutput/NextContinuationToken
  :cognitect.aws.s3/NextToken)

(s/def
  :cognitect.aws.s3.ListBucketMetricsConfigurationsOutput/MetricsConfigurationList
  :cognitect.aws.s3/MetricsConfigurationList)

(s/def :cognitect.aws.s3.HeadBucketRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.GetBucketCorsRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.Tag/Key :cognitect.aws.s3/ObjectKey)

(s/def :cognitect.aws.s3.Tag/Value :cognitect.aws.s3/Value)

(s/def :cognitect.aws.s3.InventorySchedule/Frequency :cognitect.aws.s3/InventoryFrequency)

(s/def :cognitect.aws.s3.S3KeyFilter/FilterRules :cognitect.aws.s3/FilterRuleList)

(s/def
  :cognitect.aws.s3.GetBucketNotificationConfigurationRequest/Bucket
  :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.CompletedPart/ETag :cognitect.aws.s3/ETag)

(s/def :cognitect.aws.s3.CompletedPart/PartNumber :cognitect.aws.s3/PartNumber)

(s/def
  :cognitect.aws.s3.ServerSideEncryptionByDefault/SSEAlgorithm
  :cognitect.aws.s3/ServerSideEncryption)

(s/def :cognitect.aws.s3.ServerSideEncryptionByDefault/KMSMasterKeyID :cognitect.aws.s3/SSEKMSKeyId)

(s/def :cognitect.aws.s3.Rule/Expiration :cognitect.aws.s3/LifecycleExpiration)

(s/def :cognitect.aws.s3.Rule/ID :cognitect.aws.s3/ID)

(s/def :cognitect.aws.s3.Rule/Prefix :cognitect.aws.s3/Prefix)

(s/def :cognitect.aws.s3.Rule/Status :cognitect.aws.s3/ExpirationStatus)

(s/def :cognitect.aws.s3.Rule/Transition :cognitect.aws.s3/Transition)

(s/def
  :cognitect.aws.s3.Rule/NoncurrentVersionTransition
  :cognitect.aws.s3/NoncurrentVersionTransition)

(s/def
  :cognitect.aws.s3.Rule/NoncurrentVersionExpiration
  :cognitect.aws.s3/NoncurrentVersionExpiration)

(s/def
  :cognitect.aws.s3.Rule/AbortIncompleteMultipartUpload
  :cognitect.aws.s3/AbortIncompleteMultipartUpload)

(s/def :cognitect.aws.s3.ProgressEvent/Details :cognitect.aws.s3/Progress)

(s/def :cognitect.aws.s3.Encryption/EncryptionType :cognitect.aws.s3/ServerSideEncryption)

(s/def :cognitect.aws.s3.Encryption/KMSKeyId :cognitect.aws.s3/SSEKMSKeyId)

(s/def :cognitect.aws.s3.Encryption/KMSContext :cognitect.aws.s3/KMSContext)

(s/def
  :cognitect.aws.s3.CompleteMultipartUploadOutput/RequestCharged
  :cognitect.aws.s3/RequestCharged)

(s/def :cognitect.aws.s3.CompleteMultipartUploadOutput/ETag :cognitect.aws.s3/ETag)

(s/def :cognitect.aws.s3.CompleteMultipartUploadOutput/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.CompleteMultipartUploadOutput/Key :cognitect.aws.s3/ObjectKey)

(s/def
  :cognitect.aws.s3.CompleteMultipartUploadOutput/ServerSideEncryption
  :cognitect.aws.s3/ServerSideEncryption)

(s/def :cognitect.aws.s3.CompleteMultipartUploadOutput/Location :cognitect.aws.s3/Location)

(s/def :cognitect.aws.s3.CompleteMultipartUploadOutput/SSEKMSKeyId :cognitect.aws.s3/SSEKMSKeyId)

(s/def :cognitect.aws.s3.CompleteMultipartUploadOutput/Expiration :cognitect.aws.s3/Expiration)

(s/def :cognitect.aws.s3.CompleteMultipartUploadOutput/VersionId :cognitect.aws.s3/ObjectVersionId)

(s/def :cognitect.aws.s3.RestoreObjectOutput/RequestCharged :cognitect.aws.s3/RequestCharged)

(s/def :cognitect.aws.s3.RestoreObjectOutput/RestoreOutputPath :cognitect.aws.s3/RestoreOutputPath)

(s/def :cognitect.aws.s3.RequestPaymentConfiguration/Payer :cognitect.aws.s3/Payer)

(s/def :cognitect.aws.s3.GetObjectLegalHoldRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.GetObjectLegalHoldRequest/Key :cognitect.aws.s3/ObjectKey)

(s/def :cognitect.aws.s3.GetObjectLegalHoldRequest/VersionId :cognitect.aws.s3/ObjectVersionId)

(s/def :cognitect.aws.s3.GetObjectLegalHoldRequest/RequestPayer :cognitect.aws.s3/RequestPayer)

(s/def :cognitect.aws.s3.QueueConfiguration/Id :cognitect.aws.s3/NotificationId)

(s/def :cognitect.aws.s3.QueueConfiguration/QueueArn :cognitect.aws.s3/QueueArn)

(s/def :cognitect.aws.s3.QueueConfiguration/Events :cognitect.aws.s3/EventList)

(s/def
  :cognitect.aws.s3.QueueConfiguration/Filter
  :cognitect.aws.s3/NotificationConfigurationFilter)

(s/def :cognitect.aws.s3.Initiator/ID :cognitect.aws.s3/ID)

(s/def :cognitect.aws.s3.Initiator/DisplayName :cognitect.aws.s3/DisplayName)

(s/def :cognitect.aws.s3.GetBucketEncryptionRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def
  :cognitect.aws.s3.PutBucketNotificationConfigurationRequest/Bucket
  :cognitect.aws.s3/BucketName)

(s/def
  :cognitect.aws.s3.PutBucketNotificationConfigurationRequest/NotificationConfiguration
  :cognitect.aws.s3/NotificationConfiguration)

(s/def :cognitect.aws.s3.GetBucketLifecycleRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.GetBucketRequestPaymentOutput/Payer :cognitect.aws.s3/Payer)

(s/def :cognitect.aws.s3.PutBucketNotificationRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.PutBucketNotificationRequest/ContentMD5 :cognitect.aws.s3/ContentMD5)

(s/def
  :cognitect.aws.s3.PutBucketNotificationRequest/NotificationConfiguration
  :cognitect.aws.s3/NotificationConfigurationDeprecated)

(s/def
  :cognitect.aws.s3.DeleteBucketMetricsConfigurationRequest/Bucket
  :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.DeleteBucketMetricsConfigurationRequest/Id :cognitect.aws.s3/MetricsId)

(s/def :cognitect.aws.s3.ListObjectsRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.ListObjectsRequest/Delimiter :cognitect.aws.s3/Delimiter)

(s/def :cognitect.aws.s3.ListObjectsRequest/EncodingType :cognitect.aws.s3/EncodingType)

(s/def :cognitect.aws.s3.ListObjectsRequest/Marker :cognitect.aws.s3/Marker)

(s/def :cognitect.aws.s3.ListObjectsRequest/MaxKeys :cognitect.aws.s3/MaxKeys)

(s/def :cognitect.aws.s3.ListObjectsRequest/Prefix :cognitect.aws.s3/Prefix)

(s/def :cognitect.aws.s3.ListObjectsRequest/RequestPayer :cognitect.aws.s3/RequestPayer)

(s/def :cognitect.aws.s3.CopyObjectOutput/RequestCharged :cognitect.aws.s3/RequestCharged)

(s/def :cognitect.aws.s3.CopyObjectOutput/SSECustomerKeyMD5 :cognitect.aws.s3/SSECustomerKeyMD5)

(s/def :cognitect.aws.s3.CopyObjectOutput/CopyObjectResult :cognitect.aws.s3/CopyObjectResult)

(s/def
  :cognitect.aws.s3.CopyObjectOutput/SSEKMSEncryptionContext
  :cognitect.aws.s3/SSEKMSEncryptionContext)

(s/def
  :cognitect.aws.s3.CopyObjectOutput/ServerSideEncryption
  :cognitect.aws.s3/ServerSideEncryption)

(s/def
  :cognitect.aws.s3.CopyObjectOutput/SSECustomerAlgorithm
  :cognitect.aws.s3/SSECustomerAlgorithm)

(s/def :cognitect.aws.s3.CopyObjectOutput/SSEKMSKeyId :cognitect.aws.s3/SSEKMSKeyId)

(s/def :cognitect.aws.s3.CopyObjectOutput/CopySourceVersionId :cognitect.aws.s3/CopySourceVersionId)

(s/def :cognitect.aws.s3.CopyObjectOutput/Expiration :cognitect.aws.s3/Expiration)

(s/def :cognitect.aws.s3.CopyObjectOutput/VersionId :cognitect.aws.s3/ObjectVersionId)

(s/def :cognitect.aws.s3.ListPartsOutput/RequestCharged :cognitect.aws.s3/RequestCharged)

(s/def :cognitect.aws.s3.ListPartsOutput/Initiator :cognitect.aws.s3/Initiator)

(s/def :cognitect.aws.s3.ListPartsOutput/UploadId :cognitect.aws.s3/MultipartUploadId)

(s/def :cognitect.aws.s3.ListPartsOutput/AbortDate :cognitect.aws.s3/AbortDate)

(s/def :cognitect.aws.s3.ListPartsOutput/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.ListPartsOutput/Key :cognitect.aws.s3/ObjectKey)

(s/def :cognitect.aws.s3.ListPartsOutput/Owner :cognitect.aws.s3/Owner)

(s/def :cognitect.aws.s3.ListPartsOutput/MaxParts :cognitect.aws.s3/MaxParts)

(s/def
  :cognitect.aws.s3.ListPartsOutput/NextPartNumberMarker
  :cognitect.aws.s3/NextPartNumberMarker)

(s/def :cognitect.aws.s3.ListPartsOutput/IsTruncated :cognitect.aws.s3/IsTruncated)

(s/def :cognitect.aws.s3.ListPartsOutput/PartNumberMarker :cognitect.aws.s3/PartNumberMarker)

(s/def :cognitect.aws.s3.ListPartsOutput/Parts :cognitect.aws.s3/Parts)

(s/def :cognitect.aws.s3.ListPartsOutput/StorageClass :cognitect.aws.s3/StorageClass)

(s/def :cognitect.aws.s3.ListPartsOutput/AbortRuleId :cognitect.aws.s3/AbortRuleId)

(s/def :cognitect.aws.s3.PutBucketVersioningRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.PutBucketVersioningRequest/ContentMD5 :cognitect.aws.s3/ContentMD5)

(s/def :cognitect.aws.s3.PutBucketVersioningRequest/MFA :cognitect.aws.s3/MFA)

(s/def
  :cognitect.aws.s3.PutBucketVersioningRequest/VersioningConfiguration
  :cognitect.aws.s3/VersioningConfiguration)

(s/def :cognitect.aws.s3.GetBucketAnalyticsConfigurationRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.GetBucketAnalyticsConfigurationRequest/Id :cognitect.aws.s3/AnalyticsId)

(s/def :cognitect.aws.s3.AccelerateConfiguration/Status :cognitect.aws.s3/BucketAccelerateStatus)

(s/def :cognitect.aws.s3.GetPublicAccessBlockRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.VersioningConfiguration/MFADelete :cognitect.aws.s3/MFADelete)

(s/def :cognitect.aws.s3.VersioningConfiguration/Status :cognitect.aws.s3/BucketVersioningStatus)

(s/def
  :cognitect.aws.s3.GetBucketEncryptionOutput/ServerSideEncryptionConfiguration
  :cognitect.aws.s3/ServerSideEncryptionConfiguration)

(s/def :cognitect.aws.s3.GetBucketPolicyStatusRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def
  :cognitect.aws.s3.StorageClassAnalysis/DataExport
  :cognitect.aws.s3/StorageClassAnalysisDataExport)

(s/def :cognitect.aws.s3.PutBucketInventoryConfigurationRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.PutBucketInventoryConfigurationRequest/Id :cognitect.aws.s3/InventoryId)

(s/def
  :cognitect.aws.s3.PutBucketInventoryConfigurationRequest/InventoryConfiguration
  :cognitect.aws.s3/InventoryConfiguration)

(s/def :cognitect.aws.s3.Bucket/Name :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.Bucket/CreationDate :cognitect.aws.s3/CreationDate)

(s/def
  :cognitect.aws.s3.GetBucketAnalyticsConfigurationOutput/AnalyticsConfiguration
  :cognitect.aws.s3/AnalyticsConfiguration)

(s/def :cognitect.aws.s3.BucketLifecycleConfiguration/Rules :cognitect.aws.s3/LifecycleRules)

(s/def :cognitect.aws.s3.ObjectVersion/ETag :cognitect.aws.s3/ETag)

(s/def :cognitect.aws.s3.ObjectVersion/Size :cognitect.aws.s3/Size)

(s/def :cognitect.aws.s3.ObjectVersion/StorageClass :cognitect.aws.s3/ObjectVersionStorageClass)

(s/def :cognitect.aws.s3.ObjectVersion/Key :cognitect.aws.s3/ObjectKey)

(s/def :cognitect.aws.s3.ObjectVersion/VersionId :cognitect.aws.s3/ObjectVersionId)

(s/def :cognitect.aws.s3.ObjectVersion/IsLatest :cognitect.aws.s3/IsLatest)

(s/def :cognitect.aws.s3.ObjectVersion/LastModified :cognitect.aws.s3/LastModified)

(s/def :cognitect.aws.s3.ObjectVersion/Owner :cognitect.aws.s3/Owner)

(s/def
  :cognitect.aws.s3.SseKmsEncryptedObjects/Status
  :cognitect.aws.s3/SseKmsEncryptedObjectsStatus)

(s/def :cognitect.aws.s3.CreateBucketOutput/Location :cognitect.aws.s3/Location)

(s/def :cognitect.aws.s3.GetObjectAclRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.GetObjectAclRequest/Key :cognitect.aws.s3/ObjectKey)

(s/def :cognitect.aws.s3.GetObjectAclRequest/VersionId :cognitect.aws.s3/ObjectVersionId)

(s/def :cognitect.aws.s3.GetObjectAclRequest/RequestPayer :cognitect.aws.s3/RequestPayer)

(s/def :cognitect.aws.s3.Stats/BytesScanned :cognitect.aws.s3/BytesScanned)

(s/def :cognitect.aws.s3.Stats/BytesProcessed :cognitect.aws.s3/BytesProcessed)

(s/def :cognitect.aws.s3.Stats/BytesReturned :cognitect.aws.s3/BytesReturned)

(s/def
  :cognitect.aws.s3.PutBucketAccelerateConfigurationRequest/Bucket
  :cognitect.aws.s3/BucketName)

(s/def
  :cognitect.aws.s3.PutBucketAccelerateConfigurationRequest/AccelerateConfiguration
  :cognitect.aws.s3/AccelerateConfiguration)

(s/def :cognitect.aws.s3.DeletePublicAccessBlockRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def
  :cognitect.aws.s3.CreateMultipartUploadOutput/RequestCharged
  :cognitect.aws.s3/RequestCharged)

(s/def :cognitect.aws.s3.CreateMultipartUploadOutput/UploadId :cognitect.aws.s3/MultipartUploadId)

(s/def :cognitect.aws.s3.CreateMultipartUploadOutput/AbortDate :cognitect.aws.s3/AbortDate)

(s/def :cognitect.aws.s3.CreateMultipartUploadOutput/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.CreateMultipartUploadOutput/Key :cognitect.aws.s3/ObjectKey)

(s/def
  :cognitect.aws.s3.CreateMultipartUploadOutput/SSECustomerKeyMD5
  :cognitect.aws.s3/SSECustomerKeyMD5)

(s/def
  :cognitect.aws.s3.CreateMultipartUploadOutput/SSEKMSEncryptionContext
  :cognitect.aws.s3/SSEKMSEncryptionContext)

(s/def
  :cognitect.aws.s3.CreateMultipartUploadOutput/ServerSideEncryption
  :cognitect.aws.s3/ServerSideEncryption)

(s/def
  :cognitect.aws.s3.CreateMultipartUploadOutput/SSECustomerAlgorithm
  :cognitect.aws.s3/SSECustomerAlgorithm)

(s/def :cognitect.aws.s3.CreateMultipartUploadOutput/SSEKMSKeyId :cognitect.aws.s3/SSEKMSKeyId)

(s/def :cognitect.aws.s3.CreateMultipartUploadOutput/AbortRuleId :cognitect.aws.s3/AbortRuleId)

(s/def :cognitect.aws.s3.DeleteMarkerEntry/Owner :cognitect.aws.s3/Owner)

(s/def :cognitect.aws.s3.DeleteMarkerEntry/Key :cognitect.aws.s3/ObjectKey)

(s/def :cognitect.aws.s3.DeleteMarkerEntry/VersionId :cognitect.aws.s3/ObjectVersionId)

(s/def :cognitect.aws.s3.DeleteMarkerEntry/IsLatest :cognitect.aws.s3/IsLatest)

(s/def :cognitect.aws.s3.DeleteMarkerEntry/LastModified :cognitect.aws.s3/LastModified)

(s/def :cognitect.aws.s3.InventoryS3BucketDestination/AccountId :cognitect.aws.s3/AccountId)

(s/def :cognitect.aws.s3.InventoryS3BucketDestination/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.InventoryS3BucketDestination/Format :cognitect.aws.s3/InventoryFormat)

(s/def :cognitect.aws.s3.InventoryS3BucketDestination/Prefix :cognitect.aws.s3/Prefix)

(s/def
  :cognitect.aws.s3.InventoryS3BucketDestination/Encryption
  :cognitect.aws.s3/InventoryEncryption)

(s/def :cognitect.aws.s3.DefaultRetention/Mode :cognitect.aws.s3/ObjectLockRetentionMode)

(s/def :cognitect.aws.s3.DefaultRetention/Days :cognitect.aws.s3/Days)

(s/def :cognitect.aws.s3.DefaultRetention/Years :cognitect.aws.s3/Years)

(s/def :cognitect.aws.s3.LoggingEnabled/TargetBucket :cognitect.aws.s3/TargetBucket)

(s/def :cognitect.aws.s3.LoggingEnabled/TargetGrants :cognitect.aws.s3/TargetGrants)

(s/def :cognitect.aws.s3.LoggingEnabled/TargetPrefix :cognitect.aws.s3/TargetPrefix)

(s/def :cognitect.aws.s3.AccessControlPolicy/Grants :cognitect.aws.s3/Grants)

(s/def :cognitect.aws.s3.AccessControlPolicy/Owner :cognitect.aws.s3/Owner)

(s/def :cognitect.aws.s3.PutObjectTaggingRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.PutObjectTaggingRequest/Key :cognitect.aws.s3/ObjectKey)

(s/def :cognitect.aws.s3.PutObjectTaggingRequest/VersionId :cognitect.aws.s3/ObjectVersionId)

(s/def :cognitect.aws.s3.PutObjectTaggingRequest/ContentMD5 :cognitect.aws.s3/ContentMD5)

(s/def :cognitect.aws.s3.PutObjectTaggingRequest/Tagging :cognitect.aws.s3/Tagging)

(s/def :cognitect.aws.s3.CopyObjectRequest/CopySource :cognitect.aws.s3/CopySource)

(s/def
  :cognitect.aws.s3.CopyObjectRequest/CopySourceSSECustomerAlgorithm
  :cognitect.aws.s3/CopySourceSSECustomerAlgorithm)

(s/def :cognitect.aws.s3.CopyObjectRequest/ContentLanguage :cognitect.aws.s3/ContentLanguage)

(s/def :cognitect.aws.s3.CopyObjectRequest/GrantRead :cognitect.aws.s3/GrantRead)

(s/def :cognitect.aws.s3.CopyObjectRequest/GrantReadACP :cognitect.aws.s3/GrantReadACP)

(s/def :cognitect.aws.s3.CopyObjectRequest/ACL :cognitect.aws.s3/ObjectCannedACL)

(s/def
  :cognitect.aws.s3.CopyObjectRequest/CopySourceSSECustomerKey
  :cognitect.aws.s3/CopySourceSSECustomerKey)

(s/def
  :cognitect.aws.s3.CopyObjectRequest/CopySourceSSECustomerKeyMD5
  :cognitect.aws.s3/CopySourceSSECustomerKeyMD5)

(s/def :cognitect.aws.s3.CopyObjectRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.CopyObjectRequest/Key :cognitect.aws.s3/ObjectKey)

(s/def :cognitect.aws.s3.CopyObjectRequest/MetadataDirective :cognitect.aws.s3/MetadataDirective)

(s/def :cognitect.aws.s3.CopyObjectRequest/SSECustomerKeyMD5 :cognitect.aws.s3/SSECustomerKeyMD5)

(s/def :cognitect.aws.s3.CopyObjectRequest/GrantWriteACP :cognitect.aws.s3/GrantWriteACP)

(s/def :cognitect.aws.s3.CopyObjectRequest/Tagging :cognitect.aws.s3/TaggingHeader)

(s/def :cognitect.aws.s3.CopyObjectRequest/ContentEncoding :cognitect.aws.s3/ContentEncoding)

(s/def
  :cognitect.aws.s3.CopyObjectRequest/CopySourceIfUnmodifiedSince
  :cognitect.aws.s3/CopySourceIfUnmodifiedSince)

(s/def :cognitect.aws.s3.CopyObjectRequest/ObjectLockMode :cognitect.aws.s3/ObjectLockMode)

(s/def
  :cognitect.aws.s3.CopyObjectRequest/CopySourceIfModifiedSince
  :cognitect.aws.s3/CopySourceIfModifiedSince)

(s/def
  :cognitect.aws.s3.CopyObjectRequest/CopySourceIfNoneMatch
  :cognitect.aws.s3/CopySourceIfNoneMatch)

(s/def :cognitect.aws.s3.CopyObjectRequest/ContentDisposition :cognitect.aws.s3/ContentDisposition)

(s/def :cognitect.aws.s3.CopyObjectRequest/SSECustomerKey :cognitect.aws.s3/SSECustomerKey)

(s/def :cognitect.aws.s3.CopyObjectRequest/Metadata :cognitect.aws.s3/Metadata)

(s/def
  :cognitect.aws.s3.CopyObjectRequest/SSEKMSEncryptionContext
  :cognitect.aws.s3/SSEKMSEncryptionContext)

(s/def :cognitect.aws.s3.CopyObjectRequest/RequestPayer :cognitect.aws.s3/RequestPayer)

(s/def
  :cognitect.aws.s3.CopyObjectRequest/ServerSideEncryption
  :cognitect.aws.s3/ServerSideEncryption)

(s/def
  :cognitect.aws.s3.CopyObjectRequest/SSECustomerAlgorithm
  :cognitect.aws.s3/SSECustomerAlgorithm)

(s/def :cognitect.aws.s3.CopyObjectRequest/CacheControl :cognitect.aws.s3/CacheControl)

(s/def :cognitect.aws.s3.CopyObjectRequest/TaggingDirective :cognitect.aws.s3/TaggingDirective)

(s/def :cognitect.aws.s3.CopyObjectRequest/ContentType :cognitect.aws.s3/ContentType)

(s/def :cognitect.aws.s3.CopyObjectRequest/CopySourceIfMatch :cognitect.aws.s3/CopySourceIfMatch)

(s/def
  :cognitect.aws.s3.CopyObjectRequest/ObjectLockRetainUntilDate
  :cognitect.aws.s3/ObjectLockRetainUntilDate)

(s/def :cognitect.aws.s3.CopyObjectRequest/GrantFullControl :cognitect.aws.s3/GrantFullControl)

(s/def :cognitect.aws.s3.CopyObjectRequest/SSEKMSKeyId :cognitect.aws.s3/SSEKMSKeyId)

(s/def
  :cognitect.aws.s3.CopyObjectRequest/WebsiteRedirectLocation
  :cognitect.aws.s3/WebsiteRedirectLocation)

(s/def
  :cognitect.aws.s3.CopyObjectRequest/ObjectLockLegalHoldStatus
  :cognitect.aws.s3/ObjectLockLegalHoldStatus)

(s/def :cognitect.aws.s3.CopyObjectRequest/StorageClass :cognitect.aws.s3/StorageClass)

(s/def :cognitect.aws.s3.CopyObjectRequest/Expires :cognitect.aws.s3/Expires)

(s/def :cognitect.aws.s3.Owner/DisplayName :cognitect.aws.s3/DisplayName)

(s/def :cognitect.aws.s3.Owner/ID :cognitect.aws.s3/ID)

(s/def :cognitect.aws.s3.Destination/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.Destination/Account :cognitect.aws.s3/AccountId)

(s/def :cognitect.aws.s3.Destination/StorageClass :cognitect.aws.s3/StorageClass)

(s/def
  :cognitect.aws.s3.Destination/AccessControlTranslation
  :cognitect.aws.s3/AccessControlTranslation)

(s/def
  :cognitect.aws.s3.Destination/EncryptionConfiguration
  :cognitect.aws.s3/EncryptionConfiguration)

(s/def :cognitect.aws.s3.Destination/ReplicationTime :cognitect.aws.s3/ReplicationTime)

(s/def :cognitect.aws.s3.Destination/Metrics :cognitect.aws.s3/Metrics)

(s/def
  :cognitect.aws.s3.GetBucketWebsiteOutput/RedirectAllRequestsTo
  :cognitect.aws.s3/RedirectAllRequestsTo)

(s/def :cognitect.aws.s3.GetBucketWebsiteOutput/IndexDocument :cognitect.aws.s3/IndexDocument)

(s/def :cognitect.aws.s3.GetBucketWebsiteOutput/ErrorDocument :cognitect.aws.s3/ErrorDocument)

(s/def :cognitect.aws.s3.GetBucketWebsiteOutput/RoutingRules :cognitect.aws.s3/RoutingRules)

(s/def :cognitect.aws.s3.GetBucketReplicationRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.HeadObjectRequest/IfNoneMatch :cognitect.aws.s3/IfNoneMatch)

(s/def :cognitect.aws.s3.HeadObjectRequest/IfUnmodifiedSince :cognitect.aws.s3/IfUnmodifiedSince)

(s/def :cognitect.aws.s3.HeadObjectRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.HeadObjectRequest/Key :cognitect.aws.s3/ObjectKey)

(s/def :cognitect.aws.s3.HeadObjectRequest/SSECustomerKeyMD5 :cognitect.aws.s3/SSECustomerKeyMD5)

(s/def :cognitect.aws.s3.HeadObjectRequest/PartNumber :cognitect.aws.s3/PartNumber)

(s/def :cognitect.aws.s3.HeadObjectRequest/IfModifiedSince :cognitect.aws.s3/IfModifiedSince)

(s/def :cognitect.aws.s3.HeadObjectRequest/SSECustomerKey :cognitect.aws.s3/SSECustomerKey)

(s/def :cognitect.aws.s3.HeadObjectRequest/Range :cognitect.aws.s3/Range)

(s/def :cognitect.aws.s3.HeadObjectRequest/IfMatch :cognitect.aws.s3/IfMatch)

(s/def :cognitect.aws.s3.HeadObjectRequest/RequestPayer :cognitect.aws.s3/RequestPayer)

(s/def
  :cognitect.aws.s3.HeadObjectRequest/SSECustomerAlgorithm
  :cognitect.aws.s3/SSECustomerAlgorithm)

(s/def :cognitect.aws.s3.HeadObjectRequest/VersionId :cognitect.aws.s3/ObjectVersionId)

(s/def :cognitect.aws.s3.AccessControlTranslation/Owner :cognitect.aws.s3/OwnerOverride)

(s/def :cognitect.aws.s3.CloudFunctionConfiguration/Id :cognitect.aws.s3/NotificationId)

(s/def :cognitect.aws.s3.CloudFunctionConfiguration/Event :cognitect.aws.s3/Event)

(s/def :cognitect.aws.s3.CloudFunctionConfiguration/Events :cognitect.aws.s3/EventList)

(s/def :cognitect.aws.s3.CloudFunctionConfiguration/CloudFunction :cognitect.aws.s3/CloudFunction)

(s/def
  :cognitect.aws.s3.CloudFunctionConfiguration/InvocationRole
  :cognitect.aws.s3/CloudFunctionInvocationRole)

(s/def :cognitect.aws.s3.IndexDocument/Suffix :cognitect.aws.s3/Suffix)

(s/def :cognitect.aws.s3.InventoryFilter/Prefix :cognitect.aws.s3/Prefix)

(s/def :cognitect.aws.s3.GetBucketVersioningOutput/Status :cognitect.aws.s3/BucketVersioningStatus)

(s/def :cognitect.aws.s3.GetBucketVersioningOutput/MFADelete :cognitect.aws.s3/MFADeleteStatus)

(s/def :cognitect.aws.s3.PutBucketMetricsConfigurationRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.PutBucketMetricsConfigurationRequest/Id :cognitect.aws.s3/MetricsId)

(s/def
  :cognitect.aws.s3.PutBucketMetricsConfigurationRequest/MetricsConfiguration
  :cognitect.aws.s3/MetricsConfiguration)

(s/def :cognitect.aws.s3.GetObjectTaggingOutput/VersionId :cognitect.aws.s3/ObjectVersionId)

(s/def :cognitect.aws.s3.GetObjectTaggingOutput/TagSet :cognitect.aws.s3/TagSet)

(s/def :cognitect.aws.s3.PutBucketLifecycleConfigurationRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def
  :cognitect.aws.s3.PutBucketLifecycleConfigurationRequest/LifecycleConfiguration
  :cognitect.aws.s3/BucketLifecycleConfiguration)

(s/def :cognitect.aws.s3.DeleteObjectTaggingRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.DeleteObjectTaggingRequest/Key :cognitect.aws.s3/ObjectKey)

(s/def :cognitect.aws.s3.DeleteObjectTaggingRequest/VersionId :cognitect.aws.s3/ObjectVersionId)

(s/def
  :cognitect.aws.s3.GetBucketAccelerateConfigurationRequest/Bucket
  :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.FilterRule/Name :cognitect.aws.s3/FilterRuleName)

(s/def :cognitect.aws.s3.FilterRule/Value :cognitect.aws.s3/FilterRuleValue)

(s/def
  :cognitect.aws.s3.DeleteMarkerReplication/Status
  :cognitect.aws.s3/DeleteMarkerReplicationStatus)

(s/def :cognitect.aws.s3.PutBucketAclRequest/GrantWrite :cognitect.aws.s3/GrantWrite)

(s/def :cognitect.aws.s3.PutBucketAclRequest/GrantRead :cognitect.aws.s3/GrantRead)

(s/def :cognitect.aws.s3.PutBucketAclRequest/GrantReadACP :cognitect.aws.s3/GrantReadACP)

(s/def :cognitect.aws.s3.PutBucketAclRequest/ACL :cognitect.aws.s3/BucketCannedACL)

(s/def :cognitect.aws.s3.PutBucketAclRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def
  :cognitect.aws.s3.PutBucketAclRequest/AccessControlPolicy
  :cognitect.aws.s3/AccessControlPolicy)

(s/def :cognitect.aws.s3.PutBucketAclRequest/ContentMD5 :cognitect.aws.s3/ContentMD5)

(s/def :cognitect.aws.s3.PutBucketAclRequest/GrantWriteACP :cognitect.aws.s3/GrantWriteACP)

(s/def :cognitect.aws.s3.PutBucketAclRequest/GrantFullControl :cognitect.aws.s3/GrantFullControl)

(s/def :cognitect.aws.s3.MetadataEntry/Name :cognitect.aws.s3/MetadataKey)

(s/def :cognitect.aws.s3.MetadataEntry/Value :cognitect.aws.s3/MetadataValue)

(s/def
  :cognitect.aws.s3.GetBucketMetricsConfigurationOutput/MetricsConfiguration
  :cognitect.aws.s3/MetricsConfiguration)

(s/def :cognitect.aws.s3.PutBucketCorsRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.PutBucketCorsRequest/CORSConfiguration :cognitect.aws.s3/CORSConfiguration)

(s/def :cognitect.aws.s3.PutBucketCorsRequest/ContentMD5 :cognitect.aws.s3/ContentMD5)

(s/def
  :cognitect.aws.s3.AbortIncompleteMultipartUpload/DaysAfterInitiation
  :cognitect.aws.s3/DaysAfterInitiation)

(s/def :cognitect.aws.s3.Tagging/TagSet :cognitect.aws.s3/TagSet)

(s/def :cognitect.aws.s3.ListObjectsV2Request/Prefix :cognitect.aws.s3/Prefix)

(s/def :cognitect.aws.s3.ListObjectsV2Request/StartAfter :cognitect.aws.s3/StartAfter)

(s/def :cognitect.aws.s3.ListObjectsV2Request/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.ListObjectsV2Request/EncodingType :cognitect.aws.s3/EncodingType)

(s/def :cognitect.aws.s3.ListObjectsV2Request/Delimiter :cognitect.aws.s3/Delimiter)

(s/def :cognitect.aws.s3.ListObjectsV2Request/FetchOwner :cognitect.aws.s3/FetchOwner)

(s/def :cognitect.aws.s3.ListObjectsV2Request/RequestPayer :cognitect.aws.s3/RequestPayer)

(s/def :cognitect.aws.s3.ListObjectsV2Request/ContinuationToken :cognitect.aws.s3/Token)

(s/def :cognitect.aws.s3.ListObjectsV2Request/MaxKeys :cognitect.aws.s3/MaxKeys)

(s/def :cognitect.aws.s3.GetObjectTorrentOutput/Body :cognitect.aws.s3/Body)

(s/def :cognitect.aws.s3.GetObjectTorrentOutput/RequestCharged :cognitect.aws.s3/RequestCharged)

(s/def :cognitect.aws.s3.ListBucketsOutput/Buckets :cognitect.aws.s3/Buckets)

(s/def :cognitect.aws.s3.ListBucketsOutput/Owner :cognitect.aws.s3/Owner)

(s/def :cognitect.aws.s3.GetBucketCorsOutput/CORSRules :cognitect.aws.s3/CORSRules)

(s/def :cognitect.aws.s3.QueueConfigurationDeprecated/Id :cognitect.aws.s3/NotificationId)

(s/def :cognitect.aws.s3.QueueConfigurationDeprecated/Event :cognitect.aws.s3/Event)

(s/def :cognitect.aws.s3.QueueConfigurationDeprecated/Events :cognitect.aws.s3/EventList)

(s/def :cognitect.aws.s3.QueueConfigurationDeprecated/Queue :cognitect.aws.s3/QueueArn)

(s/def :cognitect.aws.s3.S3Location/BucketName :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.S3Location/Prefix :cognitect.aws.s3/LocationPrefix)

(s/def :cognitect.aws.s3.S3Location/Encryption :cognitect.aws.s3/Encryption)

(s/def :cognitect.aws.s3.S3Location/CannedACL :cognitect.aws.s3/ObjectCannedACL)

(s/def :cognitect.aws.s3.S3Location/AccessControlList :cognitect.aws.s3/Grants)

(s/def :cognitect.aws.s3.S3Location/Tagging :cognitect.aws.s3/Tagging)

(s/def :cognitect.aws.s3.S3Location/UserMetadata :cognitect.aws.s3/UserMetadata)

(s/def :cognitect.aws.s3.S3Location/StorageClass :cognitect.aws.s3/StorageClass)

(s/def :cognitect.aws.s3.MetricsConfiguration/Id :cognitect.aws.s3/MetricsId)

(s/def :cognitect.aws.s3.MetricsConfiguration/Filter :cognitect.aws.s3/MetricsFilter)

(s/def :cognitect.aws.s3.GlacierJobParameters/Tier :cognitect.aws.s3/Tier)

(s/def
  :cognitect.aws.s3.UploadPartCopyOutput/CopySourceVersionId
  :cognitect.aws.s3/CopySourceVersionId)

(s/def :cognitect.aws.s3.UploadPartCopyOutput/CopyPartResult :cognitect.aws.s3/CopyPartResult)

(s/def
  :cognitect.aws.s3.UploadPartCopyOutput/ServerSideEncryption
  :cognitect.aws.s3/ServerSideEncryption)

(s/def
  :cognitect.aws.s3.UploadPartCopyOutput/SSECustomerAlgorithm
  :cognitect.aws.s3/SSECustomerAlgorithm)

(s/def :cognitect.aws.s3.UploadPartCopyOutput/SSECustomerKeyMD5 :cognitect.aws.s3/SSECustomerKeyMD5)

(s/def :cognitect.aws.s3.UploadPartCopyOutput/SSEKMSKeyId :cognitect.aws.s3/SSEKMSKeyId)

(s/def :cognitect.aws.s3.UploadPartCopyOutput/RequestCharged :cognitect.aws.s3/RequestCharged)

(s/def :cognitect.aws.s3.GetBucketLoggingRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.PutObjectOutput/RequestCharged :cognitect.aws.s3/RequestCharged)

(s/def :cognitect.aws.s3.PutObjectOutput/ETag :cognitect.aws.s3/ETag)

(s/def :cognitect.aws.s3.PutObjectOutput/SSECustomerKeyMD5 :cognitect.aws.s3/SSECustomerKeyMD5)

(s/def
  :cognitect.aws.s3.PutObjectOutput/SSEKMSEncryptionContext
  :cognitect.aws.s3/SSEKMSEncryptionContext)

(s/def
  :cognitect.aws.s3.PutObjectOutput/ServerSideEncryption
  :cognitect.aws.s3/ServerSideEncryption)

(s/def
  :cognitect.aws.s3.PutObjectOutput/SSECustomerAlgorithm
  :cognitect.aws.s3/SSECustomerAlgorithm)

(s/def :cognitect.aws.s3.PutObjectOutput/SSEKMSKeyId :cognitect.aws.s3/SSEKMSKeyId)

(s/def :cognitect.aws.s3.PutObjectOutput/Expiration :cognitect.aws.s3/Expiration)

(s/def :cognitect.aws.s3.PutObjectOutput/VersionId :cognitect.aws.s3/ObjectVersionId)

(s/def
  :cognitect.aws.s3.GetBucketReplicationOutput/ReplicationConfiguration
  :cognitect.aws.s3/ReplicationConfiguration)

(s/def :cognitect.aws.s3.TopicConfiguration/Id :cognitect.aws.s3/NotificationId)

(s/def :cognitect.aws.s3.TopicConfiguration/TopicArn :cognitect.aws.s3/TopicArn)

(s/def :cognitect.aws.s3.TopicConfiguration/Events :cognitect.aws.s3/EventList)

(s/def
  :cognitect.aws.s3.TopicConfiguration/Filter
  :cognitect.aws.s3/NotificationConfigurationFilter)

(s/def :cognitect.aws.s3.Grant/Grantee :cognitect.aws.s3/Grantee)

(s/def :cognitect.aws.s3.Grant/Permission :cognitect.aws.s3/Permission)

(s/def
  :cognitect.aws.s3.NotificationConfiguration/TopicConfigurations
  :cognitect.aws.s3/TopicConfigurationList)

(s/def
  :cognitect.aws.s3.NotificationConfiguration/QueueConfigurations
  :cognitect.aws.s3/QueueConfigurationList)

(s/def
  :cognitect.aws.s3.NotificationConfiguration/LambdaFunctionConfigurations
  :cognitect.aws.s3/LambdaFunctionConfigurationList)

(s/def :cognitect.aws.s3.RedirectAllRequestsTo/HostName :cognitect.aws.s3/HostName)

(s/def :cognitect.aws.s3.RedirectAllRequestsTo/Protocol :cognitect.aws.s3/Protocol)

(s/def
  :cognitect.aws.s3.CreateBucketConfiguration/LocationConstraint
  :cognitect.aws.s3/BucketLocationConstraint)

(s/def :cognitect.aws.s3.JSONOutput/RecordDelimiter :cognitect.aws.s3/RecordDelimiter)

(s/def
  :cognitect.aws.s3.SelectObjectContentOutput/Payload
  :cognitect.aws.s3/SelectObjectContentEventStream)

(s/def :cognitect.aws.s3.ListBucketMetricsConfigurationsRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def
  :cognitect.aws.s3.ListBucketMetricsConfigurationsRequest/ContinuationToken
  :cognitect.aws.s3/Token)

(s/def :cognitect.aws.s3.UploadPartCopyRequest/CopySource :cognitect.aws.s3/CopySource)

(s/def
  :cognitect.aws.s3.UploadPartCopyRequest/CopySourceSSECustomerAlgorithm
  :cognitect.aws.s3/CopySourceSSECustomerAlgorithm)

(s/def :cognitect.aws.s3.UploadPartCopyRequest/UploadId :cognitect.aws.s3/MultipartUploadId)

(s/def
  :cognitect.aws.s3.UploadPartCopyRequest/CopySourceSSECustomerKey
  :cognitect.aws.s3/CopySourceSSECustomerKey)

(s/def
  :cognitect.aws.s3.UploadPartCopyRequest/CopySourceSSECustomerKeyMD5
  :cognitect.aws.s3/CopySourceSSECustomerKeyMD5)

(s/def :cognitect.aws.s3.UploadPartCopyRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.UploadPartCopyRequest/Key :cognitect.aws.s3/ObjectKey)

(s/def
  :cognitect.aws.s3.UploadPartCopyRequest/SSECustomerKeyMD5
  :cognitect.aws.s3/SSECustomerKeyMD5)

(s/def
  :cognitect.aws.s3.UploadPartCopyRequest/CopySourceIfUnmodifiedSince
  :cognitect.aws.s3/CopySourceIfUnmodifiedSince)

(s/def
  :cognitect.aws.s3.UploadPartCopyRequest/CopySourceIfModifiedSince
  :cognitect.aws.s3/CopySourceIfModifiedSince)

(s/def :cognitect.aws.s3.UploadPartCopyRequest/PartNumber :cognitect.aws.s3/PartNumber)

(s/def
  :cognitect.aws.s3.UploadPartCopyRequest/CopySourceIfNoneMatch
  :cognitect.aws.s3/CopySourceIfNoneMatch)

(s/def :cognitect.aws.s3.UploadPartCopyRequest/SSECustomerKey :cognitect.aws.s3/SSECustomerKey)

(s/def :cognitect.aws.s3.UploadPartCopyRequest/RequestPayer :cognitect.aws.s3/RequestPayer)

(s/def
  :cognitect.aws.s3.UploadPartCopyRequest/SSECustomerAlgorithm
  :cognitect.aws.s3/SSECustomerAlgorithm)

(s/def
  :cognitect.aws.s3.UploadPartCopyRequest/CopySourceIfMatch
  :cognitect.aws.s3/CopySourceIfMatch)

(s/def :cognitect.aws.s3.UploadPartCopyRequest/CopySourceRange :cognitect.aws.s3/CopySourceRange)

(s/def :cognitect.aws.s3.GetObjectOutput/RequestCharged :cognitect.aws.s3/RequestCharged)

(s/def :cognitect.aws.s3.GetObjectOutput/LastModified :cognitect.aws.s3/LastModified)

(s/def :cognitect.aws.s3.GetObjectOutput/PartsCount :cognitect.aws.s3/PartsCount)

(s/def :cognitect.aws.s3.GetObjectOutput/ETag :cognitect.aws.s3/ETag)

(s/def :cognitect.aws.s3.GetObjectOutput/ContentLanguage :cognitect.aws.s3/ContentLanguage)

(s/def :cognitect.aws.s3.GetObjectOutput/ReplicationStatus :cognitect.aws.s3/ReplicationStatus)

(s/def :cognitect.aws.s3.GetObjectOutput/SSECustomerKeyMD5 :cognitect.aws.s3/SSECustomerKeyMD5)

(s/def :cognitect.aws.s3.GetObjectOutput/ContentRange :cognitect.aws.s3/ContentRange)

(s/def :cognitect.aws.s3.GetObjectOutput/ContentEncoding :cognitect.aws.s3/ContentEncoding)

(s/def :cognitect.aws.s3.GetObjectOutput/ObjectLockMode :cognitect.aws.s3/ObjectLockMode)

(s/def :cognitect.aws.s3.GetObjectOutput/Body :cognitect.aws.s3/Body)

(s/def :cognitect.aws.s3.GetObjectOutput/ContentDisposition :cognitect.aws.s3/ContentDisposition)

(s/def :cognitect.aws.s3.GetObjectOutput/Metadata :cognitect.aws.s3/Metadata)

(s/def
  :cognitect.aws.s3.GetObjectOutput/ServerSideEncryption
  :cognitect.aws.s3/ServerSideEncryption)

(s/def :cognitect.aws.s3.GetObjectOutput/Restore :cognitect.aws.s3/Restore)

(s/def :cognitect.aws.s3.GetObjectOutput/ContentLength :cognitect.aws.s3/ContentLength)

(s/def :cognitect.aws.s3.GetObjectOutput/DeleteMarker :cognitect.aws.s3/DeleteMarker)

(s/def
  :cognitect.aws.s3.GetObjectOutput/SSECustomerAlgorithm
  :cognitect.aws.s3/SSECustomerAlgorithm)

(s/def :cognitect.aws.s3.GetObjectOutput/CacheControl :cognitect.aws.s3/CacheControl)

(s/def :cognitect.aws.s3.GetObjectOutput/ContentType :cognitect.aws.s3/ContentType)

(s/def :cognitect.aws.s3.GetObjectOutput/AcceptRanges :cognitect.aws.s3/AcceptRanges)

(s/def
  :cognitect.aws.s3.GetObjectOutput/ObjectLockRetainUntilDate
  :cognitect.aws.s3/ObjectLockRetainUntilDate)

(s/def :cognitect.aws.s3.GetObjectOutput/SSEKMSKeyId :cognitect.aws.s3/SSEKMSKeyId)

(s/def
  :cognitect.aws.s3.GetObjectOutput/WebsiteRedirectLocation
  :cognitect.aws.s3/WebsiteRedirectLocation)

(s/def :cognitect.aws.s3.GetObjectOutput/MissingMeta :cognitect.aws.s3/MissingMeta)

(s/def
  :cognitect.aws.s3.GetObjectOutput/ObjectLockLegalHoldStatus
  :cognitect.aws.s3/ObjectLockLegalHoldStatus)

(s/def :cognitect.aws.s3.GetObjectOutput/StorageClass :cognitect.aws.s3/StorageClass)

(s/def :cognitect.aws.s3.GetObjectOutput/TagCount :cognitect.aws.s3/TagCount)

(s/def :cognitect.aws.s3.GetObjectOutput/Expiration :cognitect.aws.s3/Expiration)

(s/def :cognitect.aws.s3.GetObjectOutput/VersionId :cognitect.aws.s3/ObjectVersionId)

(s/def :cognitect.aws.s3.GetObjectOutput/Expires :cognitect.aws.s3/Expires)

(s/def :cognitect.aws.s3.GetObjectLegalHoldOutput/LegalHold :cognitect.aws.s3/ObjectLockLegalHold)

(s/def
  :cognitect.aws.s3.ServerSideEncryptionRule/ApplyServerSideEncryptionByDefault
  :cognitect.aws.s3/ServerSideEncryptionByDefault)

(s/def :cognitect.aws.s3.GetObjectRetentionRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.GetObjectRetentionRequest/Key :cognitect.aws.s3/ObjectKey)

(s/def :cognitect.aws.s3.GetObjectRetentionRequest/VersionId :cognitect.aws.s3/ObjectVersionId)

(s/def :cognitect.aws.s3.GetObjectRetentionRequest/RequestPayer :cognitect.aws.s3/RequestPayer)

(s/def :cognitect.aws.s3.GetBucketPolicyStatusOutput/PolicyStatus :cognitect.aws.s3/PolicyStatus)

(s/def :cognitect.aws.s3.CommonPrefix/Prefix :cognitect.aws.s3/Prefix)

(s/def :cognitect.aws.s3.ObjectIdentifier/Key :cognitect.aws.s3/ObjectKey)

(s/def :cognitect.aws.s3.ObjectIdentifier/VersionId :cognitect.aws.s3/ObjectVersionId)

(s/def :cognitect.aws.s3.DeleteObjectRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.DeleteObjectRequest/Key :cognitect.aws.s3/ObjectKey)

(s/def :cognitect.aws.s3.DeleteObjectRequest/MFA :cognitect.aws.s3/MFA)

(s/def :cognitect.aws.s3.DeleteObjectRequest/VersionId :cognitect.aws.s3/ObjectVersionId)

(s/def :cognitect.aws.s3.DeleteObjectRequest/RequestPayer :cognitect.aws.s3/RequestPayer)

(s/def
  :cognitect.aws.s3.DeleteObjectRequest/BypassGovernanceRetention
  :cognitect.aws.s3/BypassGovernanceRetention)

(s/def :cognitect.aws.s3.ListObjectsOutput/Prefix :cognitect.aws.s3/Prefix)

(s/def :cognitect.aws.s3.ListObjectsOutput/EncodingType :cognitect.aws.s3/EncodingType)

(s/def :cognitect.aws.s3.ListObjectsOutput/Delimiter :cognitect.aws.s3/Delimiter)

(s/def :cognitect.aws.s3.ListObjectsOutput/Marker :cognitect.aws.s3/Marker)

(s/def :cognitect.aws.s3.ListObjectsOutput/CommonPrefixes :cognitect.aws.s3/CommonPrefixList)

(s/def :cognitect.aws.s3.ListObjectsOutput/Contents :cognitect.aws.s3/ObjectList)

(s/def :cognitect.aws.s3.ListObjectsOutput/MaxKeys :cognitect.aws.s3/MaxKeys)

(s/def :cognitect.aws.s3.ListObjectsOutput/IsTruncated :cognitect.aws.s3/IsTruncated)

(s/def :cognitect.aws.s3.ListObjectsOutput/NextMarker :cognitect.aws.s3/NextMarker)

(s/def :cognitect.aws.s3.ListObjectsOutput/Name :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.GetBucketMetricsConfigurationRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.GetBucketMetricsConfigurationRequest/Id :cognitect.aws.s3/MetricsId)

(s/def :cognitect.aws.s3.CSVOutput/QuoteFields :cognitect.aws.s3/QuoteFields)

(s/def :cognitect.aws.s3.CSVOutput/QuoteEscapeCharacter :cognitect.aws.s3/QuoteEscapeCharacter)

(s/def :cognitect.aws.s3.CSVOutput/RecordDelimiter :cognitect.aws.s3/RecordDelimiter)

(s/def :cognitect.aws.s3.CSVOutput/FieldDelimiter :cognitect.aws.s3/FieldDelimiter)

(s/def :cognitect.aws.s3.CSVOutput/QuoteCharacter :cognitect.aws.s3/QuoteCharacter)

(s/def :cognitect.aws.s3.Metrics/Status :cognitect.aws.s3/MetricsStatus)

(s/def :cognitect.aws.s3.Metrics/EventThreshold :cognitect.aws.s3/ReplicationTimeValue)

(s/def
  :cognitect.aws.s3.AnalyticsExportDestination/S3BucketDestination
  :cognitect.aws.s3/AnalyticsS3BucketDestination)

(s/def :cognitect.aws.s3.DeleteObjectsOutput/Deleted :cognitect.aws.s3/DeletedObjects)

(s/def :cognitect.aws.s3.DeleteObjectsOutput/RequestCharged :cognitect.aws.s3/RequestCharged)

(s/def :cognitect.aws.s3.DeleteObjectsOutput/Errors :cognitect.aws.s3/Errors)

(s/def :cognitect.aws.s3.PutBucketReplicationRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.PutBucketReplicationRequest/ContentMD5 :cognitect.aws.s3/ContentMD5)

(s/def
  :cognitect.aws.s3.PutBucketReplicationRequest/ReplicationConfiguration
  :cognitect.aws.s3/ReplicationConfiguration)

(s/def :cognitect.aws.s3.PutBucketReplicationRequest/Token :cognitect.aws.s3/ObjectLockToken)

(s/def :cognitect.aws.s3.CSVInput/FileHeaderInfo :cognitect.aws.s3/FileHeaderInfo)

(s/def :cognitect.aws.s3.CSVInput/Comments :cognitect.aws.s3/Comments)

(s/def :cognitect.aws.s3.CSVInput/QuoteEscapeCharacter :cognitect.aws.s3/QuoteEscapeCharacter)

(s/def :cognitect.aws.s3.CSVInput/RecordDelimiter :cognitect.aws.s3/RecordDelimiter)

(s/def :cognitect.aws.s3.CSVInput/FieldDelimiter :cognitect.aws.s3/FieldDelimiter)

(s/def :cognitect.aws.s3.CSVInput/QuoteCharacter :cognitect.aws.s3/QuoteCharacter)

(s/def
  :cognitect.aws.s3.CSVInput/AllowQuotedRecordDelimiter
  :cognitect.aws.s3/AllowQuotedRecordDelimiter)

(s/def :cognitect.aws.s3.PutObjectAclRequest/GrantWrite :cognitect.aws.s3/GrantWrite)

(s/def :cognitect.aws.s3.PutObjectAclRequest/GrantRead :cognitect.aws.s3/GrantRead)

(s/def :cognitect.aws.s3.PutObjectAclRequest/GrantReadACP :cognitect.aws.s3/GrantReadACP)

(s/def :cognitect.aws.s3.PutObjectAclRequest/ACL :cognitect.aws.s3/ObjectCannedACL)

(s/def :cognitect.aws.s3.PutObjectAclRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.PutObjectAclRequest/Key :cognitect.aws.s3/ObjectKey)

(s/def
  :cognitect.aws.s3.PutObjectAclRequest/AccessControlPolicy
  :cognitect.aws.s3/AccessControlPolicy)

(s/def :cognitect.aws.s3.PutObjectAclRequest/ContentMD5 :cognitect.aws.s3/ContentMD5)

(s/def :cognitect.aws.s3.PutObjectAclRequest/GrantWriteACP :cognitect.aws.s3/GrantWriteACP)

(s/def :cognitect.aws.s3.PutObjectAclRequest/RequestPayer :cognitect.aws.s3/RequestPayer)

(s/def :cognitect.aws.s3.PutObjectAclRequest/GrantFullControl :cognitect.aws.s3/GrantFullControl)

(s/def :cognitect.aws.s3.PutObjectAclRequest/VersionId :cognitect.aws.s3/ObjectVersionId)

(s/def :cognitect.aws.s3.SelectObjectContentEventStream/Records :cognitect.aws.s3/RecordsEvent)

(s/def :cognitect.aws.s3.SelectObjectContentEventStream/Stats :cognitect.aws.s3/StatsEvent)

(s/def :cognitect.aws.s3.SelectObjectContentEventStream/Progress :cognitect.aws.s3/ProgressEvent)

(s/def :cognitect.aws.s3.SelectObjectContentEventStream/Cont :cognitect.aws.s3/ContinuationEvent)

(s/def :cognitect.aws.s3.SelectObjectContentEventStream/End :cognitect.aws.s3/EndEvent)

(s/def :cognitect.aws.s3.GetObjectRetentionOutput/Retention :cognitect.aws.s3/ObjectLockRetention)

(s/def :cognitect.aws.s3.OutputLocation/S3 :cognitect.aws.s3/S3Location)

(s/def :cognitect.aws.s3.CreateBucketRequest/GrantWrite :cognitect.aws.s3/GrantWrite)

(s/def :cognitect.aws.s3.CreateBucketRequest/GrantRead :cognitect.aws.s3/GrantRead)

(s/def :cognitect.aws.s3.CreateBucketRequest/GrantReadACP :cognitect.aws.s3/GrantReadACP)

(s/def :cognitect.aws.s3.CreateBucketRequest/ACL :cognitect.aws.s3/BucketCannedACL)

(s/def :cognitect.aws.s3.CreateBucketRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.CreateBucketRequest/GrantWriteACP :cognitect.aws.s3/GrantWriteACP)

(s/def
  :cognitect.aws.s3.CreateBucketRequest/CreateBucketConfiguration
  :cognitect.aws.s3/CreateBucketConfiguration)

(s/def
  :cognitect.aws.s3.CreateBucketRequest/ObjectLockEnabledForBucket
  :cognitect.aws.s3/ObjectLockEnabledForBucket)

(s/def :cognitect.aws.s3.CreateBucketRequest/GrantFullControl :cognitect.aws.s3/GrantFullControl)

(s/def :cognitect.aws.s3.ObjectLockRule/DefaultRetention :cognitect.aws.s3/DefaultRetention)

(s/def :cognitect.aws.s3.PublicAccessBlockConfiguration/BlockPublicAcls :cognitect.aws.s3/Setting)

(s/def :cognitect.aws.s3.PublicAccessBlockConfiguration/IgnorePublicAcls :cognitect.aws.s3/Setting)

(s/def :cognitect.aws.s3.PublicAccessBlockConfiguration/BlockPublicPolicy :cognitect.aws.s3/Setting)

(s/def
  :cognitect.aws.s3.PublicAccessBlockConfiguration/RestrictPublicBuckets
  :cognitect.aws.s3/Setting)

(s/def :cognitect.aws.s3.AbortMultipartUploadOutput/RequestCharged :cognitect.aws.s3/RequestCharged)

(s/def :cognitect.aws.s3.AbortMultipartUploadRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.AbortMultipartUploadRequest/Key :cognitect.aws.s3/ObjectKey)

(s/def :cognitect.aws.s3.AbortMultipartUploadRequest/UploadId :cognitect.aws.s3/MultipartUploadId)

(s/def :cognitect.aws.s3.AbortMultipartUploadRequest/RequestPayer :cognitect.aws.s3/RequestPayer)

(s/def :cognitect.aws.s3.PolicyStatus/IsPublic :cognitect.aws.s3/IsPublic)

(s/def :cognitect.aws.s3.LifecycleExpiration/Date :cognitect.aws.s3/Date)

(s/def :cognitect.aws.s3.LifecycleExpiration/Days :cognitect.aws.s3/Days)

(s/def
  :cognitect.aws.s3.LifecycleExpiration/ExpiredObjectDeleteMarker
  :cognitect.aws.s3/ExpiredObjectDeleteMarker)

(s/def :cognitect.aws.s3.NotificationConfigurationFilter/Key :cognitect.aws.s3/S3KeyFilter)

(s/def :cognitect.aws.s3.CopyObjectResult/ETag :cognitect.aws.s3/ETag)

(s/def :cognitect.aws.s3.CopyObjectResult/LastModified :cognitect.aws.s3/LastModified)

(s/def
  :cognitect.aws.s3.ListBucketInventoryConfigurationsOutput/ContinuationToken
  :cognitect.aws.s3/Token)

(s/def
  :cognitect.aws.s3.ListBucketInventoryConfigurationsOutput/InventoryConfigurationList
  :cognitect.aws.s3/InventoryConfigurationList)

(s/def
  :cognitect.aws.s3.ListBucketInventoryConfigurationsOutput/IsTruncated
  :cognitect.aws.s3/IsTruncated)

(s/def
  :cognitect.aws.s3.ListBucketInventoryConfigurationsOutput/NextContinuationToken
  :cognitect.aws.s3/NextToken)

(s/def :cognitect.aws.s3.DeleteBucketEncryptionRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.PutBucketPolicyRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.PutBucketPolicyRequest/ContentMD5 :cognitect.aws.s3/ContentMD5)

(s/def
  :cognitect.aws.s3.PutBucketPolicyRequest/ConfirmRemoveSelfBucketAccess
  :cognitect.aws.s3/ConfirmRemoveSelfBucketAccess)

(s/def :cognitect.aws.s3.PutBucketPolicyRequest/Policy :cognitect.aws.s3/Policy)

(s/def :cognitect.aws.s3.CompletedMultipartUpload/Parts :cognitect.aws.s3/CompletedPartList)

(s/def :cognitect.aws.s3.SSEKMS/KeyId :cognitect.aws.s3/SSEKMSKeyId)

(s/def
  :cognitect.aws.s3.ObjectLockConfiguration/ObjectLockEnabled
  :cognitect.aws.s3/ObjectLockEnabled)

(s/def :cognitect.aws.s3.ObjectLockConfiguration/Rule :cognitect.aws.s3/ObjectLockRule)

(s/def
  :cognitect.aws.s3.GetPublicAccessBlockOutput/PublicAccessBlockConfiguration
  :cognitect.aws.s3/PublicAccessBlockConfiguration)

(s/def :cognitect.aws.s3.PutBucketEncryptionRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.PutBucketEncryptionRequest/ContentMD5 :cognitect.aws.s3/ContentMD5)

(s/def
  :cognitect.aws.s3.PutBucketEncryptionRequest/ServerSideEncryptionConfiguration
  :cognitect.aws.s3/ServerSideEncryptionConfiguration)

(s/def :cognitect.aws.s3.LifecycleRuleFilter/Prefix :cognitect.aws.s3/Prefix)

(s/def :cognitect.aws.s3.LifecycleRuleFilter/Tag :cognitect.aws.s3/Tag)

(s/def :cognitect.aws.s3.LifecycleRuleFilter/And :cognitect.aws.s3/LifecycleRuleAndOperator)

(s/def
  :cognitect.aws.s3.NotificationConfigurationDeprecated/TopicConfiguration
  :cognitect.aws.s3/TopicConfigurationDeprecated)

(s/def
  :cognitect.aws.s3.NotificationConfigurationDeprecated/QueueConfiguration
  :cognitect.aws.s3/QueueConfigurationDeprecated)

(s/def
  :cognitect.aws.s3.NotificationConfigurationDeprecated/CloudFunctionConfiguration
  :cognitect.aws.s3/CloudFunctionConfiguration)

(s/def :cognitect.aws.s3.GetObjectRequest/IfNoneMatch :cognitect.aws.s3/IfNoneMatch)

(s/def :cognitect.aws.s3.GetObjectRequest/IfUnmodifiedSince :cognitect.aws.s3/IfUnmodifiedSince)

(s/def :cognitect.aws.s3.GetObjectRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.GetObjectRequest/Key :cognitect.aws.s3/ObjectKey)

(s/def :cognitect.aws.s3.GetObjectRequest/SSECustomerKeyMD5 :cognitect.aws.s3/SSECustomerKeyMD5)

(s/def
  :cognitect.aws.s3.GetObjectRequest/ResponseContentLanguage
  :cognitect.aws.s3/ResponseContentLanguage)

(s/def :cognitect.aws.s3.GetObjectRequest/ResponseContentType :cognitect.aws.s3/ResponseContentType)

(s/def :cognitect.aws.s3.GetObjectRequest/PartNumber :cognitect.aws.s3/PartNumber)

(s/def :cognitect.aws.s3.GetObjectRequest/IfModifiedSince :cognitect.aws.s3/IfModifiedSince)

(s/def :cognitect.aws.s3.GetObjectRequest/SSECustomerKey :cognitect.aws.s3/SSECustomerKey)

(s/def :cognitect.aws.s3.GetObjectRequest/Range :cognitect.aws.s3/Range)

(s/def :cognitect.aws.s3.GetObjectRequest/IfMatch :cognitect.aws.s3/IfMatch)

(s/def :cognitect.aws.s3.GetObjectRequest/RequestPayer :cognitect.aws.s3/RequestPayer)

(s/def
  :cognitect.aws.s3.GetObjectRequest/ResponseContentEncoding
  :cognitect.aws.s3/ResponseContentEncoding)

(s/def
  :cognitect.aws.s3.GetObjectRequest/SSECustomerAlgorithm
  :cognitect.aws.s3/SSECustomerAlgorithm)

(s/def
  :cognitect.aws.s3.GetObjectRequest/ResponseCacheControl
  :cognitect.aws.s3/ResponseCacheControl)

(s/def :cognitect.aws.s3.GetObjectRequest/ResponseExpires :cognitect.aws.s3/ResponseExpires)

(s/def
  :cognitect.aws.s3.GetObjectRequest/ResponseContentDisposition
  :cognitect.aws.s3/ResponseContentDisposition)

(s/def :cognitect.aws.s3.GetObjectRequest/VersionId :cognitect.aws.s3/ObjectVersionId)

(s/def :cognitect.aws.s3.GetBucketLoggingOutput/LoggingEnabled :cognitect.aws.s3/LoggingEnabled)

(s/def :cognitect.aws.s3.NoncurrentVersionTransition/NoncurrentDays :cognitect.aws.s3/Days)

(s/def
  :cognitect.aws.s3.NoncurrentVersionTransition/StorageClass
  :cognitect.aws.s3/TransitionStorageClass)

(s/def :cognitect.aws.s3.LifecycleConfiguration/Rules :cognitect.aws.s3/Rules)

(s/def :cognitect.aws.s3.InputSerialization/CSV :cognitect.aws.s3/CSVInput)

(s/def :cognitect.aws.s3.InputSerialization/CompressionType :cognitect.aws.s3/CompressionType)

(s/def :cognitect.aws.s3.InputSerialization/JSON :cognitect.aws.s3/JSONInput)

(s/def :cognitect.aws.s3.InputSerialization/Parquet :cognitect.aws.s3/ParquetInput)

(s/def :cognitect.aws.s3.GetObjectTorrentRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.GetObjectTorrentRequest/Key :cognitect.aws.s3/ObjectKey)

(s/def :cognitect.aws.s3.GetObjectTorrentRequest/RequestPayer :cognitect.aws.s3/RequestPayer)

(s/def :cognitect.aws.s3.Transition/Date :cognitect.aws.s3/Date)

(s/def :cognitect.aws.s3.Transition/Days :cognitect.aws.s3/Days)

(s/def :cognitect.aws.s3.Transition/StorageClass :cognitect.aws.s3/TransitionStorageClass)

(s/def
  :cognitect.aws.s3.StorageClassAnalysisDataExport/OutputSchemaVersion
  :cognitect.aws.s3/StorageClassAnalysisSchemaVersion)

(s/def
  :cognitect.aws.s3.StorageClassAnalysisDataExport/Destination
  :cognitect.aws.s3/AnalyticsExportDestination)

(s/def :cognitect.aws.s3.GetBucketPolicyRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.DeletedObject/Key :cognitect.aws.s3/ObjectKey)

(s/def :cognitect.aws.s3.DeletedObject/VersionId :cognitect.aws.s3/ObjectVersionId)

(s/def :cognitect.aws.s3.DeletedObject/DeleteMarker :cognitect.aws.s3/DeleteMarker)

(s/def
  :cognitect.aws.s3.DeletedObject/DeleteMarkerVersionId
  :cognitect.aws.s3/DeleteMarkerVersionId)

(s/def :cognitect.aws.s3.ReplicationConfiguration/Role :cognitect.aws.s3/Role)

(s/def :cognitect.aws.s3.ReplicationConfiguration/Rules :cognitect.aws.s3/ReplicationRules)

(s/def
  :cognitect.aws.s3.AnalyticsS3BucketDestination/Format
  :cognitect.aws.s3/AnalyticsS3ExportFileFormat)

(s/def :cognitect.aws.s3.AnalyticsS3BucketDestination/BucketAccountId :cognitect.aws.s3/AccountId)

(s/def :cognitect.aws.s3.AnalyticsS3BucketDestination/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.AnalyticsS3BucketDestination/Prefix :cognitect.aws.s3/Prefix)

(s/def :cognitect.aws.s3.PutObjectLockConfigurationRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def
  :cognitect.aws.s3.PutObjectLockConfigurationRequest/ObjectLockConfiguration
  :cognitect.aws.s3/ObjectLockConfiguration)

(s/def
  :cognitect.aws.s3.PutObjectLockConfigurationRequest/RequestPayer
  :cognitect.aws.s3/RequestPayer)

(s/def :cognitect.aws.s3.PutObjectLockConfigurationRequest/Token :cognitect.aws.s3/ObjectLockToken)

(s/def :cognitect.aws.s3.PutObjectLockConfigurationRequest/ContentMD5 :cognitect.aws.s3/ContentMD5)

(s/def :cognitect.aws.s3.Grantee/DisplayName :cognitect.aws.s3/DisplayName)

(s/def :cognitect.aws.s3.Grantee/EmailAddress :cognitect.aws.s3/EmailAddress)

(s/def :cognitect.aws.s3.Grantee/ID :cognitect.aws.s3/ID)

(s/def :cognitect.aws.s3.Grantee/Type :cognitect.aws.s3/Type)

(s/def :cognitect.aws.s3.Grantee/URI :cognitect.aws.s3/URI)

(s/def :cognitect.aws.s3.GetObjectLockConfigurationRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.Delete/Objects :cognitect.aws.s3/ObjectIdentifierList)

(s/def :cognitect.aws.s3.Delete/Quiet :cognitect.aws.s3/Quiet)

(s/def :cognitect.aws.s3.MetricsAndOperator/Prefix :cognitect.aws.s3/Prefix)

(s/def :cognitect.aws.s3.MetricsAndOperator/Tags :cognitect.aws.s3/TagSet)

(s/def :cognitect.aws.s3.DeleteBucketCorsRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.BucketLoggingStatus/LoggingEnabled :cognitect.aws.s3/LoggingEnabled)

(s/def :cognitect.aws.s3.GetBucketWebsiteRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.ReplicationTimeValue/Minutes :cognitect.aws.s3/Minutes)

(s/def :cognitect.aws.s3.GetBucketVersioningRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.ReplicationRule/ID :cognitect.aws.s3/ID)

(s/def
  :cognitect.aws.s3.ReplicationRule/SourceSelectionCriteria
  :cognitect.aws.s3/SourceSelectionCriteria)

(s/def :cognitect.aws.s3.ReplicationRule/Prefix :cognitect.aws.s3/Prefix)

(s/def :cognitect.aws.s3.ReplicationRule/Filter :cognitect.aws.s3/ReplicationRuleFilter)

(s/def :cognitect.aws.s3.ReplicationRule/Destination :cognitect.aws.s3/Destination)

(s/def
  :cognitect.aws.s3.ReplicationRule/DeleteMarkerReplication
  :cognitect.aws.s3/DeleteMarkerReplication)

(s/def :cognitect.aws.s3.ReplicationRule/Priority :cognitect.aws.s3/Priority)

(s/def
  :cognitect.aws.s3.ReplicationRule/ExistingObjectReplication
  :cognitect.aws.s3/ExistingObjectReplication)

(s/def :cognitect.aws.s3.ReplicationRule/Status :cognitect.aws.s3/ReplicationRuleStatus)

(s/def
  :cognitect.aws.s3.UploadPartOutput/ServerSideEncryption
  :cognitect.aws.s3/ServerSideEncryption)

(s/def :cognitect.aws.s3.UploadPartOutput/ETag :cognitect.aws.s3/ETag)

(s/def
  :cognitect.aws.s3.UploadPartOutput/SSECustomerAlgorithm
  :cognitect.aws.s3/SSECustomerAlgorithm)

(s/def :cognitect.aws.s3.UploadPartOutput/SSECustomerKeyMD5 :cognitect.aws.s3/SSECustomerKeyMD5)

(s/def :cognitect.aws.s3.UploadPartOutput/SSEKMSKeyId :cognitect.aws.s3/SSEKMSKeyId)

(s/def :cognitect.aws.s3.UploadPartOutput/RequestCharged :cognitect.aws.s3/RequestCharged)

(s/def :cognitect.aws.s3.EncryptionConfiguration/ReplicaKmsKeyID :cognitect.aws.s3/ReplicaKmsKeyID)

(s/def
  :cognitect.aws.s3.ExistingObjectReplication/Status
  :cognitect.aws.s3/ExistingObjectReplicationStatus)

(s/def :cognitect.aws.s3.DeleteBucketRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.PutBucketLifecycleRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.PutBucketLifecycleRequest/ContentMD5 :cognitect.aws.s3/ContentMD5)

(s/def
  :cognitect.aws.s3.PutBucketLifecycleRequest/LifecycleConfiguration
  :cognitect.aws.s3/LifecycleConfiguration)

(s/def :cognitect.aws.s3.GetBucketLifecycleOutput/Rules :cognitect.aws.s3/Rules)

(s/def :cognitect.aws.s3.DeleteBucketLifecycleRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.PutBucketAnalyticsConfigurationRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.PutBucketAnalyticsConfigurationRequest/Id :cognitect.aws.s3/AnalyticsId)

(s/def
  :cognitect.aws.s3.PutBucketAnalyticsConfigurationRequest/AnalyticsConfiguration
  :cognitect.aws.s3/AnalyticsConfiguration)

(s/def :cognitect.aws.s3.LifecycleRule/ID :cognitect.aws.s3/ID)

(s/def :cognitect.aws.s3.LifecycleRule/Prefix :cognitect.aws.s3/Prefix)

(s/def :cognitect.aws.s3.LifecycleRule/Filter :cognitect.aws.s3/LifecycleRuleFilter)

(s/def
  :cognitect.aws.s3.LifecycleRule/NoncurrentVersionTransitions
  :cognitect.aws.s3/NoncurrentVersionTransitionList)

(s/def
  :cognitect.aws.s3.LifecycleRule/AbortIncompleteMultipartUpload
  :cognitect.aws.s3/AbortIncompleteMultipartUpload)

(s/def :cognitect.aws.s3.LifecycleRule/Transitions :cognitect.aws.s3/TransitionList)

(s/def
  :cognitect.aws.s3.LifecycleRule/NoncurrentVersionExpiration
  :cognitect.aws.s3/NoncurrentVersionExpiration)

(s/def :cognitect.aws.s3.LifecycleRule/Expiration :cognitect.aws.s3/LifecycleExpiration)

(s/def :cognitect.aws.s3.LifecycleRule/Status :cognitect.aws.s3/ExpirationStatus)

(s/def :cognitect.aws.s3.GetBucketAclOutput/Owner :cognitect.aws.s3/Owner)

(s/def :cognitect.aws.s3.GetBucketAclOutput/Grants :cognitect.aws.s3/Grants)

(s/def :cognitect.aws.s3.ObjectLockLegalHold/Status :cognitect.aws.s3/ObjectLockLegalHoldStatus)

(s/def :cognitect.aws.s3.DeleteBucketWebsiteRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.ListObjectVersionsRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.ListObjectVersionsRequest/Delimiter :cognitect.aws.s3/Delimiter)

(s/def :cognitect.aws.s3.ListObjectVersionsRequest/EncodingType :cognitect.aws.s3/EncodingType)

(s/def :cognitect.aws.s3.ListObjectVersionsRequest/KeyMarker :cognitect.aws.s3/KeyMarker)

(s/def :cognitect.aws.s3.ListObjectVersionsRequest/MaxKeys :cognitect.aws.s3/MaxKeys)

(s/def :cognitect.aws.s3.ListObjectVersionsRequest/Prefix :cognitect.aws.s3/Prefix)

(s/def
  :cognitect.aws.s3.ListObjectVersionsRequest/VersionIdMarker
  :cognitect.aws.s3/VersionIdMarker)

(s/def :cognitect.aws.s3.GetBucketTaggingRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.PutObjectRetentionOutput/RequestCharged :cognitect.aws.s3/RequestCharged)

(s/def
  :cognitect.aws.s3.CreateMultipartUploadRequest/ContentLanguage
  :cognitect.aws.s3/ContentLanguage)

(s/def :cognitect.aws.s3.CreateMultipartUploadRequest/GrantRead :cognitect.aws.s3/GrantRead)

(s/def :cognitect.aws.s3.CreateMultipartUploadRequest/GrantReadACP :cognitect.aws.s3/GrantReadACP)

(s/def :cognitect.aws.s3.CreateMultipartUploadRequest/ACL :cognitect.aws.s3/ObjectCannedACL)

(s/def :cognitect.aws.s3.CreateMultipartUploadRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.CreateMultipartUploadRequest/Key :cognitect.aws.s3/ObjectKey)

(s/def
  :cognitect.aws.s3.CreateMultipartUploadRequest/SSECustomerKeyMD5
  :cognitect.aws.s3/SSECustomerKeyMD5)

(s/def :cognitect.aws.s3.CreateMultipartUploadRequest/GrantWriteACP :cognitect.aws.s3/GrantWriteACP)

(s/def :cognitect.aws.s3.CreateMultipartUploadRequest/Tagging :cognitect.aws.s3/TaggingHeader)

(s/def
  :cognitect.aws.s3.CreateMultipartUploadRequest/ContentEncoding
  :cognitect.aws.s3/ContentEncoding)

(s/def
  :cognitect.aws.s3.CreateMultipartUploadRequest/ObjectLockMode
  :cognitect.aws.s3/ObjectLockMode)

(s/def
  :cognitect.aws.s3.CreateMultipartUploadRequest/ContentDisposition
  :cognitect.aws.s3/ContentDisposition)

(s/def
  :cognitect.aws.s3.CreateMultipartUploadRequest/SSECustomerKey
  :cognitect.aws.s3/SSECustomerKey)

(s/def :cognitect.aws.s3.CreateMultipartUploadRequest/Metadata :cognitect.aws.s3/Metadata)

(s/def
  :cognitect.aws.s3.CreateMultipartUploadRequest/SSEKMSEncryptionContext
  :cognitect.aws.s3/SSEKMSEncryptionContext)

(s/def :cognitect.aws.s3.CreateMultipartUploadRequest/RequestPayer :cognitect.aws.s3/RequestPayer)

(s/def
  :cognitect.aws.s3.CreateMultipartUploadRequest/ServerSideEncryption
  :cognitect.aws.s3/ServerSideEncryption)

(s/def
  :cognitect.aws.s3.CreateMultipartUploadRequest/SSECustomerAlgorithm
  :cognitect.aws.s3/SSECustomerAlgorithm)

(s/def :cognitect.aws.s3.CreateMultipartUploadRequest/CacheControl :cognitect.aws.s3/CacheControl)

(s/def :cognitect.aws.s3.CreateMultipartUploadRequest/ContentType :cognitect.aws.s3/ContentType)

(s/def
  :cognitect.aws.s3.CreateMultipartUploadRequest/ObjectLockRetainUntilDate
  :cognitect.aws.s3/ObjectLockRetainUntilDate)

(s/def
  :cognitect.aws.s3.CreateMultipartUploadRequest/GrantFullControl
  :cognitect.aws.s3/GrantFullControl)

(s/def :cognitect.aws.s3.CreateMultipartUploadRequest/SSEKMSKeyId :cognitect.aws.s3/SSEKMSKeyId)

(s/def
  :cognitect.aws.s3.CreateMultipartUploadRequest/WebsiteRedirectLocation
  :cognitect.aws.s3/WebsiteRedirectLocation)

(s/def
  :cognitect.aws.s3.CreateMultipartUploadRequest/ObjectLockLegalHoldStatus
  :cognitect.aws.s3/ObjectLockLegalHoldStatus)

(s/def :cognitect.aws.s3.CreateMultipartUploadRequest/StorageClass :cognitect.aws.s3/StorageClass)

(s/def :cognitect.aws.s3.CreateMultipartUploadRequest/Expires :cognitect.aws.s3/Expires)

(s/def :cognitect.aws.s3.GetBucketInventoryConfigurationRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.GetBucketInventoryConfigurationRequest/Id :cognitect.aws.s3/InventoryId)

(s/def :cognitect.aws.s3.DeleteObjectsRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.DeleteObjectsRequest/Delete :cognitect.aws.s3/Delete)

(s/def :cognitect.aws.s3.DeleteObjectsRequest/MFA :cognitect.aws.s3/MFA)

(s/def :cognitect.aws.s3.DeleteObjectsRequest/RequestPayer :cognitect.aws.s3/RequestPayer)

(s/def
  :cognitect.aws.s3.DeleteObjectsRequest/BypassGovernanceRetention
  :cognitect.aws.s3/BypassGovernanceRetention)

(s/def :cognitect.aws.s3.DeleteBucketPolicyRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.CORSConfiguration/CORSRules :cognitect.aws.s3/CORSRules)

(s/def :cognitect.aws.s3.SelectParameters/InputSerialization :cognitect.aws.s3/InputSerialization)

(s/def :cognitect.aws.s3.SelectParameters/ExpressionType :cognitect.aws.s3/ExpressionType)

(s/def :cognitect.aws.s3.SelectParameters/Expression :cognitect.aws.s3/Expression)

(s/def :cognitect.aws.s3.SelectParameters/OutputSerialization :cognitect.aws.s3/OutputSerialization)

(s/def :cognitect.aws.s3.ListMultipartUploadsRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.ListMultipartUploadsRequest/Delimiter :cognitect.aws.s3/Delimiter)

(s/def :cognitect.aws.s3.ListMultipartUploadsRequest/EncodingType :cognitect.aws.s3/EncodingType)

(s/def :cognitect.aws.s3.ListMultipartUploadsRequest/KeyMarker :cognitect.aws.s3/KeyMarker)

(s/def :cognitect.aws.s3.ListMultipartUploadsRequest/MaxUploads :cognitect.aws.s3/MaxUploads)

(s/def :cognitect.aws.s3.ListMultipartUploadsRequest/Prefix :cognitect.aws.s3/Prefix)

(s/def
  :cognitect.aws.s3.ListMultipartUploadsRequest/UploadIdMarker
  :cognitect.aws.s3/UploadIdMarker)

(s/def
  :cognitect.aws.s3.GetObjectLockConfigurationOutput/ObjectLockConfiguration
  :cognitect.aws.s3/ObjectLockConfiguration)

(s/def :cognitect.aws.s3.OutputSerialization/CSV :cognitect.aws.s3/CSVOutput)

(s/def :cognitect.aws.s3.OutputSerialization/JSON :cognitect.aws.s3/JSONOutput)

(s/def :cognitect.aws.s3.MetricsFilter/Prefix :cognitect.aws.s3/Prefix)

(s/def :cognitect.aws.s3.MetricsFilter/Tag :cognitect.aws.s3/Tag)

(s/def :cognitect.aws.s3.MetricsFilter/And :cognitect.aws.s3/MetricsAndOperator)

(s/def :cognitect.aws.s3.LifecycleRuleAndOperator/Prefix :cognitect.aws.s3/Prefix)

(s/def :cognitect.aws.s3.LifecycleRuleAndOperator/Tags :cognitect.aws.s3/TagSet)

(s/def :cognitect.aws.s3.ListPartsRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.ListPartsRequest/Key :cognitect.aws.s3/ObjectKey)

(s/def :cognitect.aws.s3.ListPartsRequest/MaxParts :cognitect.aws.s3/MaxParts)

(s/def :cognitect.aws.s3.ListPartsRequest/PartNumberMarker :cognitect.aws.s3/PartNumberMarker)

(s/def :cognitect.aws.s3.ListPartsRequest/UploadId :cognitect.aws.s3/MultipartUploadId)

(s/def :cognitect.aws.s3.ListPartsRequest/RequestPayer :cognitect.aws.s3/RequestPayer)

(s/def :cognitect.aws.s3.PutObjectRetentionRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.PutObjectRetentionRequest/Key :cognitect.aws.s3/ObjectKey)

(s/def :cognitect.aws.s3.PutObjectRetentionRequest/Retention :cognitect.aws.s3/ObjectLockRetention)

(s/def :cognitect.aws.s3.PutObjectRetentionRequest/RequestPayer :cognitect.aws.s3/RequestPayer)

(s/def :cognitect.aws.s3.PutObjectRetentionRequest/VersionId :cognitect.aws.s3/ObjectVersionId)

(s/def
  :cognitect.aws.s3.PutObjectRetentionRequest/BypassGovernanceRetention
  :cognitect.aws.s3/BypassGovernanceRetention)

(s/def :cognitect.aws.s3.PutObjectRetentionRequest/ContentMD5 :cognitect.aws.s3/ContentMD5)

(s/def :cognitect.aws.s3.SelectObjectContentRequest/ScanRange :cognitect.aws.s3/ScanRange)

(s/def :cognitect.aws.s3.SelectObjectContentRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.SelectObjectContentRequest/Key :cognitect.aws.s3/ObjectKey)

(s/def
  :cognitect.aws.s3.SelectObjectContentRequest/SSECustomerKeyMD5
  :cognitect.aws.s3/SSECustomerKeyMD5)

(s/def :cognitect.aws.s3.SelectObjectContentRequest/ExpressionType :cognitect.aws.s3/ExpressionType)

(s/def :cognitect.aws.s3.SelectObjectContentRequest/Expression :cognitect.aws.s3/Expression)

(s/def :cognitect.aws.s3.SelectObjectContentRequest/SSECustomerKey :cognitect.aws.s3/SSECustomerKey)

(s/def
  :cognitect.aws.s3.SelectObjectContentRequest/InputSerialization
  :cognitect.aws.s3/InputSerialization)

(s/def
  :cognitect.aws.s3.SelectObjectContentRequest/SSECustomerAlgorithm
  :cognitect.aws.s3/SSECustomerAlgorithm)

(s/def
  :cognitect.aws.s3.SelectObjectContentRequest/OutputSerialization
  :cognitect.aws.s3/OutputSerialization)

(s/def
  :cognitect.aws.s3.SelectObjectContentRequest/RequestProgress
  :cognitect.aws.s3/RequestProgress)

(s/def :cognitect.aws.s3.DeleteObjectTaggingOutput/VersionId :cognitect.aws.s3/ObjectVersionId)

(s/def
  :cognitect.aws.s3.InventoryDestination/S3BucketDestination
  :cognitect.aws.s3/InventoryS3BucketDestination)

(s/def
  :cognitect.aws.s3.GetBucketLocationOutput/LocationConstraint
  :cognitect.aws.s3/BucketLocationConstraint)

(s/def :cognitect.aws.s3.GetObjectTaggingRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.GetObjectTaggingRequest/Key :cognitect.aws.s3/ObjectKey)

(s/def :cognitect.aws.s3.GetObjectTaggingRequest/VersionId :cognitect.aws.s3/ObjectVersionId)

(s/def :cognitect.aws.s3.UploadPartRequest/UploadId :cognitect.aws.s3/MultipartUploadId)

(s/def :cognitect.aws.s3.UploadPartRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.UploadPartRequest/Key :cognitect.aws.s3/ObjectKey)

(s/def :cognitect.aws.s3.UploadPartRequest/ContentMD5 :cognitect.aws.s3/ContentMD5)

(s/def :cognitect.aws.s3.UploadPartRequest/SSECustomerKeyMD5 :cognitect.aws.s3/SSECustomerKeyMD5)

(s/def :cognitect.aws.s3.UploadPartRequest/PartNumber :cognitect.aws.s3/PartNumber)

(s/def :cognitect.aws.s3.UploadPartRequest/Body :cognitect.aws.s3/Body)

(s/def :cognitect.aws.s3.UploadPartRequest/SSECustomerKey :cognitect.aws.s3/SSECustomerKey)

(s/def :cognitect.aws.s3.UploadPartRequest/RequestPayer :cognitect.aws.s3/RequestPayer)

(s/def :cognitect.aws.s3.UploadPartRequest/ContentLength :cognitect.aws.s3/ContentLength)

(s/def
  :cognitect.aws.s3.UploadPartRequest/SSECustomerAlgorithm
  :cognitect.aws.s3/SSECustomerAlgorithm)

(s/def :cognitect.aws.s3.HeadObjectOutput/RequestCharged :cognitect.aws.s3/RequestCharged)

(s/def :cognitect.aws.s3.HeadObjectOutput/LastModified :cognitect.aws.s3/LastModified)

(s/def :cognitect.aws.s3.HeadObjectOutput/PartsCount :cognitect.aws.s3/PartsCount)

(s/def :cognitect.aws.s3.HeadObjectOutput/ETag :cognitect.aws.s3/ETag)

(s/def :cognitect.aws.s3.HeadObjectOutput/ContentLanguage :cognitect.aws.s3/ContentLanguage)

(s/def :cognitect.aws.s3.HeadObjectOutput/ReplicationStatus :cognitect.aws.s3/ReplicationStatus)

(s/def :cognitect.aws.s3.HeadObjectOutput/SSECustomerKeyMD5 :cognitect.aws.s3/SSECustomerKeyMD5)

(s/def :cognitect.aws.s3.HeadObjectOutput/ContentEncoding :cognitect.aws.s3/ContentEncoding)

(s/def :cognitect.aws.s3.HeadObjectOutput/ObjectLockMode :cognitect.aws.s3/ObjectLockMode)

(s/def :cognitect.aws.s3.HeadObjectOutput/ContentDisposition :cognitect.aws.s3/ContentDisposition)

(s/def :cognitect.aws.s3.HeadObjectOutput/Metadata :cognitect.aws.s3/Metadata)

(s/def
  :cognitect.aws.s3.HeadObjectOutput/ServerSideEncryption
  :cognitect.aws.s3/ServerSideEncryption)

(s/def :cognitect.aws.s3.HeadObjectOutput/Restore :cognitect.aws.s3/Restore)

(s/def :cognitect.aws.s3.HeadObjectOutput/ContentLength :cognitect.aws.s3/ContentLength)

(s/def :cognitect.aws.s3.HeadObjectOutput/DeleteMarker :cognitect.aws.s3/DeleteMarker)

(s/def
  :cognitect.aws.s3.HeadObjectOutput/SSECustomerAlgorithm
  :cognitect.aws.s3/SSECustomerAlgorithm)

(s/def :cognitect.aws.s3.HeadObjectOutput/CacheControl :cognitect.aws.s3/CacheControl)

(s/def :cognitect.aws.s3.HeadObjectOutput/ContentType :cognitect.aws.s3/ContentType)

(s/def :cognitect.aws.s3.HeadObjectOutput/AcceptRanges :cognitect.aws.s3/AcceptRanges)

(s/def
  :cognitect.aws.s3.HeadObjectOutput/ObjectLockRetainUntilDate
  :cognitect.aws.s3/ObjectLockRetainUntilDate)

(s/def :cognitect.aws.s3.HeadObjectOutput/SSEKMSKeyId :cognitect.aws.s3/SSEKMSKeyId)

(s/def
  :cognitect.aws.s3.HeadObjectOutput/WebsiteRedirectLocation
  :cognitect.aws.s3/WebsiteRedirectLocation)

(s/def :cognitect.aws.s3.HeadObjectOutput/MissingMeta :cognitect.aws.s3/MissingMeta)

(s/def
  :cognitect.aws.s3.HeadObjectOutput/ObjectLockLegalHoldStatus
  :cognitect.aws.s3/ObjectLockLegalHoldStatus)

(s/def :cognitect.aws.s3.HeadObjectOutput/StorageClass :cognitect.aws.s3/StorageClass)

(s/def :cognitect.aws.s3.HeadObjectOutput/Expiration :cognitect.aws.s3/Expiration)

(s/def :cognitect.aws.s3.HeadObjectOutput/VersionId :cognitect.aws.s3/ObjectVersionId)

(s/def :cognitect.aws.s3.HeadObjectOutput/Expires :cognitect.aws.s3/Expires)

(s/def :cognitect.aws.s3.Error/Key :cognitect.aws.s3/ObjectKey)

(s/def :cognitect.aws.s3.Error/VersionId :cognitect.aws.s3/ObjectVersionId)

(s/def :cognitect.aws.s3.Error/Code :cognitect.aws.s3/Code)

(s/def :cognitect.aws.s3.Error/Message :cognitect.aws.s3/Message)

(s/def :cognitect.aws.s3.PutBucketLoggingRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def
  :cognitect.aws.s3.PutBucketLoggingRequest/BucketLoggingStatus
  :cognitect.aws.s3/BucketLoggingStatus)

(s/def :cognitect.aws.s3.PutBucketLoggingRequest/ContentMD5 :cognitect.aws.s3/ContentMD5)

(s/def :cognitect.aws.s3.DeleteBucketReplicationRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.GetBucketLifecycleConfigurationRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.AnalyticsConfiguration/Id :cognitect.aws.s3/AnalyticsId)

(s/def :cognitect.aws.s3.AnalyticsConfiguration/Filter :cognitect.aws.s3/AnalyticsFilter)

(s/def
  :cognitect.aws.s3.AnalyticsConfiguration/StorageClassAnalysis
  :cognitect.aws.s3/StorageClassAnalysis)

(s/def
  :cognitect.aws.s3.Condition/HttpErrorCodeReturnedEquals
  :cognitect.aws.s3/HttpErrorCodeReturnedEquals)

(s/def :cognitect.aws.s3.Condition/KeyPrefixEquals :cognitect.aws.s3/KeyPrefixEquals)

(s/def
  :cognitect.aws.s3.PutObjectLockConfigurationOutput/RequestCharged
  :cognitect.aws.s3/RequestCharged)

(s/def :cognitect.aws.s3.CopyPartResult/ETag :cognitect.aws.s3/ETag)

(s/def :cognitect.aws.s3.CopyPartResult/LastModified :cognitect.aws.s3/LastModified)

(s/def :cognitect.aws.s3.Progress/BytesScanned :cognitect.aws.s3/BytesScanned)

(s/def :cognitect.aws.s3.Progress/BytesProcessed :cognitect.aws.s3/BytesProcessed)

(s/def :cognitect.aws.s3.Progress/BytesReturned :cognitect.aws.s3/BytesReturned)

(s/def :cognitect.aws.s3.GetBucketPolicyOutput/Policy :cognitect.aws.s3/Policy)

(s/def
  :cognitect.aws.s3.ListBucketAnalyticsConfigurationsOutput/IsTruncated
  :cognitect.aws.s3/IsTruncated)

(s/def
  :cognitect.aws.s3.ListBucketAnalyticsConfigurationsOutput/ContinuationToken
  :cognitect.aws.s3/Token)

(s/def
  :cognitect.aws.s3.ListBucketAnalyticsConfigurationsOutput/NextContinuationToken
  :cognitect.aws.s3/NextToken)

(s/def
  :cognitect.aws.s3.ListBucketAnalyticsConfigurationsOutput/AnalyticsConfigurationList
  :cognitect.aws.s3/AnalyticsConfigurationList)

(s/def :cognitect.aws.s3.PutObjectLegalHoldRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.PutObjectLegalHoldRequest/Key :cognitect.aws.s3/ObjectKey)

(s/def :cognitect.aws.s3.PutObjectLegalHoldRequest/LegalHold :cognitect.aws.s3/ObjectLockLegalHold)

(s/def :cognitect.aws.s3.PutObjectLegalHoldRequest/RequestPayer :cognitect.aws.s3/RequestPayer)

(s/def :cognitect.aws.s3.PutObjectLegalHoldRequest/VersionId :cognitect.aws.s3/ObjectVersionId)

(s/def :cognitect.aws.s3.PutObjectLegalHoldRequest/ContentMD5 :cognitect.aws.s3/ContentMD5)

(s/def :cognitect.aws.s3.AnalyticsAndOperator/Prefix :cognitect.aws.s3/Prefix)

(s/def :cognitect.aws.s3.AnalyticsAndOperator/Tags :cognitect.aws.s3/TagSet)

(s/def :cognitect.aws.s3.Object/Key :cognitect.aws.s3/ObjectKey)

(s/def :cognitect.aws.s3.Object/LastModified :cognitect.aws.s3/LastModified)

(s/def :cognitect.aws.s3.Object/ETag :cognitect.aws.s3/ETag)

(s/def :cognitect.aws.s3.Object/Size :cognitect.aws.s3/Size)

(s/def :cognitect.aws.s3.Object/StorageClass :cognitect.aws.s3/ObjectStorageClass)

(s/def :cognitect.aws.s3.Object/Owner :cognitect.aws.s3/Owner)

(s/def :cognitect.aws.s3.NoncurrentVersionExpiration/NoncurrentDays :cognitect.aws.s3/Days)

(s/def :cognitect.aws.s3.ErrorDocument/Key :cognitect.aws.s3/ObjectKey)

(s/def :cognitect.aws.s3.ListObjectsV2Output/Prefix :cognitect.aws.s3/Prefix)

(s/def :cognitect.aws.s3.ListObjectsV2Output/StartAfter :cognitect.aws.s3/StartAfter)

(s/def :cognitect.aws.s3.ListObjectsV2Output/EncodingType :cognitect.aws.s3/EncodingType)

(s/def :cognitect.aws.s3.ListObjectsV2Output/Delimiter :cognitect.aws.s3/Delimiter)

(s/def :cognitect.aws.s3.ListObjectsV2Output/NextContinuationToken :cognitect.aws.s3/NextToken)

(s/def :cognitect.aws.s3.ListObjectsV2Output/CommonPrefixes :cognitect.aws.s3/CommonPrefixList)

(s/def :cognitect.aws.s3.ListObjectsV2Output/ContinuationToken :cognitect.aws.s3/Token)

(s/def :cognitect.aws.s3.ListObjectsV2Output/Contents :cognitect.aws.s3/ObjectList)

(s/def :cognitect.aws.s3.ListObjectsV2Output/MaxKeys :cognitect.aws.s3/MaxKeys)

(s/def :cognitect.aws.s3.ListObjectsV2Output/IsTruncated :cognitect.aws.s3/IsTruncated)

(s/def :cognitect.aws.s3.ListObjectsV2Output/Name :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.ListObjectsV2Output/KeyCount :cognitect.aws.s3/KeyCount)

(s/def :cognitect.aws.s3.MultipartUpload/UploadId :cognitect.aws.s3/MultipartUploadId)

(s/def :cognitect.aws.s3.MultipartUpload/Key :cognitect.aws.s3/ObjectKey)

(s/def :cognitect.aws.s3.MultipartUpload/Initiated :cognitect.aws.s3/Initiated)

(s/def :cognitect.aws.s3.MultipartUpload/StorageClass :cognitect.aws.s3/StorageClass)

(s/def :cognitect.aws.s3.MultipartUpload/Owner :cognitect.aws.s3/Owner)

(s/def :cognitect.aws.s3.MultipartUpload/Initiator :cognitect.aws.s3/Initiator)

(s/def :cognitect.aws.s3.PutObjectRequest/ContentLanguage :cognitect.aws.s3/ContentLanguage)

(s/def :cognitect.aws.s3.PutObjectRequest/GrantRead :cognitect.aws.s3/GrantRead)

(s/def :cognitect.aws.s3.PutObjectRequest/GrantReadACP :cognitect.aws.s3/GrantReadACP)

(s/def :cognitect.aws.s3.PutObjectRequest/ACL :cognitect.aws.s3/ObjectCannedACL)

(s/def :cognitect.aws.s3.PutObjectRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.PutObjectRequest/Key :cognitect.aws.s3/ObjectKey)

(s/def :cognitect.aws.s3.PutObjectRequest/ContentMD5 :cognitect.aws.s3/ContentMD5)

(s/def :cognitect.aws.s3.PutObjectRequest/SSECustomerKeyMD5 :cognitect.aws.s3/SSECustomerKeyMD5)

(s/def :cognitect.aws.s3.PutObjectRequest/GrantWriteACP :cognitect.aws.s3/GrantWriteACP)

(s/def :cognitect.aws.s3.PutObjectRequest/Tagging :cognitect.aws.s3/TaggingHeader)

(s/def :cognitect.aws.s3.PutObjectRequest/ContentEncoding :cognitect.aws.s3/ContentEncoding)

(s/def :cognitect.aws.s3.PutObjectRequest/ObjectLockMode :cognitect.aws.s3/ObjectLockMode)

(s/def :cognitect.aws.s3.PutObjectRequest/Body :cognitect.aws.s3/Body)

(s/def :cognitect.aws.s3.PutObjectRequest/ContentDisposition :cognitect.aws.s3/ContentDisposition)

(s/def :cognitect.aws.s3.PutObjectRequest/SSECustomerKey :cognitect.aws.s3/SSECustomerKey)

(s/def :cognitect.aws.s3.PutObjectRequest/Metadata :cognitect.aws.s3/Metadata)

(s/def
  :cognitect.aws.s3.PutObjectRequest/SSEKMSEncryptionContext
  :cognitect.aws.s3/SSEKMSEncryptionContext)

(s/def :cognitect.aws.s3.PutObjectRequest/RequestPayer :cognitect.aws.s3/RequestPayer)

(s/def
  :cognitect.aws.s3.PutObjectRequest/ServerSideEncryption
  :cognitect.aws.s3/ServerSideEncryption)

(s/def :cognitect.aws.s3.PutObjectRequest/ContentLength :cognitect.aws.s3/ContentLength)

(s/def
  :cognitect.aws.s3.PutObjectRequest/SSECustomerAlgorithm
  :cognitect.aws.s3/SSECustomerAlgorithm)

(s/def :cognitect.aws.s3.PutObjectRequest/CacheControl :cognitect.aws.s3/CacheControl)

(s/def :cognitect.aws.s3.PutObjectRequest/ContentType :cognitect.aws.s3/ContentType)

(s/def
  :cognitect.aws.s3.PutObjectRequest/ObjectLockRetainUntilDate
  :cognitect.aws.s3/ObjectLockRetainUntilDate)

(s/def :cognitect.aws.s3.PutObjectRequest/GrantFullControl :cognitect.aws.s3/GrantFullControl)

(s/def :cognitect.aws.s3.PutObjectRequest/SSEKMSKeyId :cognitect.aws.s3/SSEKMSKeyId)

(s/def
  :cognitect.aws.s3.PutObjectRequest/WebsiteRedirectLocation
  :cognitect.aws.s3/WebsiteRedirectLocation)

(s/def
  :cognitect.aws.s3.PutObjectRequest/ObjectLockLegalHoldStatus
  :cognitect.aws.s3/ObjectLockLegalHoldStatus)

(s/def :cognitect.aws.s3.PutObjectRequest/StorageClass :cognitect.aws.s3/StorageClass)

(s/def :cognitect.aws.s3.PutObjectRequest/Expires :cognitect.aws.s3/Expires)

(s/def :cognitect.aws.s3.RestoreRequest/Days :cognitect.aws.s3/Days)

(s/def :cognitect.aws.s3.RestoreRequest/GlacierJobParameters :cognitect.aws.s3/GlacierJobParameters)

(s/def :cognitect.aws.s3.RestoreRequest/Type :cognitect.aws.s3/RestoreRequestType)

(s/def :cognitect.aws.s3.RestoreRequest/Tier :cognitect.aws.s3/Tier)

(s/def :cognitect.aws.s3.RestoreRequest/Description :cognitect.aws.s3/Description)

(s/def :cognitect.aws.s3.RestoreRequest/SelectParameters :cognitect.aws.s3/SelectParameters)

(s/def :cognitect.aws.s3.RestoreRequest/OutputLocation :cognitect.aws.s3/OutputLocation)

(s/def :cognitect.aws.s3.TargetGrant/Grantee :cognitect.aws.s3/Grantee)

(s/def :cognitect.aws.s3.TargetGrant/Permission :cognitect.aws.s3/BucketLogsPermission)

(s/def :cognitect.aws.s3.ReplicationRuleAndOperator/Prefix :cognitect.aws.s3/Prefix)

(s/def :cognitect.aws.s3.ReplicationRuleAndOperator/Tags :cognitect.aws.s3/TagSet)

(s/def
  :cognitect.aws.s3.ListObjectVersionsOutput/NextVersionIdMarker
  :cognitect.aws.s3/NextVersionIdMarker)

(s/def :cognitect.aws.s3.ListObjectVersionsOutput/Prefix :cognitect.aws.s3/Prefix)

(s/def :cognitect.aws.s3.ListObjectVersionsOutput/DeleteMarkers :cognitect.aws.s3/DeleteMarkers)

(s/def :cognitect.aws.s3.ListObjectVersionsOutput/NextKeyMarker :cognitect.aws.s3/NextKeyMarker)

(s/def :cognitect.aws.s3.ListObjectVersionsOutput/EncodingType :cognitect.aws.s3/EncodingType)

(s/def :cognitect.aws.s3.ListObjectVersionsOutput/Versions :cognitect.aws.s3/ObjectVersionList)

(s/def :cognitect.aws.s3.ListObjectVersionsOutput/Delimiter :cognitect.aws.s3/Delimiter)

(s/def :cognitect.aws.s3.ListObjectVersionsOutput/KeyMarker :cognitect.aws.s3/KeyMarker)

(s/def :cognitect.aws.s3.ListObjectVersionsOutput/CommonPrefixes :cognitect.aws.s3/CommonPrefixList)

(s/def :cognitect.aws.s3.ListObjectVersionsOutput/MaxKeys :cognitect.aws.s3/MaxKeys)

(s/def :cognitect.aws.s3.ListObjectVersionsOutput/IsTruncated :cognitect.aws.s3/IsTruncated)

(s/def :cognitect.aws.s3.ListObjectVersionsOutput/Name :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.ListObjectVersionsOutput/VersionIdMarker :cognitect.aws.s3/VersionIdMarker)

(s/def :cognitect.aws.s3.PutBucketWebsiteRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.PutBucketWebsiteRequest/ContentMD5 :cognitect.aws.s3/ContentMD5)

(s/def
  :cognitect.aws.s3.PutBucketWebsiteRequest/WebsiteConfiguration
  :cognitect.aws.s3/WebsiteConfiguration)

(s/def :cognitect.aws.s3.WebsiteConfiguration/ErrorDocument :cognitect.aws.s3/ErrorDocument)

(s/def :cognitect.aws.s3.WebsiteConfiguration/IndexDocument :cognitect.aws.s3/IndexDocument)

(s/def
  :cognitect.aws.s3.WebsiteConfiguration/RedirectAllRequestsTo
  :cognitect.aws.s3/RedirectAllRequestsTo)

(s/def :cognitect.aws.s3.WebsiteConfiguration/RoutingRules :cognitect.aws.s3/RoutingRules)

(s/def
  :cognitect.aws.s3.GetBucketInventoryConfigurationOutput/InventoryConfiguration
  :cognitect.aws.s3/InventoryConfiguration)

(s/def :cognitect.aws.s3.Redirect/HostName :cognitect.aws.s3/HostName)

(s/def :cognitect.aws.s3.Redirect/HttpRedirectCode :cognitect.aws.s3/HttpRedirectCode)

(s/def :cognitect.aws.s3.Redirect/Protocol :cognitect.aws.s3/Protocol)

(s/def :cognitect.aws.s3.Redirect/ReplaceKeyPrefixWith :cognitect.aws.s3/ReplaceKeyPrefixWith)

(s/def :cognitect.aws.s3.Redirect/ReplaceKeyWith :cognitect.aws.s3/ReplaceKeyWith)

(s/def :cognitect.aws.s3.GetBucketAclRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def
  :cognitect.aws.s3.GetBucketAccelerateConfigurationOutput/Status
  :cognitect.aws.s3/BucketAccelerateStatus)

(s/def :cognitect.aws.s3.DeleteObjectOutput/DeleteMarker :cognitect.aws.s3/DeleteMarker)

(s/def :cognitect.aws.s3.DeleteObjectOutput/VersionId :cognitect.aws.s3/ObjectVersionId)

(s/def :cognitect.aws.s3.DeleteObjectOutput/RequestCharged :cognitect.aws.s3/RequestCharged)

(s/def
  :cognitect.aws.s3.ServerSideEncryptionConfiguration/Rules
  :cognitect.aws.s3/ServerSideEncryptionRules)

(s/def :cognitect.aws.s3.InventoryEncryption/SSES3 :cognitect.aws.s3/SSES3)

(s/def :cognitect.aws.s3.InventoryEncryption/SSEKMS :cognitect.aws.s3/SSEKMS)

(s/def :cognitect.aws.s3.RecordsEvent/Payload :cognitect.aws.s3/Body)

(s/def :cognitect.aws.s3.GetObjectAclOutput/Owner :cognitect.aws.s3/Owner)

(s/def :cognitect.aws.s3.GetObjectAclOutput/Grants :cognitect.aws.s3/Grants)

(s/def :cognitect.aws.s3.GetObjectAclOutput/RequestCharged :cognitect.aws.s3/RequestCharged)

(s/def :cognitect.aws.s3.GetBucketLocationRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.GetBucketRequestPaymentRequest/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.GetBucketTaggingOutput/TagSet :cognitect.aws.s3/TagSet)

(s/def
  :cognitect.aws.s3.ListBucketInventoryConfigurationsRequest/Bucket
  :cognitect.aws.s3/BucketName)

(s/def
  :cognitect.aws.s3.ListBucketInventoryConfigurationsRequest/ContinuationToken
  :cognitect.aws.s3/Token)

(s/def :cognitect.aws.s3.PutObjectAclOutput/RequestCharged :cognitect.aws.s3/RequestCharged)

(s/def :cognitect.aws.s3.AnalyticsFilter/Prefix :cognitect.aws.s3/Prefix)

(s/def :cognitect.aws.s3.AnalyticsFilter/Tag :cognitect.aws.s3/Tag)

(s/def :cognitect.aws.s3.AnalyticsFilter/And :cognitect.aws.s3/AnalyticsAndOperator)

(s/def :cognitect.aws.s3.Part/PartNumber :cognitect.aws.s3/PartNumber)

(s/def :cognitect.aws.s3.Part/LastModified :cognitect.aws.s3/LastModified)

(s/def :cognitect.aws.s3.Part/ETag :cognitect.aws.s3/ETag)

(s/def :cognitect.aws.s3.Part/Size :cognitect.aws.s3/Size)

(s/def :cognitect.aws.s3.ReplicationTime/Status :cognitect.aws.s3/ReplicationTimeStatus)

(s/def :cognitect.aws.s3.ReplicationTime/Time :cognitect.aws.s3/ReplicationTimeValue)

(s/def :cognitect.aws.s3.RequestProgress/Enabled :cognitect.aws.s3/EnableRequestProgress)

(s/def
  :cognitect.aws.s3.GetBucketLifecycleConfigurationOutput/Rules
  :cognitect.aws.s3/LifecycleRules)

(s/def :cognitect.aws.s3.ListMultipartUploadsOutput/Prefix :cognitect.aws.s3/Prefix)

(s/def :cognitect.aws.s3.ListMultipartUploadsOutput/Uploads :cognitect.aws.s3/MultipartUploadList)

(s/def :cognitect.aws.s3.ListMultipartUploadsOutput/UploadIdMarker :cognitect.aws.s3/UploadIdMarker)

(s/def :cognitect.aws.s3.ListMultipartUploadsOutput/Bucket :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.ListMultipartUploadsOutput/NextKeyMarker :cognitect.aws.s3/NextKeyMarker)

(s/def :cognitect.aws.s3.ListMultipartUploadsOutput/EncodingType :cognitect.aws.s3/EncodingType)

(s/def :cognitect.aws.s3.ListMultipartUploadsOutput/Delimiter :cognitect.aws.s3/Delimiter)

(s/def :cognitect.aws.s3.ListMultipartUploadsOutput/MaxUploads :cognitect.aws.s3/MaxUploads)

(s/def :cognitect.aws.s3.ListMultipartUploadsOutput/KeyMarker :cognitect.aws.s3/KeyMarker)

(s/def
  :cognitect.aws.s3.ListMultipartUploadsOutput/CommonPrefixes
  :cognitect.aws.s3/CommonPrefixList)

(s/def :cognitect.aws.s3.ListMultipartUploadsOutput/IsTruncated :cognitect.aws.s3/IsTruncated)

(s/def
  :cognitect.aws.s3.ListMultipartUploadsOutput/NextUploadIdMarker
  :cognitect.aws.s3/NextUploadIdMarker)

(s/def
  :cognitect.aws.s3.DeleteBucketInventoryConfigurationRequest/Bucket
  :cognitect.aws.s3/BucketName)

(s/def :cognitect.aws.s3.DeleteBucketInventoryConfigurationRequest/Id :cognitect.aws.s3/InventoryId)

