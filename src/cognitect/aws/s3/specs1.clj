(ns cognitect.aws.s3.specs1)

;; Copyright (c) Cognitect, Inc.
;; All rights reserved.

(ns cognitect.aws.s3.specs
  (:require [clojure.spec.alpha :as s] [clojure.spec.gen.alpha :as gen]))

(s/def :cognitect.aws/client map?)

(s/def :core.async/channel any?)

(s/def :cognitect.aws.s3/JSONInput (s/keys :opt-un [:cognitect.aws.s3.JSONInput/Type]))

(s/def
  :cognitect.aws.s3/CompleteMultipartUploadRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.CompleteMultipartUploadRequest/Bucket
     :cognitect.aws.s3.CompleteMultipartUploadRequest/Key
     :cognitect.aws.s3.CompleteMultipartUploadRequest/UploadId]
    :opt-un
    [:cognitect.aws.s3.CompleteMultipartUploadRequest/MultipartUpload
     :cognitect.aws.s3.CompleteMultipartUploadRequest/RequestPayer]))

(s/def :cognitect.aws.s3/QuoteCharacter string?)

(s/def
  :cognitect.aws.s3/ObjectLockRetention
  (s/keys
    :opt-un
    [:cognitect.aws.s3.ObjectLockRetention/RetainUntilDate
     :cognitect.aws.s3.ObjectLockRetention/Mode]))

(s/def :cognitect.aws.s3/ReplaceKeyWith string?)

(s/def
  :cognitect.aws.s3/ReplicationRuleFilter
  (s/keys
    :opt-un
    [:cognitect.aws.s3.ReplicationRuleFilter/And
     :cognitect.aws.s3.ReplicationRuleFilter/Prefix
     :cognitect.aws.s3.ReplicationRuleFilter/Tag]))

(s/def :cognitect.aws.s3/QueueArn string?)

(s/def :cognitect.aws.s3/ID string?)

(s/def
  :cognitect.aws.s3/PutObjectTaggingOutput
  (s/keys :opt-un [:cognitect.aws.s3.PutObjectTaggingOutput/VersionId]))

(s/def
  :cognitect.aws.s3/PutPublicAccessBlockRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.PutPublicAccessBlockRequest/Bucket
     :cognitect.aws.s3.PutPublicAccessBlockRequest/PublicAccessBlockConfiguration]
    :opt-un
    [:cognitect.aws.s3.PutPublicAccessBlockRequest/ContentMD5]))

(s/def :cognitect.aws.s3/MFADelete (s/spec string? :gen #(s/gen #{"Disabled" "Enabled"})))

(s/def
  :cognitect.aws.s3/DeleteBucketTaggingRequest
  (s/keys :req-un [:cognitect.aws.s3.DeleteBucketTaggingRequest/Bucket]))

(s/def
  :cognitect.aws.s3/MetricsConfigurationList
  (s/coll-of :cognitect.aws.s3/MetricsConfiguration))

(s/def
  :cognitect.aws.s3/PutBucketRequestPaymentRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.PutBucketRequestPaymentRequest/Bucket
     :cognitect.aws.s3.PutBucketRequestPaymentRequest/RequestPaymentConfiguration]
    :opt-un
    [:cognitect.aws.s3.PutBucketRequestPaymentRequest/ContentMD5]))

(s/def :cognitect.aws.s3/MetadataKey string?)

(s/def :cognitect.aws.s3/IsEnabled boolean?)

(s/def :cognitect.aws.s3/IfNoneMatch string?)

(s/def :cognitect.aws.s3/LocationPrefix string?)

(s/def
  :cognitect.aws.s3/SourceSelectionCriteria
  (s/keys :opt-un [:cognitect.aws.s3.SourceSelectionCriteria/SseKmsEncryptedObjects]))

(s/def
  :cognitect.aws.s3/ListBucketAnalyticsConfigurationsRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.ListBucketAnalyticsConfigurationsRequest/Bucket]
    :opt-un
    [:cognitect.aws.s3.ListBucketAnalyticsConfigurationsRequest/ContinuationToken]))

(s/def
  :cognitect.aws.s3/MaxAgeSeconds
  (s/spec int? :gen #(gen/choose Long/MIN_VALUE Long/MAX_VALUE)))

(s/def :cognitect.aws.s3/CompressionType (s/spec string? :gen #(s/gen #{"GZIP" "NONE" "BZIP2"})))

(s/def
  :cognitect.aws.s3/PutBucketTaggingRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.PutBucketTaggingRequest/Bucket
     :cognitect.aws.s3.PutBucketTaggingRequest/Tagging]
    :opt-un
    [:cognitect.aws.s3.PutBucketTaggingRequest/ContentMD5]))

(s/def
  :cognitect.aws.s3/RestoreObjectRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.RestoreObjectRequest/Bucket :cognitect.aws.s3.RestoreObjectRequest/Key]
    :opt-un
    [:cognitect.aws.s3.RestoreObjectRequest/VersionId
     :cognitect.aws.s3.RestoreObjectRequest/RestoreRequest
     :cognitect.aws.s3.RestoreObjectRequest/RequestPayer]))

(s/def
  :cognitect.aws.s3/LambdaFunctionConfiguration
  (s/keys
    :req-un
    [:cognitect.aws.s3.LambdaFunctionConfiguration/LambdaFunctionArn
     :cognitect.aws.s3.LambdaFunctionConfiguration/Events]
    :opt-un
    [:cognitect.aws.s3.LambdaFunctionConfiguration/Filter
     :cognitect.aws.s3.LambdaFunctionConfiguration/Id]))

(s/def :cognitect.aws.s3/NextVersionIdMarker string?)

(s/def
  :cognitect.aws.s3/InventoryConfigurationList
  (s/coll-of :cognitect.aws.s3/InventoryConfiguration))

(s/def
  :cognitect.aws.s3/TopicConfigurationDeprecated
  (s/keys
    :opt-un
    [:cognitect.aws.s3.TopicConfigurationDeprecated/Events
     :cognitect.aws.s3.TopicConfigurationDeprecated/Event
     :cognitect.aws.s3.TopicConfigurationDeprecated/Topic
     :cognitect.aws.s3.TopicConfigurationDeprecated/Id]))

(s/def :cognitect.aws.s3/GrantWrite string?)

(s/def
  :cognitect.aws.s3/ObjectLockRetentionMode
  (s/spec string? :gen #(s/gen #{"GOVERNANCE" "COMPLIANCE"})))

(s/def
  :cognitect.aws.s3/CORSRule
  (s/keys
    :req-un
    [:cognitect.aws.s3.CORSRule/AllowedMethods :cognitect.aws.s3.CORSRule/AllowedOrigins]
    :opt-un
    [:cognitect.aws.s3.CORSRule/ExposeHeaders
     :cognitect.aws.s3.CORSRule/AllowedHeaders
     :cognitect.aws.s3.CORSRule/MaxAgeSeconds]))

(s/def :cognitect.aws.s3/ObjectList (s/coll-of :cognitect.aws.s3/Object))

(s/def :cognitect.aws.s3/StatsEvent (s/keys :opt-un [:cognitect.aws.s3.StatsEvent/Details]))

(s/def
  :cognitect.aws.s3/RoutingRule
  (s/keys
    :req-un
    [:cognitect.aws.s3.RoutingRule/Redirect]
    :opt-un
    [:cognitect.aws.s3.RoutingRule/Condition]))

(s/def
  :cognitect.aws.s3/ScanRange
  (s/keys :opt-un [:cognitect.aws.s3.ScanRange/Start :cognitect.aws.s3.ScanRange/End]))

(s/def :cognitect.aws.s3/RequestCharged (s/spec string? :gen #(s/gen #{"requester"})))

(s/def :cognitect.aws.s3/Rules (s/coll-of :cognitect.aws.s3/Rule))

(s/def :cognitect.aws.s3/Years (s/spec int? :gen #(gen/choose Long/MIN_VALUE Long/MAX_VALUE)))

(s/def :cognitect.aws.s3/MFADeleteStatus (s/spec string? :gen #(s/gen #{"Disabled" "Enabled"})))

(s/def
  :cognitect.aws.s3/AnalyticsConfigurationList
  (s/coll-of :cognitect.aws.s3/AnalyticsConfiguration))

(s/def :cognitect.aws.s3/Minutes (s/spec int? :gen #(gen/choose Long/MIN_VALUE Long/MAX_VALUE)))

(s/def :cognitect.aws.s3/LastModified inst?)

(s/def
  :cognitect.aws.s3/InventoryConfiguration
  (s/keys
    :req-un
    [:cognitect.aws.s3.InventoryConfiguration/Destination
     :cognitect.aws.s3.InventoryConfiguration/IsEnabled
     :cognitect.aws.s3.InventoryConfiguration/Id
     :cognitect.aws.s3.InventoryConfiguration/IncludedObjectVersions
     :cognitect.aws.s3.InventoryConfiguration/Schedule]
    :opt-un
    [:cognitect.aws.s3.InventoryConfiguration/Filter
     :cognitect.aws.s3.InventoryConfiguration/OptionalFields]))

(s/def :cognitect.aws.s3/AllowedOrigin string?)

(s/def
  :cognitect.aws.s3/PutObjectLegalHoldOutput
  (s/keys :opt-un [:cognitect.aws.s3.PutObjectLegalHoldOutput/RequestCharged]))

(s/def
  :cognitect.aws.s3/DeleteBucketAnalyticsConfigurationRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.DeleteBucketAnalyticsConfigurationRequest/Bucket
     :cognitect.aws.s3.DeleteBucketAnalyticsConfigurationRequest/Id]))

(s/def
  :cognitect.aws.s3/ServerSideEncryptionRules
  (s/coll-of :cognitect.aws.s3/ServerSideEncryptionRule))

(s/def
  :cognitect.aws.s3/ListBucketMetricsConfigurationsOutput
  (s/keys
    :opt-un
    [:cognitect.aws.s3.ListBucketMetricsConfigurationsOutput/MetricsConfigurationList
     :cognitect.aws.s3.ListBucketMetricsConfigurationsOutput/IsTruncated
     :cognitect.aws.s3.ListBucketMetricsConfigurationsOutput/ContinuationToken
     :cognitect.aws.s3.ListBucketMetricsConfigurationsOutput/NextContinuationToken]))

(s/def
  :cognitect.aws.s3/HeadBucketRequest
  (s/keys :req-un [:cognitect.aws.s3.HeadBucketRequest/Bucket]))

(s/def
  :cognitect.aws.s3/GetBucketCorsRequest
  (s/keys :req-un [:cognitect.aws.s3.GetBucketCorsRequest/Bucket]))

(s/def :cognitect.aws.s3/PartsCount (s/spec int? :gen #(gen/choose Long/MIN_VALUE Long/MAX_VALUE)))

(s/def
  :cognitect.aws.s3/Tag
  (s/keys :req-un [:cognitect.aws.s3.Tag/Key :cognitect.aws.s3.Tag/Value]))

(s/def
  :cognitect.aws.s3/CopySource
  (s/spec #(re-matches (re-pattern "\\/.+\\/.+") %) :gen #(gen/string)))

(s/def
  :cognitect.aws.s3/InventorySchedule
  (s/keys :req-un [:cognitect.aws.s3.InventorySchedule/Frequency]))

(s/def :cognitect.aws.s3/S3KeyFilter (s/keys :opt-un [:cognitect.aws.s3.S3KeyFilter/FilterRules]))

(s/def :cognitect.aws.s3/Errors (s/coll-of :cognitect.aws.s3/Error))

(s/def
  :cognitect.aws.s3/GetBucketNotificationConfigurationRequest
  (s/keys :req-un [:cognitect.aws.s3.GetBucketNotificationConfigurationRequest/Bucket]))

(s/def :cognitect.aws.s3/CopySourceSSECustomerAlgorithm string?)

(s/def
  :cognitect.aws.s3/CompletedPart
  (s/keys
    :opt-un
    [:cognitect.aws.s3.CompletedPart/PartNumber :cognitect.aws.s3.CompletedPart/ETag]))

(s/def
  :cognitect.aws.s3/ServerSideEncryptionByDefault
  (s/keys
    :req-un
    [:cognitect.aws.s3.ServerSideEncryptionByDefault/SSEAlgorithm]
    :opt-un
    [:cognitect.aws.s3.ServerSideEncryptionByDefault/KMSMasterKeyID]))

(s/def
  :cognitect.aws.s3/Rule
  (s/keys
    :req-un
    [:cognitect.aws.s3.Rule/Prefix :cognitect.aws.s3.Rule/Status]
    :opt-un
    [:cognitect.aws.s3.Rule/Transition
     :cognitect.aws.s3.Rule/ID
     :cognitect.aws.s3.Rule/NoncurrentVersionExpiration
     :cognitect.aws.s3.Rule/AbortIncompleteMultipartUpload
     :cognitect.aws.s3.Rule/NoncurrentVersionTransition
     :cognitect.aws.s3.Rule/Expiration]))

(s/def :cognitect.aws.s3/ETag string?)

(s/def :cognitect.aws.s3/Prefix string?)

(s/def :cognitect.aws.s3/ProgressEvent (s/keys :opt-un [:cognitect.aws.s3.ProgressEvent/Details]))

(s/def
  :cognitect.aws.s3/Encryption
  (s/keys
    :req-un
    [:cognitect.aws.s3.Encryption/EncryptionType]
    :opt-un
    [:cognitect.aws.s3.Encryption/KMSKeyId :cognitect.aws.s3.Encryption/KMSContext]))

(s/def :cognitect.aws.s3/ContentLanguage string?)

(s/def
  :cognitect.aws.s3/CompleteMultipartUploadOutput
  (s/keys
    :opt-un
    [:cognitect.aws.s3.CompleteMultipartUploadOutput/Bucket
     :cognitect.aws.s3.CompleteMultipartUploadOutput/VersionId
     :cognitect.aws.s3.CompleteMultipartUploadOutput/Location
     :cognitect.aws.s3.CompleteMultipartUploadOutput/ServerSideEncryption
     :cognitect.aws.s3.CompleteMultipartUploadOutput/ETag
     :cognitect.aws.s3.CompleteMultipartUploadOutput/Key
     :cognitect.aws.s3.CompleteMultipartUploadOutput/RequestCharged
     :cognitect.aws.s3.CompleteMultipartUploadOutput/SSEKMSKeyId
     :cognitect.aws.s3.CompleteMultipartUploadOutput/Expiration]))

(s/def :cognitect.aws.s3/MetadataValue string?)

(s/def :cognitect.aws.s3/TopicConfigurationList (s/coll-of :cognitect.aws.s3/TopicConfiguration))

(s/def
  :cognitect.aws.s3/RestoreObjectOutput
  (s/keys
    :opt-un
    [:cognitect.aws.s3.RestoreObjectOutput/RestoreOutputPath
     :cognitect.aws.s3.RestoreObjectOutput/RequestCharged]))

(s/def :cognitect.aws.s3/GrantRead string?)

(s/def
  :cognitect.aws.s3/RequestPaymentConfiguration
  (s/keys :req-un [:cognitect.aws.s3.RequestPaymentConfiguration/Payer]))

(s/def
  :cognitect.aws.s3/GetObjectLegalHoldRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.GetObjectLegalHoldRequest/Bucket
     :cognitect.aws.s3.GetObjectLegalHoldRequest/Key]
    :opt-un
    [:cognitect.aws.s3.GetObjectLegalHoldRequest/VersionId
     :cognitect.aws.s3.GetObjectLegalHoldRequest/RequestPayer]))

(s/def
  :cognitect.aws.s3/QueueConfiguration
  (s/keys
    :req-un
    [:cognitect.aws.s3.QueueConfiguration/QueueArn :cognitect.aws.s3.QueueConfiguration/Events]
    :opt-un
    [:cognitect.aws.s3.QueueConfiguration/Filter :cognitect.aws.s3.QueueConfiguration/Id]))

(s/def
  :cognitect.aws.s3/Initiator
  (s/keys :opt-un [:cognitect.aws.s3.Initiator/DisplayName :cognitect.aws.s3.Initiator/ID]))

(s/def
  :cognitect.aws.s3/GetBucketEncryptionRequest
  (s/keys :req-un [:cognitect.aws.s3.GetBucketEncryptionRequest/Bucket]))

(s/def :cognitect.aws.s3/GrantReadACP string?)

(s/def
  :cognitect.aws.s3/PutBucketNotificationConfigurationRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.PutBucketNotificationConfigurationRequest/Bucket
     :cognitect.aws.s3.PutBucketNotificationConfigurationRequest/NotificationConfiguration]))

(s/def
  :cognitect.aws.s3/GetBucketLifecycleRequest
  (s/keys :req-un [:cognitect.aws.s3.GetBucketLifecycleRequest/Bucket]))

(s/def :cognitect.aws.s3/FilterRuleName (s/spec string? :gen #(s/gen #{"prefix" "suffix"})))

(s/def :cognitect.aws.s3/ObjectVersionStorageClass (s/spec string? :gen #(s/gen #{"STANDARD"})))

(s/def :cognitect.aws.s3/UploadIdMarker string?)

(s/def
  :cognitect.aws.s3/GetBucketRequestPaymentOutput
  (s/keys :opt-un [:cognitect.aws.s3.GetBucketRequestPaymentOutput/Payer]))

(s/def
  :cognitect.aws.s3/PutBucketNotificationRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.PutBucketNotificationRequest/Bucket
     :cognitect.aws.s3.PutBucketNotificationRequest/NotificationConfiguration]
    :opt-un
    [:cognitect.aws.s3.PutBucketNotificationRequest/ContentMD5]))

(s/def
  :cognitect.aws.s3/BucketCannedACL
  (s/spec
    string?
    :gen
    #(s/gen #{"authenticated-read" "public-read-write" "private" "public-read"})))

(s/def :cognitect.aws.s3/FilterRuleList (s/coll-of :cognitect.aws.s3/FilterRule))

(s/def
  :cognitect.aws.s3/DeleteBucketMetricsConfigurationRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.DeleteBucketMetricsConfigurationRequest/Bucket
     :cognitect.aws.s3.DeleteBucketMetricsConfigurationRequest/Id]))

(s/def
  :cognitect.aws.s3/ListObjectsRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.ListObjectsRequest/Bucket]
    :opt-un
    [:cognitect.aws.s3.ListObjectsRequest/Prefix
     :cognitect.aws.s3.ListObjectsRequest/EncodingType
     :cognitect.aws.s3.ListObjectsRequest/Marker
     :cognitect.aws.s3.ListObjectsRequest/Delimiter
     :cognitect.aws.s3.ListObjectsRequest/MaxKeys
     :cognitect.aws.s3.ListObjectsRequest/RequestPayer]))

(s/def
  :cognitect.aws.s3/CopyObjectOutput
  (s/keys
    :opt-un
    [:cognitect.aws.s3.CopyObjectOutput/SSECustomerKeyMD5
     :cognitect.aws.s3.CopyObjectOutput/CopySourceVersionId
     :cognitect.aws.s3.CopyObjectOutput/SSECustomerAlgorithm
     :cognitect.aws.s3.CopyObjectOutput/VersionId
     :cognitect.aws.s3.CopyObjectOutput/ServerSideEncryption
     :cognitect.aws.s3.CopyObjectOutput/CopyObjectResult
     :cognitect.aws.s3.CopyObjectOutput/SSEKMSEncryptionContext
     :cognitect.aws.s3.CopyObjectOutput/RequestCharged
     :cognitect.aws.s3.CopyObjectOutput/SSEKMSKeyId
     :cognitect.aws.s3.CopyObjectOutput/Expiration]))

(s/def :cognitect.aws.s3/Buckets (s/coll-of :cognitect.aws.s3/Bucket))

(s/def :cognitect.aws.s3/JSONType (s/spec string? :gen #(s/gen #{"DOCUMENT" "LINES"})))

(s/def :cognitect.aws.s3/Message string?)

(s/def
  :cognitect.aws.s3/ListPartsOutput
  (s/keys
    :opt-un
    [:cognitect.aws.s3.ListPartsOutput/Bucket
     :cognitect.aws.s3.ListPartsOutput/Parts
     :cognitect.aws.s3.ListPartsOutput/AbortDate
     :cognitect.aws.s3.ListPartsOutput/MaxParts
     :cognitect.aws.s3.ListPartsOutput/NextPartNumberMarker
     :cognitect.aws.s3.ListPartsOutput/Key
     :cognitect.aws.s3.ListPartsOutput/IsTruncated
     :cognitect.aws.s3.ListPartsOutput/AbortRuleId
     :cognitect.aws.s3.ListPartsOutput/PartNumberMarker
     :cognitect.aws.s3.ListPartsOutput/Owner
     :cognitect.aws.s3.ListPartsOutput/UploadId
     :cognitect.aws.s3.ListPartsOutput/StorageClass
     :cognitect.aws.s3.ListPartsOutput/RequestCharged
     :cognitect.aws.s3.ListPartsOutput/Initiator]))

(s/def
  :cognitect.aws.s3/PutBucketVersioningRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.PutBucketVersioningRequest/Bucket
     :cognitect.aws.s3.PutBucketVersioningRequest/VersioningConfiguration]
    :opt-un
    [:cognitect.aws.s3.PutBucketVersioningRequest/ContentMD5
     :cognitect.aws.s3.PutBucketVersioningRequest/MFA]))

(s/def :cognitect.aws.s3/IfUnmodifiedSince inst?)

(s/def
  :cognitect.aws.s3/GetBucketAnalyticsConfigurationRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.GetBucketAnalyticsConfigurationRequest/Bucket
     :cognitect.aws.s3.GetBucketAnalyticsConfigurationRequest/Id]))

(s/def
  :cognitect.aws.s3/AccelerateConfiguration
  (s/keys :opt-un [:cognitect.aws.s3.AccelerateConfiguration/Status]))

(s/def :cognitect.aws.s3/EnableRequestProgress boolean?)

(s/def
  :cognitect.aws.s3/GetPublicAccessBlockRequest
  (s/keys :req-un [:cognitect.aws.s3.GetPublicAccessBlockRequest/Bucket]))

(s/def
  :cognitect.aws.s3/CopySourceSSECustomerKey
  (s/or :byte-array bytes? :input-stream #(instance? java.io.InputStream %)))

(s/def :cognitect.aws.s3/Size (s/spec int? :gen #(gen/choose Long/MIN_VALUE Long/MAX_VALUE)))

(s/def :cognitect.aws.s3/AbortDate inst?)

(s/def
  :cognitect.aws.s3/Permission
  (s/spec string? :gen #(s/gen #{"WRITE" "FULL_CONTROL" "READ_ACP" "WRITE_ACP" "READ"})))

(s/def
  :cognitect.aws.s3/ReplicationStatus
  (s/spec string? :gen #(s/gen #{"REPLICA" "PENDING" "COMPLETE" "FAILED"})))

(s/def
  :cognitect.aws.s3/VersioningConfiguration
  (s/keys
    :opt-un
    [:cognitect.aws.s3.VersioningConfiguration/MFADelete
     :cognitect.aws.s3.VersioningConfiguration/Status]))

(s/def :cognitect.aws.s3/CopySourceSSECustomerKeyMD5 string?)

(s/def
  :cognitect.aws.s3/GetBucketEncryptionOutput
  (s/keys :opt-un [:cognitect.aws.s3.GetBucketEncryptionOutput/ServerSideEncryptionConfiguration]))

(s/def :cognitect.aws.s3/Suffix string?)

(s/def
  :cognitect.aws.s3/GetBucketPolicyStatusRequest
  (s/keys :req-un [:cognitect.aws.s3.GetBucketPolicyStatusRequest/Bucket]))

(s/def
  :cognitect.aws.s3/StorageClassAnalysis
  (s/keys :opt-un [:cognitect.aws.s3.StorageClassAnalysis/DataExport]))

(s/def :cognitect.aws.s3/StartAfter string?)

(s/def
  :cognitect.aws.s3/PutBucketInventoryConfigurationRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.PutBucketInventoryConfigurationRequest/Bucket
     :cognitect.aws.s3.PutBucketInventoryConfigurationRequest/Id
     :cognitect.aws.s3.PutBucketInventoryConfigurationRequest/InventoryConfiguration]))

(s/def :cognitect.aws.s3/Token string?)

(s/def
  :cognitect.aws.s3/SseKmsEncryptedObjectsStatus
  (s/spec string? :gen #(s/gen #{"Disabled" "Enabled"})))

(s/def
  :cognitect.aws.s3/Bucket
  (s/keys :opt-un [:cognitect.aws.s3.Bucket/CreationDate :cognitect.aws.s3.Bucket/Name]))

(s/def :cognitect.aws.s3/TopicArn string?)

(s/def :cognitect.aws.s3/AccountId string?)

(s/def
  :cognitect.aws.s3/GetBucketAnalyticsConfigurationOutput
  (s/keys :opt-un [:cognitect.aws.s3.GetBucketAnalyticsConfigurationOutput/AnalyticsConfiguration]))

(s/def
  :cognitect.aws.s3/BucketLifecycleConfiguration
  (s/keys :req-un [:cognitect.aws.s3.BucketLifecycleConfiguration/Rules]))

(s/def
  :cognitect.aws.s3/ObjectVersion
  (s/keys
    :opt-un
    [:cognitect.aws.s3.ObjectVersion/VersionId
     :cognitect.aws.s3.ObjectVersion/ETag
     :cognitect.aws.s3.ObjectVersion/Key
     :cognitect.aws.s3.ObjectVersion/LastModified
     :cognitect.aws.s3.ObjectVersion/Size
     :cognitect.aws.s3.ObjectVersion/Owner
     :cognitect.aws.s3.ObjectVersion/StorageClass
     :cognitect.aws.s3.ObjectVersion/IsLatest]))

(s/def
  :cognitect.aws.s3/SseKmsEncryptedObjects
  (s/keys :req-un [:cognitect.aws.s3.SseKmsEncryptedObjects/Status]))

(s/def
  :cognitect.aws.s3/CreateBucketOutput
  (s/keys :opt-un [:cognitect.aws.s3.CreateBucketOutput/Location]))

(s/def
  :cognitect.aws.s3/GetObjectAclRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.GetObjectAclRequest/Bucket :cognitect.aws.s3.GetObjectAclRequest/Key]
    :opt-un
    [:cognitect.aws.s3.GetObjectAclRequest/VersionId
     :cognitect.aws.s3.GetObjectAclRequest/RequestPayer]))

(s/def
  :cognitect.aws.s3/ReplicationTimeStatus
  (s/spec string? :gen #(s/gen #{"Disabled" "Enabled"})))

(s/def
  :cognitect.aws.s3/Stats
  (s/keys
    :opt-un
    [:cognitect.aws.s3.Stats/BytesReturned
     :cognitect.aws.s3.Stats/BytesScanned
     :cognitect.aws.s3.Stats/BytesProcessed]))

(s/def
  :cognitect.aws.s3/ObjectStorageClass
  (s/spec
    string?
    :gen
    #(s/gen
      #{"DEEP_ARCHIVE" "GLACIER" "ONEZONE_IA" "STANDARD_IA" "STANDARD" "INTELLIGENT_TIERING"
        "REDUCED_REDUNDANCY"})))

(s/def :cognitect.aws.s3/DeleteMarkers (s/coll-of :cognitect.aws.s3/DeleteMarkerEntry))

(s/def
  :cognitect.aws.s3/PutBucketAccelerateConfigurationRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.PutBucketAccelerateConfigurationRequest/Bucket
     :cognitect.aws.s3.PutBucketAccelerateConfigurationRequest/AccelerateConfiguration]))

(s/def
  :cognitect.aws.s3/DeletePublicAccessBlockRequest
  (s/keys :req-un [:cognitect.aws.s3.DeletePublicAccessBlockRequest/Bucket]))

(s/def
  :cognitect.aws.s3/CreateMultipartUploadOutput
  (s/keys
    :opt-un
    [:cognitect.aws.s3.CreateMultipartUploadOutput/Bucket
     :cognitect.aws.s3.CreateMultipartUploadOutput/SSECustomerKeyMD5
     :cognitect.aws.s3.CreateMultipartUploadOutput/SSECustomerAlgorithm
     :cognitect.aws.s3.CreateMultipartUploadOutput/AbortDate
     :cognitect.aws.s3.CreateMultipartUploadOutput/ServerSideEncryption
     :cognitect.aws.s3.CreateMultipartUploadOutput/Key
     :cognitect.aws.s3.CreateMultipartUploadOutput/AbortRuleId
     :cognitect.aws.s3.CreateMultipartUploadOutput/UploadId
     :cognitect.aws.s3.CreateMultipartUploadOutput/SSEKMSEncryptionContext
     :cognitect.aws.s3.CreateMultipartUploadOutput/RequestCharged
     :cognitect.aws.s3.CreateMultipartUploadOutput/SSEKMSKeyId]))

(s/def
  :cognitect.aws.s3/DeleteMarkerEntry
  (s/keys
    :opt-un
    [:cognitect.aws.s3.DeleteMarkerEntry/VersionId
     :cognitect.aws.s3.DeleteMarkerEntry/Key
     :cognitect.aws.s3.DeleteMarkerEntry/LastModified
     :cognitect.aws.s3.DeleteMarkerEntry/Owner
     :cognitect.aws.s3.DeleteMarkerEntry/IsLatest]))

(s/def
  :cognitect.aws.s3/InventoryS3BucketDestination
  (s/keys
    :req-un
    [:cognitect.aws.s3.InventoryS3BucketDestination/Bucket
     :cognitect.aws.s3.InventoryS3BucketDestination/Format]
    :opt-un
    [:cognitect.aws.s3.InventoryS3BucketDestination/Prefix
     :cognitect.aws.s3.InventoryS3BucketDestination/AccountId
     :cognitect.aws.s3.InventoryS3BucketDestination/Encryption]))

(s/def
  :cognitect.aws.s3/DefaultRetention
  (s/keys
    :opt-un
    [:cognitect.aws.s3.DefaultRetention/Mode
     :cognitect.aws.s3.DefaultRetention/Days
     :cognitect.aws.s3.DefaultRetention/Years]))

(s/def
  :cognitect.aws.s3/BytesReturned
  (s/spec int? :gen #(gen/choose Long/MIN_VALUE Long/MAX_VALUE)))

(s/def
  :cognitect.aws.s3/LoggingEnabled
  (s/keys
    :req-un
    [:cognitect.aws.s3.LoggingEnabled/TargetBucket :cognitect.aws.s3.LoggingEnabled/TargetPrefix]
    :opt-un
    [:cognitect.aws.s3.LoggingEnabled/TargetGrants]))

(s/def
  :cognitect.aws.s3/AccessControlPolicy
  (s/keys
    :opt-un
    [:cognitect.aws.s3.AccessControlPolicy/Grants :cognitect.aws.s3.AccessControlPolicy/Owner]))

(s/def :cognitect.aws.s3/MetadataDirective (s/spec string? :gen #(s/gen #{"COPY" "REPLACE"})))

(s/def
  :cognitect.aws.s3/PutObjectTaggingRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.PutObjectTaggingRequest/Bucket
     :cognitect.aws.s3.PutObjectTaggingRequest/Key
     :cognitect.aws.s3.PutObjectTaggingRequest/Tagging]
    :opt-un
    [:cognitect.aws.s3.PutObjectTaggingRequest/VersionId
     :cognitect.aws.s3.PutObjectTaggingRequest/ContentMD5]))

(s/def
  :cognitect.aws.s3/CopyObjectRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.CopyObjectRequest/Bucket
     :cognitect.aws.s3.CopyObjectRequest/CopySource
     :cognitect.aws.s3.CopyObjectRequest/Key]
    :opt-un
    [:cognitect.aws.s3.CopyObjectRequest/ContentEncoding
     :cognitect.aws.s3.CopyObjectRequest/TaggingDirective
     :cognitect.aws.s3.CopyObjectRequest/SSECustomerKeyMD5
     :cognitect.aws.s3.CopyObjectRequest/CopySourceSSECustomerKey
     :cognitect.aws.s3.CopyObjectRequest/WebsiteRedirectLocation
     :cognitect.aws.s3.CopyObjectRequest/SSECustomerAlgorithm
     :cognitect.aws.s3.CopyObjectRequest/MetadataDirective
     :cognitect.aws.s3.CopyObjectRequest/SSECustomerKey
     :cognitect.aws.s3.CopyObjectRequest/GrantReadACP
     :cognitect.aws.s3.CopyObjectRequest/GrantRead
     :cognitect.aws.s3.CopyObjectRequest/ServerSideEncryption
     :cognitect.aws.s3.CopyObjectRequest/ContentDisposition
     :cognitect.aws.s3.CopyObjectRequest/CopySourceIfMatch
     :cognitect.aws.s3.CopyObjectRequest/ContentType
     :cognitect.aws.s3.CopyObjectRequest/ACL
     :cognitect.aws.s3.CopyObjectRequest/CopySourceIfNoneMatch
     :cognitect.aws.s3.CopyObjectRequest/CopySourceIfModifiedSince
     :cognitect.aws.s3.CopyObjectRequest/CopySourceIfUnmodifiedSince
     :cognitect.aws.s3.CopyObjectRequest/CopySourceSSECustomerAlgorithm
     :cognitect.aws.s3.CopyObjectRequest/ObjectLockLegalHoldStatus
     :cognitect.aws.s3.CopyObjectRequest/CopySourceSSECustomerKeyMD5
     :cognitect.aws.s3.CopyObjectRequest/Metadata
     :cognitect.aws.s3.CopyObjectRequest/CacheControl
     :cognitect.aws.s3.CopyObjectRequest/ContentLanguage
     :cognitect.aws.s3.CopyObjectRequest/GrantWriteACP
     :cognitect.aws.s3.CopyObjectRequest/GrantFullControl
     :cognitect.aws.s3.CopyObjectRequest/Tagging
     :cognitect.aws.s3.CopyObjectRequest/SSEKMSEncryptionContext
     :cognitect.aws.s3.CopyObjectRequest/StorageClass
     :cognitect.aws.s3.CopyObjectRequest/Expires
     :cognitect.aws.s3.CopyObjectRequest/ObjectLockRetainUntilDate
     :cognitect.aws.s3.CopyObjectRequest/SSEKMSKeyId
     :cognitect.aws.s3.CopyObjectRequest/ObjectLockMode
     :cognitect.aws.s3.CopyObjectRequest/RequestPayer]))

(s/def
  :cognitect.aws.s3/Owner
  (s/keys :opt-un [:cognitect.aws.s3.Owner/DisplayName :cognitect.aws.s3.Owner/ID]))

(s/def :cognitect.aws.s3/ExposeHeader string?)

(s/def
  :cognitect.aws.s3/Destination
  (s/keys
    :req-un
    [:cognitect.aws.s3.Destination/Bucket]
    :opt-un
    [:cognitect.aws.s3.Destination/AccessControlTranslation
     :cognitect.aws.s3.Destination/EncryptionConfiguration
     :cognitect.aws.s3.Destination/Account
     :cognitect.aws.s3.Destination/ReplicationTime
     :cognitect.aws.s3.Destination/StorageClass
     :cognitect.aws.s3.Destination/Metrics]))

(s/def :cognitect.aws.s3/TaggingHeader string?)

(s/def :cognitect.aws.s3/ContinuationEvent (s/keys))

(s/def
  :cognitect.aws.s3/GetBucketWebsiteOutput
  (s/keys
    :opt-un
    [:cognitect.aws.s3.GetBucketWebsiteOutput/RoutingRules
     :cognitect.aws.s3.GetBucketWebsiteOutput/ErrorDocument
     :cognitect.aws.s3.GetBucketWebsiteOutput/RedirectAllRequestsTo
     :cognitect.aws.s3.GetBucketWebsiteOutput/IndexDocument]))

(s/def :cognitect.aws.s3/NotificationId string?)

(s/def
  :cognitect.aws.s3/GetBucketReplicationRequest
  (s/keys :req-un [:cognitect.aws.s3.GetBucketReplicationRequest/Bucket]))

(s/def :cognitect.aws.s3/NextKeyMarker string?)

(s/def :cognitect.aws.s3/RoutingRules (s/coll-of :cognitect.aws.s3/RoutingRule))

(s/def
  :cognitect.aws.s3/HeadObjectRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.HeadObjectRequest/Bucket :cognitect.aws.s3.HeadObjectRequest/Key]
    :opt-un
    [:cognitect.aws.s3.HeadObjectRequest/SSECustomerKeyMD5
     :cognitect.aws.s3.HeadObjectRequest/IfUnmodifiedSince
     :cognitect.aws.s3.HeadObjectRequest/SSECustomerAlgorithm
     :cognitect.aws.s3.HeadObjectRequest/SSECustomerKey
     :cognitect.aws.s3.HeadObjectRequest/VersionId
     :cognitect.aws.s3.HeadObjectRequest/IfModifiedSince
     :cognitect.aws.s3.HeadObjectRequest/PartNumber
     :cognitect.aws.s3.HeadObjectRequest/Range
     :cognitect.aws.s3.HeadObjectRequest/IfMatch
     :cognitect.aws.s3.HeadObjectRequest/IfNoneMatch
     :cognitect.aws.s3.HeadObjectRequest/RequestPayer]))

(s/def
  :cognitect.aws.s3/AccessControlTranslation
  (s/keys :req-un [:cognitect.aws.s3.AccessControlTranslation/Owner]))

(s/def
  :cognitect.aws.s3/CloudFunctionConfiguration
  (s/keys
    :opt-un
    [:cognitect.aws.s3.CloudFunctionConfiguration/Events
     :cognitect.aws.s3.CloudFunctionConfiguration/Event
     :cognitect.aws.s3.CloudFunctionConfiguration/InvocationRole
     :cognitect.aws.s3.CloudFunctionConfiguration/Id
     :cognitect.aws.s3.CloudFunctionConfiguration/CloudFunction]))

(s/def :cognitect.aws.s3/KeyPrefixEquals string?)

(s/def :cognitect.aws.s3/EmailAddress string?)

(s/def :cognitect.aws.s3/IndexDocument (s/keys :req-un [:cognitect.aws.s3.IndexDocument/Suffix]))

(s/def
  :cognitect.aws.s3/InventoryFilter
  (s/keys :req-un [:cognitect.aws.s3.InventoryFilter/Prefix]))

(s/def
  :cognitect.aws.s3/GetBucketVersioningOutput
  (s/keys
    :opt-un
    [:cognitect.aws.s3.GetBucketVersioningOutput/MFADelete
     :cognitect.aws.s3.GetBucketVersioningOutput/Status]))

(s/def :cognitect.aws.s3/ContentMD5 string?)

(s/def
  :cognitect.aws.s3/PutBucketMetricsConfigurationRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.PutBucketMetricsConfigurationRequest/Bucket
     :cognitect.aws.s3.PutBucketMetricsConfigurationRequest/Id
     :cognitect.aws.s3.PutBucketMetricsConfigurationRequest/MetricsConfiguration]))

(s/def :cognitect.aws.s3/ObjectIdentifierList (s/coll-of :cognitect.aws.s3/ObjectIdentifier))

(s/def :cognitect.aws.s3/SSECustomerKeyMD5 string?)

(s/def
  :cognitect.aws.s3/InventoryOptionalField
  (s/spec
    string?
    :gen
    #(s/gen
      #{"LastModifiedDate" "ReplicationStatus" "EncryptionStatus" "ETag" "Size"
        "ObjectLockLegalHoldStatus" "IsMultipartUploaded" "StorageClass"
        "IntelligentTieringAccessTier" "ObjectLockRetainUntilDate" "ObjectLockMode"})))

(s/def
  :cognitect.aws.s3/GetObjectTaggingOutput
  (s/keys
    :req-un
    [:cognitect.aws.s3.GetObjectTaggingOutput/TagSet]
    :opt-un
    [:cognitect.aws.s3.GetObjectTaggingOutput/VersionId]))

(s/def
  :cognitect.aws.s3/LambdaFunctionConfigurationList
  (s/coll-of :cognitect.aws.s3/LambdaFunctionConfiguration))

(s/def
  :cognitect.aws.s3/BytesScanned
  (s/spec int? :gen #(gen/choose Long/MIN_VALUE Long/MAX_VALUE)))

(s/def :cognitect.aws.s3/ContentRange string?)

(s/def
  :cognitect.aws.s3/PutBucketLifecycleConfigurationRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.PutBucketLifecycleConfigurationRequest/Bucket]
    :opt-un
    [:cognitect.aws.s3.PutBucketLifecycleConfigurationRequest/LifecycleConfiguration]))

(s/def
  :cognitect.aws.s3/ObjectKey
  (s/spec
    (s/and string? #(<= 1 (count %)))
    :gen
    (fn []
      (gen/fmap #(apply str %) (gen/bind (gen/choose 1 100) #(gen/vector (gen/char-alpha) %))))))

(s/def
  :cognitect.aws.s3/DeleteObjectTaggingRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.DeleteObjectTaggingRequest/Bucket
     :cognitect.aws.s3.DeleteObjectTaggingRequest/Key]
    :opt-un
    [:cognitect.aws.s3.DeleteObjectTaggingRequest/VersionId]))

(s/def :cognitect.aws.s3/AllowQuotedRecordDelimiter boolean?)

(s/def :cognitect.aws.s3/TargetPrefix string?)

(s/def :cognitect.aws.s3/QuoteEscapeCharacter string?)

(s/def
  :cognitect.aws.s3/GetBucketAccelerateConfigurationRequest
  (s/keys :req-un [:cognitect.aws.s3.GetBucketAccelerateConfigurationRequest/Bucket]))

(s/def :cognitect.aws.s3/ObjectLockEnabled (s/spec string? :gen #(s/gen #{"Enabled"})))

(s/def
  :cognitect.aws.s3/FilterRule
  (s/keys :opt-un [:cognitect.aws.s3.FilterRule/Value :cognitect.aws.s3.FilterRule/Name]))

(s/def
  :cognitect.aws.s3/DeleteMarkerReplication
  (s/keys :opt-un [:cognitect.aws.s3.DeleteMarkerReplication/Status]))

(s/def
  :cognitect.aws.s3/PutBucketAclRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.PutBucketAclRequest/Bucket]
    :opt-un
    [:cognitect.aws.s3.PutBucketAclRequest/ContentMD5
     :cognitect.aws.s3.PutBucketAclRequest/GrantReadACP
     :cognitect.aws.s3.PutBucketAclRequest/AccessControlPolicy
     :cognitect.aws.s3.PutBucketAclRequest/GrantRead
     :cognitect.aws.s3.PutBucketAclRequest/ACL
     :cognitect.aws.s3.PutBucketAclRequest/GrantWrite
     :cognitect.aws.s3.PutBucketAclRequest/GrantWriteACP
     :cognitect.aws.s3.PutBucketAclRequest/GrantFullControl]))

(s/def
  :cognitect.aws.s3/MetadataEntry
  (s/keys :opt-un [:cognitect.aws.s3.MetadataEntry/Value :cognitect.aws.s3.MetadataEntry/Name]))

(s/def :cognitect.aws.s3/RecordDelimiter string?)

(s/def :cognitect.aws.s3/EncodingType (s/spec string? :gen #(s/gen #{"url"})))

(s/def
  :cognitect.aws.s3/GetBucketMetricsConfigurationOutput
  (s/keys :opt-un [:cognitect.aws.s3.GetBucketMetricsConfigurationOutput/MetricsConfiguration]))

(s/def :cognitect.aws.s3/MaxParts (s/spec int? :gen #(gen/choose Long/MIN_VALUE Long/MAX_VALUE)))

(s/def :cognitect.aws.s3/MetricsStatus (s/spec string? :gen #(s/gen #{"Disabled" "Enabled"})))

(s/def :cognitect.aws.s3/CompletedPartList (s/coll-of :cognitect.aws.s3/CompletedPart))

(s/def :cognitect.aws.s3/Grants (s/coll-of :cognitect.aws.s3/Grant))

(s/def :cognitect.aws.s3/ReplicationRules (s/coll-of :cognitect.aws.s3/ReplicationRule))

(s/def :cognitect.aws.s3/GrantWriteACP string?)

(s/def
  :cognitect.aws.s3/PutBucketCorsRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.PutBucketCorsRequest/Bucket
     :cognitect.aws.s3.PutBucketCorsRequest/CORSConfiguration]
    :opt-un
    [:cognitect.aws.s3.PutBucketCorsRequest/ContentMD5]))

(s/def
  :cognitect.aws.s3/AbortIncompleteMultipartUpload
  (s/keys :opt-un [:cognitect.aws.s3.AbortIncompleteMultipartUpload/DaysAfterInitiation]))

(s/def :cognitect.aws.s3/Tagging (s/keys :req-un [:cognitect.aws.s3.Tagging/TagSet]))

(s/def
  :cognitect.aws.s3/ListObjectsV2Request
  (s/keys
    :req-un
    [:cognitect.aws.s3.ListObjectsV2Request/Bucket]
    :opt-un
    [:cognitect.aws.s3.ListObjectsV2Request/Prefix
     :cognitect.aws.s3.ListObjectsV2Request/EncodingType
     :cognitect.aws.s3.ListObjectsV2Request/StartAfter
     :cognitect.aws.s3.ListObjectsV2Request/ContinuationToken
     :cognitect.aws.s3.ListObjectsV2Request/Delimiter
     :cognitect.aws.s3.ListObjectsV2Request/FetchOwner
     :cognitect.aws.s3.ListObjectsV2Request/MaxKeys
     :cognitect.aws.s3.ListObjectsV2Request/RequestPayer]))

(s/def :cognitect.aws.s3/RestoreOutputPath string?)

(s/def
  :cognitect.aws.s3/GetObjectTorrentOutput
  (s/keys
    :opt-un
    [:cognitect.aws.s3.GetObjectTorrentOutput/Body
     :cognitect.aws.s3.GetObjectTorrentOutput/RequestCharged]))

(s/def
  :cognitect.aws.s3/ListBucketsOutput
  (s/keys
    :opt-un
    [:cognitect.aws.s3.ListBucketsOutput/Buckets :cognitect.aws.s3.ListBucketsOutput/Owner]))

(s/def
  :cognitect.aws.s3/GetBucketCorsOutput
  (s/keys :opt-un [:cognitect.aws.s3.GetBucketCorsOutput/CORSRules]))

(s/def :cognitect.aws.s3/ContentEncoding string?)

(s/def :cognitect.aws.s3/CopySourceIfUnmodifiedSince inst?)

(s/def
  :cognitect.aws.s3/QueueConfigurationDeprecated
  (s/keys
    :opt-un
    [:cognitect.aws.s3.QueueConfigurationDeprecated/Queue
     :cognitect.aws.s3.QueueConfigurationDeprecated/Events
     :cognitect.aws.s3.QueueConfigurationDeprecated/Event
     :cognitect.aws.s3.QueueConfigurationDeprecated/Id]))

(s/def :cognitect.aws.s3/ResponseContentLanguage string?)

(s/def :cognitect.aws.s3/ObjectLockMode (s/spec string? :gen #(s/gen #{"GOVERNANCE" "COMPLIANCE"})))

(s/def
  :cognitect.aws.s3/Event
  (s/spec
    string?
    :gen
    #(s/gen
      #{"s3:Replication:OperationNotTracked" "s3:ObjectRestore:Completed"
        "s3:Replication:OperationFailedReplication" "s3:ObjectCreated:*" "s3:ObjectRestore:*"
        "s3:ObjectRemoved:Delete" "s3:ObjectRemoved:*" "s3:ObjectRemoved:DeleteMarkerCreated"
        "s3:Replication:*" "s3:Replication:OperationMissedThreshold" "s3:ObjectCreated:Copy"
        "s3:ObjectCreated:Post" "s3:ObjectCreated:CompleteMultipartUpload"
        "s3:ReducedRedundancyLostObject" "s3:ObjectRestore:Post" "s3:ObjectCreated:Put"
        "s3:Replication:OperationReplicatedAfterThreshold"})))

(s/def
  :cognitect.aws.s3/S3Location
  (s/keys
    :req-un
    [:cognitect.aws.s3.S3Location/BucketName :cognitect.aws.s3.S3Location/Prefix]
    :opt-un
    [:cognitect.aws.s3.S3Location/AccessControlList
     :cognitect.aws.s3.S3Location/CannedACL
     :cognitect.aws.s3.S3Location/UserMetadata
     :cognitect.aws.s3.S3Location/Encryption
     :cognitect.aws.s3.S3Location/Tagging
     :cognitect.aws.s3.S3Location/StorageClass]))

(s/def :cognitect.aws.s3/Days (s/spec int? :gen #(gen/choose Long/MIN_VALUE Long/MAX_VALUE)))

(s/def :cognitect.aws.s3/ResponseContentType string?)

(s/def
  :cognitect.aws.s3/MetricsConfiguration
  (s/keys
    :req-un
    [:cognitect.aws.s3.MetricsConfiguration/Id]
    :opt-un
    [:cognitect.aws.s3.MetricsConfiguration/Filter]))

(s/def
  :cognitect.aws.s3/GlacierJobParameters
  (s/keys :req-un [:cognitect.aws.s3.GlacierJobParameters/Tier]))

(s/def
  :cognitect.aws.s3/UploadPartCopyOutput
  (s/keys
    :opt-un
    [:cognitect.aws.s3.UploadPartCopyOutput/SSECustomerKeyMD5
     :cognitect.aws.s3.UploadPartCopyOutput/CopySourceVersionId
     :cognitect.aws.s3.UploadPartCopyOutput/SSECustomerAlgorithm
     :cognitect.aws.s3.UploadPartCopyOutput/CopyPartResult
     :cognitect.aws.s3.UploadPartCopyOutput/ServerSideEncryption
     :cognitect.aws.s3.UploadPartCopyOutput/RequestCharged
     :cognitect.aws.s3.UploadPartCopyOutput/SSEKMSKeyId]))

(s/def
  :cognitect.aws.s3/InventoryIncludedObjectVersions
  (s/spec string? :gen #(s/gen #{"Current" "All"})))

(s/def :cognitect.aws.s3/Delimiter string?)

(s/def :cognitect.aws.s3/Tier (s/spec string? :gen #(s/gen #{"Expedited" "Standard" "Bulk"})))

(s/def :cognitect.aws.s3/ExpressionType (s/spec string? :gen #(s/gen #{"SQL"})))

(s/def
  :cognitect.aws.s3/GetBucketLoggingRequest
  (s/keys :req-un [:cognitect.aws.s3.GetBucketLoggingRequest/Bucket]))

(s/def
  :cognitect.aws.s3/PutObjectOutput
  (s/keys
    :opt-un
    [:cognitect.aws.s3.PutObjectOutput/SSECustomerKeyMD5
     :cognitect.aws.s3.PutObjectOutput/SSECustomerAlgorithm
     :cognitect.aws.s3.PutObjectOutput/VersionId
     :cognitect.aws.s3.PutObjectOutput/ServerSideEncryption
     :cognitect.aws.s3.PutObjectOutput/ETag
     :cognitect.aws.s3.PutObjectOutput/SSEKMSEncryptionContext
     :cognitect.aws.s3.PutObjectOutput/RequestCharged
     :cognitect.aws.s3.PutObjectOutput/SSEKMSKeyId
     :cognitect.aws.s3.PutObjectOutput/Expiration]))

(s/def
  :cognitect.aws.s3/GetBucketReplicationOutput
  (s/keys :opt-un [:cognitect.aws.s3.GetBucketReplicationOutput/ReplicationConfiguration]))

(s/def
  :cognitect.aws.s3/TopicConfiguration
  (s/keys
    :req-un
    [:cognitect.aws.s3.TopicConfiguration/TopicArn :cognitect.aws.s3.TopicConfiguration/Events]
    :opt-un
    [:cognitect.aws.s3.TopicConfiguration/Filter :cognitect.aws.s3.TopicConfiguration/Id]))

(s/def
  :cognitect.aws.s3/Grant
  (s/keys :opt-un [:cognitect.aws.s3.Grant/Permission :cognitect.aws.s3.Grant/Grantee]))

(s/def :cognitect.aws.s3/UserMetadata (s/coll-of :cognitect.aws.s3/MetadataEntry))

(s/def
  :cognitect.aws.s3/BucketVersioningStatus
  (s/spec string? :gen #(s/gen #{"Suspended" "Enabled"})))

(s/def
  :cognitect.aws.s3/NotificationConfiguration
  (s/keys
    :opt-un
    [:cognitect.aws.s3.NotificationConfiguration/LambdaFunctionConfigurations
     :cognitect.aws.s3.NotificationConfiguration/TopicConfigurations
     :cognitect.aws.s3.NotificationConfiguration/QueueConfigurations]))

(s/def
  :cognitect.aws.s3/RedirectAllRequestsTo
  (s/keys
    :req-un
    [:cognitect.aws.s3.RedirectAllRequestsTo/HostName]
    :opt-un
    [:cognitect.aws.s3.RedirectAllRequestsTo/Protocol]))

(s/def
  :cognitect.aws.s3/CreateBucketConfiguration
  (s/keys :opt-un [:cognitect.aws.s3.CreateBucketConfiguration/LocationConstraint]))

(s/def :cognitect.aws.s3/JSONOutput (s/keys :opt-un [:cognitect.aws.s3.JSONOutput/RecordDelimiter]))

(s/def
  :cognitect.aws.s3/SelectObjectContentOutput
  (s/keys :opt-un [:cognitect.aws.s3.SelectObjectContentOutput/Payload]))

(s/def
  :cognitect.aws.s3/ListBucketMetricsConfigurationsRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.ListBucketMetricsConfigurationsRequest/Bucket]
    :opt-un
    [:cognitect.aws.s3.ListBucketMetricsConfigurationsRequest/ContinuationToken]))

(s/def
  :cognitect.aws.s3/UploadPartCopyRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.UploadPartCopyRequest/Bucket
     :cognitect.aws.s3.UploadPartCopyRequest/CopySource
     :cognitect.aws.s3.UploadPartCopyRequest/Key
     :cognitect.aws.s3.UploadPartCopyRequest/PartNumber
     :cognitect.aws.s3.UploadPartCopyRequest/UploadId]
    :opt-un
    [:cognitect.aws.s3.UploadPartCopyRequest/SSECustomerKeyMD5
     :cognitect.aws.s3.UploadPartCopyRequest/CopySourceSSECustomerKey
     :cognitect.aws.s3.UploadPartCopyRequest/SSECustomerAlgorithm
     :cognitect.aws.s3.UploadPartCopyRequest/SSECustomerKey
     :cognitect.aws.s3.UploadPartCopyRequest/CopySourceIfMatch
     :cognitect.aws.s3.UploadPartCopyRequest/CopySourceRange
     :cognitect.aws.s3.UploadPartCopyRequest/CopySourceIfNoneMatch
     :cognitect.aws.s3.UploadPartCopyRequest/CopySourceIfModifiedSince
     :cognitect.aws.s3.UploadPartCopyRequest/CopySourceIfUnmodifiedSince
     :cognitect.aws.s3.UploadPartCopyRequest/CopySourceSSECustomerAlgorithm
     :cognitect.aws.s3.UploadPartCopyRequest/CopySourceSSECustomerKeyMD5
     :cognitect.aws.s3.UploadPartCopyRequest/RequestPayer]))

(s/def :cognitect.aws.s3/CopySourceIfModifiedSince inst?)

(s/def
  :cognitect.aws.s3/GetObjectOutput
  (s/keys
    :opt-un
    [:cognitect.aws.s3.GetObjectOutput/MissingMeta
     :cognitect.aws.s3.GetObjectOutput/ContentEncoding
     :cognitect.aws.s3.GetObjectOutput/SSECustomerKeyMD5
     :cognitect.aws.s3.GetObjectOutput/WebsiteRedirectLocation
     :cognitect.aws.s3.GetObjectOutput/SSECustomerAlgorithm
     :cognitect.aws.s3.GetObjectOutput/VersionId
     :cognitect.aws.s3.GetObjectOutput/Body
     :cognitect.aws.s3.GetObjectOutput/ContentRange
     :cognitect.aws.s3.GetObjectOutput/AcceptRanges
     :cognitect.aws.s3.GetObjectOutput/ReplicationStatus
     :cognitect.aws.s3.GetObjectOutput/ServerSideEncryption
     :cognitect.aws.s3.GetObjectOutput/ContentDisposition
     :cognitect.aws.s3.GetObjectOutput/ETag
     :cognitect.aws.s3.GetObjectOutput/ContentType
     :cognitect.aws.s3.GetObjectOutput/Restore
     :cognitect.aws.s3.GetObjectOutput/TagCount
     :cognitect.aws.s3.GetObjectOutput/ContentLength
     :cognitect.aws.s3.GetObjectOutput/LastModified
     :cognitect.aws.s3.GetObjectOutput/PartsCount
     :cognitect.aws.s3.GetObjectOutput/ObjectLockLegalHoldStatus
     :cognitect.aws.s3.GetObjectOutput/Metadata
     :cognitect.aws.s3.GetObjectOutput/CacheControl
     :cognitect.aws.s3.GetObjectOutput/ContentLanguage
     :cognitect.aws.s3.GetObjectOutput/StorageClass
     :cognitect.aws.s3.GetObjectOutput/DeleteMarker
     :cognitect.aws.s3.GetObjectOutput/RequestCharged
     :cognitect.aws.s3.GetObjectOutput/Expires
     :cognitect.aws.s3.GetObjectOutput/ObjectLockRetainUntilDate
     :cognitect.aws.s3.GetObjectOutput/SSEKMSKeyId
     :cognitect.aws.s3.GetObjectOutput/ObjectLockMode
     :cognitect.aws.s3.GetObjectOutput/Expiration]))

(s/def
  :cognitect.aws.s3/GetObjectLegalHoldOutput
  (s/keys :opt-un [:cognitect.aws.s3.GetObjectLegalHoldOutput/LegalHold]))

(s/def
  :cognitect.aws.s3/ServerSideEncryptionRule
  (s/keys :opt-un [:cognitect.aws.s3.ServerSideEncryptionRule/ApplyServerSideEncryptionByDefault]))

(s/def
  :cognitect.aws.s3/GetObjectRetentionRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.GetObjectRetentionRequest/Bucket
     :cognitect.aws.s3.GetObjectRetentionRequest/Key]
    :opt-un
    [:cognitect.aws.s3.GetObjectRetentionRequest/VersionId
     :cognitect.aws.s3.GetObjectRetentionRequest/RequestPayer]))

(s/def
  :cognitect.aws.s3/GetBucketPolicyStatusOutput
  (s/keys :opt-un [:cognitect.aws.s3.GetBucketPolicyStatusOutput/PolicyStatus]))

(s/def :cognitect.aws.s3/CommonPrefix (s/keys :opt-un [:cognitect.aws.s3.CommonPrefix/Prefix]))

(s/def
  :cognitect.aws.s3/ObjectIdentifier
  (s/keys
    :req-un
    [:cognitect.aws.s3.ObjectIdentifier/Key]
    :opt-un
    [:cognitect.aws.s3.ObjectIdentifier/VersionId]))

(s/def :cognitect.aws.s3/IsLatest boolean?)

(s/def :cognitect.aws.s3/CloudFunction string?)

(s/def
  :cognitect.aws.s3/DeleteObjectRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.DeleteObjectRequest/Bucket :cognitect.aws.s3.DeleteObjectRequest/Key]
    :opt-un
    [:cognitect.aws.s3.DeleteObjectRequest/VersionId
     :cognitect.aws.s3.DeleteObjectRequest/MFA
     :cognitect.aws.s3.DeleteObjectRequest/BypassGovernanceRetention
     :cognitect.aws.s3.DeleteObjectRequest/RequestPayer]))

(s/def :cognitect.aws.s3/NextToken string?)

(s/def
  :cognitect.aws.s3/ListObjectsOutput
  (s/keys
    :opt-un
    [:cognitect.aws.s3.ListObjectsOutput/Prefix
     :cognitect.aws.s3.ListObjectsOutput/EncodingType
     :cognitect.aws.s3.ListObjectsOutput/Marker
     :cognitect.aws.s3.ListObjectsOutput/IsTruncated
     :cognitect.aws.s3.ListObjectsOutput/CommonPrefixes
     :cognitect.aws.s3.ListObjectsOutput/Contents
     :cognitect.aws.s3.ListObjectsOutput/NextMarker
     :cognitect.aws.s3.ListObjectsOutput/Delimiter
     :cognitect.aws.s3.ListObjectsOutput/Name
     :cognitect.aws.s3.ListObjectsOutput/MaxKeys]))

(s/def :cognitect.aws.s3/EventList (s/coll-of :cognitect.aws.s3/Event))

(s/def
  :cognitect.aws.s3/GetBucketMetricsConfigurationRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.GetBucketMetricsConfigurationRequest/Bucket
     :cognitect.aws.s3.GetBucketMetricsConfigurationRequest/Id]))

(s/def
  :cognitect.aws.s3/CSVOutput
  (s/keys
    :opt-un
    [:cognitect.aws.s3.CSVOutput/RecordDelimiter
     :cognitect.aws.s3.CSVOutput/QuoteCharacter
     :cognitect.aws.s3.CSVOutput/FieldDelimiter
     :cognitect.aws.s3.CSVOutput/QuoteEscapeCharacter
     :cognitect.aws.s3.CSVOutput/QuoteFields]))

(s/def
  :cognitect.aws.s3/Metrics
  (s/keys :req-un [:cognitect.aws.s3.Metrics/Status :cognitect.aws.s3.Metrics/EventThreshold]))

(s/def :cognitect.aws.s3/Payer (s/spec string? :gen #(s/gen #{"Requester" "BucketOwner"})))

(s/def
  :cognitect.aws.s3/BucketLogsPermission
  (s/spec string? :gen #(s/gen #{"WRITE" "FULL_CONTROL" "READ"})))

(s/def
  :cognitect.aws.s3/AnalyticsExportDestination
  (s/keys :req-un [:cognitect.aws.s3.AnalyticsExportDestination/S3BucketDestination]))

(s/def
  :cognitect.aws.s3/DeleteObjectsOutput
  (s/keys
    :opt-un
    [:cognitect.aws.s3.DeleteObjectsOutput/Deleted
     :cognitect.aws.s3.DeleteObjectsOutput/RequestCharged
     :cognitect.aws.s3.DeleteObjectsOutput/Errors]))

(s/def :cognitect.aws.s3/Initiated inst?)

(s/def :cognitect.aws.s3/MaxUploads (s/spec int? :gen #(gen/choose Long/MIN_VALUE Long/MAX_VALUE)))

(s/def :cognitect.aws.s3/KeyMarker string?)

(s/def :cognitect.aws.s3/PartNumber (s/spec int? :gen #(gen/choose Long/MIN_VALUE Long/MAX_VALUE)))

(s/def
  :cognitect.aws.s3/ObjectCannedACL
  (s/spec
    string?
    :gen
    #(s/gen
      #{"authenticated-read" "public-read-write" "bucket-owner-full-control" "private"
        "aws-exec-read" "bucket-owner-read" "public-read"})))

(s/def
  :cognitect.aws.s3/PutBucketReplicationRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.PutBucketReplicationRequest/Bucket
     :cognitect.aws.s3.PutBucketReplicationRequest/ReplicationConfiguration]
    :opt-un
    [:cognitect.aws.s3.PutBucketReplicationRequest/ContentMD5
     :cognitect.aws.s3.PutBucketReplicationRequest/Token]))

(s/def :cognitect.aws.s3/ReplaceKeyPrefixWith string?)

(s/def
  :cognitect.aws.s3/CSVInput
  (s/keys
    :opt-un
    [:cognitect.aws.s3.CSVInput/Comments
     :cognitect.aws.s3.CSVInput/AllowQuotedRecordDelimiter
     :cognitect.aws.s3.CSVInput/RecordDelimiter
     :cognitect.aws.s3.CSVInput/QuoteCharacter
     :cognitect.aws.s3.CSVInput/FieldDelimiter
     :cognitect.aws.s3.CSVInput/QuoteEscapeCharacter
     :cognitect.aws.s3.CSVInput/FileHeaderInfo]))

(s/def :cognitect.aws.s3/TagSet (s/coll-of :cognitect.aws.s3/Tag))

(s/def
  :cognitect.aws.s3/PutObjectAclRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.PutObjectAclRequest/Bucket :cognitect.aws.s3.PutObjectAclRequest/Key]
    :opt-un
    [:cognitect.aws.s3.PutObjectAclRequest/VersionId
     :cognitect.aws.s3.PutObjectAclRequest/ContentMD5
     :cognitect.aws.s3.PutObjectAclRequest/GrantReadACP
     :cognitect.aws.s3.PutObjectAclRequest/AccessControlPolicy
     :cognitect.aws.s3.PutObjectAclRequest/GrantRead
     :cognitect.aws.s3.PutObjectAclRequest/ACL
     :cognitect.aws.s3.PutObjectAclRequest/GrantWrite
     :cognitect.aws.s3.PutObjectAclRequest/GrantWriteACP
     :cognitect.aws.s3.PutObjectAclRequest/GrantFullControl
     :cognitect.aws.s3.PutObjectAclRequest/RequestPayer]))

(s/def
  :cognitect.aws.s3/SelectObjectContentEventStream
  (s/keys
    :opt-un
    [:cognitect.aws.s3.SelectObjectContentEventStream/End
     :cognitect.aws.s3.SelectObjectContentEventStream/Progress
     :cognitect.aws.s3.SelectObjectContentEventStream/Cont
     :cognitect.aws.s3.SelectObjectContentEventStream/Records
     :cognitect.aws.s3.SelectObjectContentEventStream/Stats]))

(s/def :cognitect.aws.s3/CopySourceIfNoneMatch string?)

(s/def :cognitect.aws.s3/Expression string?)

(s/def
  :cognitect.aws.s3/NoncurrentVersionTransitionList
  (s/coll-of :cognitect.aws.s3/NoncurrentVersionTransition))

(s/def
  :cognitect.aws.s3/GetObjectRetentionOutput
  (s/keys :opt-un [:cognitect.aws.s3.GetObjectRetentionOutput/Retention]))

(s/def :cognitect.aws.s3/TransitionList (s/coll-of :cognitect.aws.s3/Transition))

(s/def :cognitect.aws.s3/IfModifiedSince inst?)

(s/def :cognitect.aws.s3/CORSRules (s/coll-of :cognitect.aws.s3/CORSRule))

(s/def
  :cognitect.aws.s3/Body
  (s/or :byte-array bytes? :input-stream #(instance? java.io.InputStream %)))

(s/def :cognitect.aws.s3/ContentDisposition string?)

(s/def :cognitect.aws.s3/MFA string?)

(s/def :cognitect.aws.s3/OutputLocation (s/keys :opt-un [:cognitect.aws.s3.OutputLocation/S3]))

(s/def
  :cognitect.aws.s3/CreateBucketRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.CreateBucketRequest/Bucket]
    :opt-un
    [:cognitect.aws.s3.CreateBucketRequest/CreateBucketConfiguration
     :cognitect.aws.s3.CreateBucketRequest/GrantReadACP
     :cognitect.aws.s3.CreateBucketRequest/GrantRead
     :cognitect.aws.s3.CreateBucketRequest/ACL
     :cognitect.aws.s3.CreateBucketRequest/ObjectLockEnabledForBucket
     :cognitect.aws.s3.CreateBucketRequest/GrantWrite
     :cognitect.aws.s3.CreateBucketRequest/GrantWriteACP
     :cognitect.aws.s3.CreateBucketRequest/GrantFullControl]))

(s/def :cognitect.aws.s3/ConfirmRemoveSelfBucketAccess boolean?)

(s/def
  :cognitect.aws.s3/ObjectLockRule
  (s/keys :opt-un [:cognitect.aws.s3.ObjectLockRule/DefaultRetention]))

(s/def
  :cognitect.aws.s3/PublicAccessBlockConfiguration
  (s/keys
    :opt-un
    [:cognitect.aws.s3.PublicAccessBlockConfiguration/IgnorePublicAcls
     :cognitect.aws.s3.PublicAccessBlockConfiguration/BlockPublicPolicy
     :cognitect.aws.s3.PublicAccessBlockConfiguration/RestrictPublicBuckets
     :cognitect.aws.s3.PublicAccessBlockConfiguration/BlockPublicAcls]))

(s/def :cognitect.aws.s3/Marker string?)

(s/def :cognitect.aws.s3/HttpRedirectCode string?)

(s/def :cognitect.aws.s3/OwnerOverride (s/spec string? :gen #(s/gen #{"Destination"})))

(s/def
  :cognitect.aws.s3/AbortMultipartUploadOutput
  (s/keys :opt-un [:cognitect.aws.s3.AbortMultipartUploadOutput/RequestCharged]))

(s/def :cognitect.aws.s3/Priority (s/spec int? :gen #(gen/choose Long/MIN_VALUE Long/MAX_VALUE)))

(s/def :cognitect.aws.s3/AllowedMethod string?)

(s/def :cognitect.aws.s3/LambdaFunctionArn string?)

(s/def
  :cognitect.aws.s3/AbortMultipartUploadRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.AbortMultipartUploadRequest/Bucket
     :cognitect.aws.s3.AbortMultipartUploadRequest/Key
     :cognitect.aws.s3.AbortMultipartUploadRequest/UploadId]
    :opt-un
    [:cognitect.aws.s3.AbortMultipartUploadRequest/RequestPayer]))

(s/def :cognitect.aws.s3/PolicyStatus (s/keys :opt-un [:cognitect.aws.s3.PolicyStatus/IsPublic]))

(s/def
  :cognitect.aws.s3/BucketAccelerateStatus
  (s/spec string? :gen #(s/gen #{"Suspended" "Enabled"})))

(s/def
  :cognitect.aws.s3/LifecycleExpiration
  (s/keys
    :opt-un
    [:cognitect.aws.s3.LifecycleExpiration/ExpiredObjectDeleteMarker
     :cognitect.aws.s3.LifecycleExpiration/Date
     :cognitect.aws.s3.LifecycleExpiration/Days]))

(s/def
  :cognitect.aws.s3/NotificationConfigurationFilter
  (s/keys :opt-un [:cognitect.aws.s3.NotificationConfigurationFilter/Key]))

(s/def :cognitect.aws.s3/FetchOwner boolean?)

(s/def
  :cognitect.aws.s3/CopyObjectResult
  (s/keys
    :opt-un
    [:cognitect.aws.s3.CopyObjectResult/ETag :cognitect.aws.s3.CopyObjectResult/LastModified]))

(s/def
  :cognitect.aws.s3/ListBucketInventoryConfigurationsOutput
  (s/keys
    :opt-un
    [:cognitect.aws.s3.ListBucketInventoryConfigurationsOutput/InventoryConfigurationList
     :cognitect.aws.s3.ListBucketInventoryConfigurationsOutput/IsTruncated
     :cognitect.aws.s3.ListBucketInventoryConfigurationsOutput/ContinuationToken
     :cognitect.aws.s3.ListBucketInventoryConfigurationsOutput/NextContinuationToken]))

(s/def :cognitect.aws.s3/ObjectVersionId string?)

(s/def
  :cognitect.aws.s3/SSECustomerKey
  (s/or :byte-array bytes? :input-stream #(instance? java.io.InputStream %)))

(s/def
  :cognitect.aws.s3/DeleteBucketEncryptionRequest
  (s/keys :req-un [:cognitect.aws.s3.DeleteBucketEncryptionRequest/Bucket]))

(s/def
  :cognitect.aws.s3/PutBucketPolicyRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.PutBucketPolicyRequest/Bucket
     :cognitect.aws.s3.PutBucketPolicyRequest/Policy]
    :opt-un
    [:cognitect.aws.s3.PutBucketPolicyRequest/ConfirmRemoveSelfBucketAccess
     :cognitect.aws.s3.PutBucketPolicyRequest/ContentMD5]))

(s/def
  :cognitect.aws.s3/Metadata
  (s/map-of :cognitect.aws.s3/MetadataKey :cognitect.aws.s3/MetadataValue))

(s/def
  :cognitect.aws.s3/NextPartNumberMarker
  (s/spec int? :gen #(gen/choose Long/MIN_VALUE Long/MAX_VALUE)))

(s/def :cognitect.aws.s3/ObjectLockToken string?)

(s/def :cognitect.aws.s3/DisplayName string?)

(s/def
  :cognitect.aws.s3/CompletedMultipartUpload
  (s/keys :opt-un [:cognitect.aws.s3.CompletedMultipartUpload/Parts]))

(s/def :cognitect.aws.s3/ObjectVersionList (s/coll-of :cognitect.aws.s3/ObjectVersion))

(s/def :cognitect.aws.s3/SSEKMS (s/keys :req-un [:cognitect.aws.s3.SSEKMS/KeyId]))

(s/def
  :cognitect.aws.s3/ObjectLockConfiguration
  (s/keys
    :opt-un
    [:cognitect.aws.s3.ObjectLockConfiguration/ObjectLockEnabled
     :cognitect.aws.s3.ObjectLockConfiguration/Rule]))

(s/def
  :cognitect.aws.s3/GetPublicAccessBlockOutput
  (s/keys :opt-un [:cognitect.aws.s3.GetPublicAccessBlockOutput/PublicAccessBlockConfiguration]))

(s/def
  :cognitect.aws.s3/BytesProcessed
  (s/spec int? :gen #(gen/choose Long/MIN_VALUE Long/MAX_VALUE)))

(s/def
  :cognitect.aws.s3/PutBucketEncryptionRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.PutBucketEncryptionRequest/Bucket
     :cognitect.aws.s3.PutBucketEncryptionRequest/ServerSideEncryptionConfiguration]
    :opt-un
    [:cognitect.aws.s3.PutBucketEncryptionRequest/ContentMD5]))

(s/def
  :cognitect.aws.s3/LifecycleRuleFilter
  (s/keys
    :opt-un
    [:cognitect.aws.s3.LifecycleRuleFilter/And
     :cognitect.aws.s3.LifecycleRuleFilter/Prefix
     :cognitect.aws.s3.LifecycleRuleFilter/Tag]))

(s/def :cognitect.aws.s3/Range string?)

(s/def
  :cognitect.aws.s3/NotificationConfigurationDeprecated
  (s/keys
    :opt-un
    [:cognitect.aws.s3.NotificationConfigurationDeprecated/CloudFunctionConfiguration
     :cognitect.aws.s3.NotificationConfigurationDeprecated/TopicConfiguration
     :cognitect.aws.s3.NotificationConfigurationDeprecated/QueueConfiguration]))

(s/def :cognitect.aws.s3/RestoreRequestType (s/spec string? :gen #(s/gen #{"SELECT"})))

(s/def
  :cognitect.aws.s3/DaysAfterInitiation
  (s/spec int? :gen #(gen/choose Long/MIN_VALUE Long/MAX_VALUE)))

(s/def
  :cognitect.aws.s3/GetObjectRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.GetObjectRequest/Bucket :cognitect.aws.s3.GetObjectRequest/Key]
    :opt-un
    [:cognitect.aws.s3.GetObjectRequest/SSECustomerKeyMD5
     :cognitect.aws.s3.GetObjectRequest/ResponseExpires
     :cognitect.aws.s3.GetObjectRequest/IfUnmodifiedSince
     :cognitect.aws.s3.GetObjectRequest/ResponseContentDisposition
     :cognitect.aws.s3.GetObjectRequest/ResponseContentLanguage
     :cognitect.aws.s3.GetObjectRequest/SSECustomerAlgorithm
     :cognitect.aws.s3.GetObjectRequest/SSECustomerKey
     :cognitect.aws.s3.GetObjectRequest/VersionId
     :cognitect.aws.s3.GetObjectRequest/ResponseContentType
     :cognitect.aws.s3.GetObjectRequest/IfModifiedSince
     :cognitect.aws.s3.GetObjectRequest/PartNumber
     :cognitect.aws.s3.GetObjectRequest/Range
     :cognitect.aws.s3.GetObjectRequest/IfMatch
     :cognitect.aws.s3.GetObjectRequest/ResponseContentEncoding
     :cognitect.aws.s3.GetObjectRequest/IfNoneMatch
     :cognitect.aws.s3.GetObjectRequest/RequestPayer
     :cognitect.aws.s3.GetObjectRequest/ResponseCacheControl]))

(s/def
  :cognitect.aws.s3/GetBucketLoggingOutput
  (s/keys :opt-un [:cognitect.aws.s3.GetBucketLoggingOutput/LoggingEnabled]))

(s/def
  :cognitect.aws.s3/NoncurrentVersionTransition
  (s/keys
    :opt-un
    [:cognitect.aws.s3.NoncurrentVersionTransition/NoncurrentDays
     :cognitect.aws.s3.NoncurrentVersionTransition/StorageClass]))

(s/def :cognitect.aws.s3/AllowedOrigins (s/coll-of :cognitect.aws.s3/AllowedOrigin))

(s/def :cognitect.aws.s3/TargetBucket string?)

(s/def :cognitect.aws.s3/SSEKMSEncryptionContext string?)

(s/def
  :cognitect.aws.s3/LifecycleConfiguration
  (s/keys :req-un [:cognitect.aws.s3.LifecycleConfiguration/Rules]))

(s/def
  :cognitect.aws.s3/InputSerialization
  (s/keys
    :opt-un
    [:cognitect.aws.s3.InputSerialization/CompressionType
     :cognitect.aws.s3.InputSerialization/CSV
     :cognitect.aws.s3.InputSerialization/Parquet
     :cognitect.aws.s3.InputSerialization/JSON]))

(s/def
  :cognitect.aws.s3/GetObjectTorrentRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.GetObjectTorrentRequest/Bucket
     :cognitect.aws.s3.GetObjectTorrentRequest/Key]
    :opt-un
    [:cognitect.aws.s3.GetObjectTorrentRequest/RequestPayer]))

(s/def :cognitect.aws.s3/BypassGovernanceRetention boolean?)

(s/def
  :cognitect.aws.s3/Transition
  (s/keys
    :opt-un
    [:cognitect.aws.s3.Transition/Date
     :cognitect.aws.s3.Transition/Days
     :cognitect.aws.s3.Transition/StorageClass]))

(s/def :cognitect.aws.s3/AllowedMethods (s/coll-of :cognitect.aws.s3/AllowedMethod))

(s/def
  :cognitect.aws.s3/StorageClassAnalysisDataExport
  (s/keys
    :req-un
    [:cognitect.aws.s3.StorageClassAnalysisDataExport/OutputSchemaVersion
     :cognitect.aws.s3.StorageClassAnalysisDataExport/Destination]))

(s/def
  :cognitect.aws.s3/GetBucketPolicyRequest
  (s/keys :req-un [:cognitect.aws.s3.GetBucketPolicyRequest/Bucket]))

(s/def
  :cognitect.aws.s3/DeletedObject
  (s/keys
    :opt-un
    [:cognitect.aws.s3.DeletedObject/VersionId
     :cognitect.aws.s3.DeletedObject/Key
     :cognitect.aws.s3.DeletedObject/DeleteMarker
     :cognitect.aws.s3.DeletedObject/DeleteMarkerVersionId]))

(s/def
  :cognitect.aws.s3/ReplicationConfiguration
  (s/keys
    :req-un
    [:cognitect.aws.s3.ReplicationConfiguration/Role
     :cognitect.aws.s3.ReplicationConfiguration/Rules]))

(s/def
  :cognitect.aws.s3/AnalyticsS3BucketDestination
  (s/keys
    :req-un
    [:cognitect.aws.s3.AnalyticsS3BucketDestination/Format
     :cognitect.aws.s3.AnalyticsS3BucketDestination/Bucket]
    :opt-un
    [:cognitect.aws.s3.AnalyticsS3BucketDestination/Prefix
     :cognitect.aws.s3.AnalyticsS3BucketDestination/BucketAccountId]))

(s/def :cognitect.aws.s3/IfMatch string?)

(s/def
  :cognitect.aws.s3/BucketLocationConstraint
  (s/spec
    string?
    :gen
    #(s/gen
      #{"ap-northeast-1" "eu-west-1" "ap-southeast-2" "EU" "cn-north-1" "sa-east-1"
        "ap-southeast-1" "eu-central-1" "us-west-2" "us-west-1" "ap-south-1"})))

(s/def :cognitect.aws.s3/URI string?)

(s/def
  :cognitect.aws.s3/PutObjectLockConfigurationRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.PutObjectLockConfigurationRequest/Bucket]
    :opt-un
    [:cognitect.aws.s3.PutObjectLockConfigurationRequest/ContentMD5
     :cognitect.aws.s3.PutObjectLockConfigurationRequest/ObjectLockConfiguration
     :cognitect.aws.s3.PutObjectLockConfigurationRequest/Token
     :cognitect.aws.s3.PutObjectLockConfigurationRequest/RequestPayer]))

(s/def
  :cognitect.aws.s3/Grantee
  (s/keys
    :req-un
    [:cognitect.aws.s3.Grantee/Type]
    :opt-un
    [:cognitect.aws.s3.Grantee/DisplayName
     :cognitect.aws.s3.Grantee/ID
     :cognitect.aws.s3.Grantee/URI
     :cognitect.aws.s3.Grantee/EmailAddress]))

(s/def
  :cognitect.aws.s3/GetObjectLockConfigurationRequest
  (s/keys :req-un [:cognitect.aws.s3.GetObjectLockConfigurationRequest/Bucket]))

(s/def :cognitect.aws.s3/RequestPayer (s/spec string? :gen #(s/gen #{"requester"})))

(s/def :cognitect.aws.s3/ExpiredObjectDeleteMarker boolean?)

(s/def :cognitect.aws.s3/ServerSideEncryption (s/spec string? :gen #(s/gen #{"aws:kms" "AES256"})))

(s/def :cognitect.aws.s3/DeleteMarkerVersionId string?)

(s/def :cognitect.aws.s3/Restore string?)

(s/def
  :cognitect.aws.s3/Delete
  (s/keys :req-un [:cognitect.aws.s3.Delete/Objects] :opt-un [:cognitect.aws.s3.Delete/Quiet]))

(s/def :cognitect.aws.s3/ObjectLockEnabledForBucket boolean?)

(s/def
  :cognitect.aws.s3/MetricsAndOperator
  (s/keys
    :opt-un
    [:cognitect.aws.s3.MetricsAndOperator/Prefix :cognitect.aws.s3.MetricsAndOperator/Tags]))

(s/def
  :cognitect.aws.s3/DeleteBucketCorsRequest
  (s/keys :req-un [:cognitect.aws.s3.DeleteBucketCorsRequest/Bucket]))

(s/def :cognitect.aws.s3/IsPublic boolean?)

(s/def
  :cognitect.aws.s3/BucketLoggingStatus
  (s/keys :opt-un [:cognitect.aws.s3.BucketLoggingStatus/LoggingEnabled]))

(s/def :cognitect.aws.s3/MaxKeys (s/spec int? :gen #(gen/choose Long/MIN_VALUE Long/MAX_VALUE)))

(s/def
  :cognitect.aws.s3/DeleteMarkerReplicationStatus
  (s/spec string? :gen #(s/gen #{"Disabled" "Enabled"})))

(s/def :cognitect.aws.s3/Comments string?)

(s/def
  :cognitect.aws.s3/GetBucketWebsiteRequest
  (s/keys :req-un [:cognitect.aws.s3.GetBucketWebsiteRequest/Bucket]))

(s/def
  :cognitect.aws.s3/ReplicationTimeValue
  (s/keys :opt-un [:cognitect.aws.s3.ReplicationTimeValue/Minutes]))

(s/def
  :cognitect.aws.s3/GetBucketVersioningRequest
  (s/keys :req-un [:cognitect.aws.s3.GetBucketVersioningRequest/Bucket]))

(s/def
  :cognitect.aws.s3/ReplicationRule
  (s/keys
    :req-un
    [:cognitect.aws.s3.ReplicationRule/Status :cognitect.aws.s3.ReplicationRule/Destination]
    :opt-un
    [:cognitect.aws.s3.ReplicationRule/Filter
     :cognitect.aws.s3.ReplicationRule/Prefix
     :cognitect.aws.s3.ReplicationRule/ExistingObjectReplication
     :cognitect.aws.s3.ReplicationRule/SourceSelectionCriteria
     :cognitect.aws.s3.ReplicationRule/Priority
     :cognitect.aws.s3.ReplicationRule/ID
     :cognitect.aws.s3.ReplicationRule/DeleteMarkerReplication]))

(s/def :cognitect.aws.s3/TargetGrants (s/coll-of :cognitect.aws.s3/TargetGrant))

(s/def
  :cognitect.aws.s3/ReplicationRuleStatus
  (s/spec string? :gen #(s/gen #{"Disabled" "Enabled"})))

(s/def :cognitect.aws.s3/CloudFunctionInvocationRole string?)

(s/def :cognitect.aws.s3/ReplicaKmsKeyID string?)

(s/def
  :cognitect.aws.s3/TransitionStorageClass
  (s/spec
    string?
    :gen
    #(s/gen #{"DEEP_ARCHIVE" "GLACIER" "ONEZONE_IA" "STANDARD_IA" "INTELLIGENT_TIERING"})))

(s/def :cognitect.aws.s3/Date inst?)

(s/def
  :cognitect.aws.s3/UploadPartOutput
  (s/keys
    :opt-un
    [:cognitect.aws.s3.UploadPartOutput/SSECustomerKeyMD5
     :cognitect.aws.s3.UploadPartOutput/SSECustomerAlgorithm
     :cognitect.aws.s3.UploadPartOutput/ServerSideEncryption
     :cognitect.aws.s3.UploadPartOutput/ETag
     :cognitect.aws.s3.UploadPartOutput/RequestCharged
     :cognitect.aws.s3.UploadPartOutput/SSEKMSKeyId]))

(s/def :cognitect.aws.s3/IsTruncated boolean?)

(s/def :cognitect.aws.s3/KMSContext string?)

(s/def
  :cognitect.aws.s3/EncryptionConfiguration
  (s/keys :opt-un [:cognitect.aws.s3.EncryptionConfiguration/ReplicaKmsKeyID]))

(s/def
  :cognitect.aws.s3/ExistingObjectReplication
  (s/keys :req-un [:cognitect.aws.s3.ExistingObjectReplication/Status]))

(s/def :cognitect.aws.s3/EndEvent (s/keys))

(s/def :cognitect.aws.s3/NextUploadIdMarker string?)

(s/def
  :cognitect.aws.s3/DeleteBucketRequest
  (s/keys :req-un [:cognitect.aws.s3.DeleteBucketRequest/Bucket]))

(s/def
  :cognitect.aws.s3/PutBucketLifecycleRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.PutBucketLifecycleRequest/Bucket]
    :opt-un
    [:cognitect.aws.s3.PutBucketLifecycleRequest/ContentMD5
     :cognitect.aws.s3.PutBucketLifecycleRequest/LifecycleConfiguration]))

(s/def
  :cognitect.aws.s3/GetBucketLifecycleOutput
  (s/keys :opt-un [:cognitect.aws.s3.GetBucketLifecycleOutput/Rules]))

(s/def :cognitect.aws.s3/MultipartUploadId string?)

(s/def :cognitect.aws.s3/AllowedHeaders (s/coll-of :cognitect.aws.s3/AllowedHeader))

(s/def :cognitect.aws.s3/Code string?)

(s/def
  :cognitect.aws.s3/DeleteBucketLifecycleRequest
  (s/keys :req-un [:cognitect.aws.s3.DeleteBucketLifecycleRequest/Bucket]))

(s/def
  :cognitect.aws.s3/PutBucketAnalyticsConfigurationRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.PutBucketAnalyticsConfigurationRequest/Bucket
     :cognitect.aws.s3.PutBucketAnalyticsConfigurationRequest/Id
     :cognitect.aws.s3.PutBucketAnalyticsConfigurationRequest/AnalyticsConfiguration]))

(s/def
  :cognitect.aws.s3/LifecycleRule
  (s/keys
    :req-un
    [:cognitect.aws.s3.LifecycleRule/Status]
    :opt-un
    [:cognitect.aws.s3.LifecycleRule/Filter
     :cognitect.aws.s3.LifecycleRule/Prefix
     :cognitect.aws.s3.LifecycleRule/Transitions
     :cognitect.aws.s3.LifecycleRule/ID
     :cognitect.aws.s3.LifecycleRule/NoncurrentVersionExpiration
     :cognitect.aws.s3.LifecycleRule/AbortIncompleteMultipartUpload
     :cognitect.aws.s3.LifecycleRule/NoncurrentVersionTransitions
     :cognitect.aws.s3.LifecycleRule/Expiration]))

(s/def
  :cognitect.aws.s3/GetBucketAclOutput
  (s/keys
    :opt-un
    [:cognitect.aws.s3.GetBucketAclOutput/Grants :cognitect.aws.s3.GetBucketAclOutput/Owner]))

(s/def :cognitect.aws.s3/SSES3 (s/keys))

(s/def
  :cognitect.aws.s3/ObjectLockLegalHold
  (s/keys :opt-un [:cognitect.aws.s3.ObjectLockLegalHold/Status]))

(s/def
  :cognitect.aws.s3/DeleteBucketWebsiteRequest
  (s/keys :req-un [:cognitect.aws.s3.DeleteBucketWebsiteRequest/Bucket]))

(s/def
  :cognitect.aws.s3/ListObjectVersionsRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.ListObjectVersionsRequest/Bucket]
    :opt-un
    [:cognitect.aws.s3.ListObjectVersionsRequest/Prefix
     :cognitect.aws.s3.ListObjectVersionsRequest/EncodingType
     :cognitect.aws.s3.ListObjectVersionsRequest/VersionIdMarker
     :cognitect.aws.s3.ListObjectVersionsRequest/Delimiter
     :cognitect.aws.s3.ListObjectVersionsRequest/MaxKeys
     :cognitect.aws.s3.ListObjectVersionsRequest/KeyMarker]))

(s/def
  :cognitect.aws.s3/ContentLength
  (s/spec int? :gen #(gen/choose Long/MIN_VALUE Long/MAX_VALUE)))

(s/def
  :cognitect.aws.s3/GetBucketTaggingRequest
  (s/keys :req-un [:cognitect.aws.s3.GetBucketTaggingRequest/Bucket]))

(s/def
  :cognitect.aws.s3/PutObjectRetentionOutput
  (s/keys :opt-un [:cognitect.aws.s3.PutObjectRetentionOutput/RequestCharged]))

(s/def :cognitect.aws.s3/AnalyticsId string?)

(s/def :cognitect.aws.s3/DeleteMarker boolean?)

(s/def
  :cognitect.aws.s3/CreateMultipartUploadRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.CreateMultipartUploadRequest/Bucket
     :cognitect.aws.s3.CreateMultipartUploadRequest/Key]
    :opt-un
    [:cognitect.aws.s3.CreateMultipartUploadRequest/ContentEncoding
     :cognitect.aws.s3.CreateMultipartUploadRequest/SSECustomerKeyMD5
     :cognitect.aws.s3.CreateMultipartUploadRequest/WebsiteRedirectLocation
     :cognitect.aws.s3.CreateMultipartUploadRequest/SSECustomerAlgorithm
     :cognitect.aws.s3.CreateMultipartUploadRequest/SSECustomerKey
     :cognitect.aws.s3.CreateMultipartUploadRequest/GrantReadACP
     :cognitect.aws.s3.CreateMultipartUploadRequest/GrantRead
     :cognitect.aws.s3.CreateMultipartUploadRequest/ServerSideEncryption
     :cognitect.aws.s3.CreateMultipartUploadRequest/ContentDisposition
     :cognitect.aws.s3.CreateMultipartUploadRequest/ContentType
     :cognitect.aws.s3.CreateMultipartUploadRequest/ACL
     :cognitect.aws.s3.CreateMultipartUploadRequest/ObjectLockLegalHoldStatus
     :cognitect.aws.s3.CreateMultipartUploadRequest/Metadata
     :cognitect.aws.s3.CreateMultipartUploadRequest/CacheControl
     :cognitect.aws.s3.CreateMultipartUploadRequest/ContentLanguage
     :cognitect.aws.s3.CreateMultipartUploadRequest/GrantWriteACP
     :cognitect.aws.s3.CreateMultipartUploadRequest/GrantFullControl
     :cognitect.aws.s3.CreateMultipartUploadRequest/Tagging
     :cognitect.aws.s3.CreateMultipartUploadRequest/SSEKMSEncryptionContext
     :cognitect.aws.s3.CreateMultipartUploadRequest/StorageClass
     :cognitect.aws.s3.CreateMultipartUploadRequest/Expires
     :cognitect.aws.s3.CreateMultipartUploadRequest/ObjectLockRetainUntilDate
     :cognitect.aws.s3.CreateMultipartUploadRequest/SSEKMSKeyId
     :cognitect.aws.s3.CreateMultipartUploadRequest/ObjectLockMode
     :cognitect.aws.s3.CreateMultipartUploadRequest/RequestPayer]))

(s/def :cognitect.aws.s3/NextMarker string?)

(s/def
  :cognitect.aws.s3/GetBucketInventoryConfigurationRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.GetBucketInventoryConfigurationRequest/Bucket
     :cognitect.aws.s3.GetBucketInventoryConfigurationRequest/Id]))

(s/def :cognitect.aws.s3/InventoryFrequency (s/spec string? :gen #(s/gen #{"Daily" "Weekly"})))

(s/def :cognitect.aws.s3/Value string?)

(s/def
  :cognitect.aws.s3/DeleteObjectsRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.DeleteObjectsRequest/Bucket :cognitect.aws.s3.DeleteObjectsRequest/Delete]
    :opt-un
    [:cognitect.aws.s3.DeleteObjectsRequest/MFA
     :cognitect.aws.s3.DeleteObjectsRequest/BypassGovernanceRetention
     :cognitect.aws.s3.DeleteObjectsRequest/RequestPayer]))

(s/def
  :cognitect.aws.s3/PartNumberMarker
  (s/spec int? :gen #(gen/choose Long/MIN_VALUE Long/MAX_VALUE)))

(s/def :cognitect.aws.s3/FieldDelimiter string?)

(s/def
  :cognitect.aws.s3/DeleteBucketPolicyRequest
  (s/keys :req-un [:cognitect.aws.s3.DeleteBucketPolicyRequest/Bucket]))

(s/def
  :cognitect.aws.s3/ExistingObjectReplicationStatus
  (s/spec string? :gen #(s/gen #{"Disabled" "Enabled"})))

(s/def :cognitect.aws.s3/BucketName string?)

(s/def :cognitect.aws.s3/ResponseContentEncoding string?)

(s/def
  :cognitect.aws.s3/CORSConfiguration
  (s/keys :req-un [:cognitect.aws.s3.CORSConfiguration/CORSRules]))

(s/def
  :cognitect.aws.s3/SelectParameters
  (s/keys
    :req-un
    [:cognitect.aws.s3.SelectParameters/InputSerialization
     :cognitect.aws.s3.SelectParameters/ExpressionType
     :cognitect.aws.s3.SelectParameters/Expression
     :cognitect.aws.s3.SelectParameters/OutputSerialization]))

(s/def
  :cognitect.aws.s3/ListMultipartUploadsRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.ListMultipartUploadsRequest/Bucket]
    :opt-un
    [:cognitect.aws.s3.ListMultipartUploadsRequest/Prefix
     :cognitect.aws.s3.ListMultipartUploadsRequest/EncodingType
     :cognitect.aws.s3.ListMultipartUploadsRequest/UploadIdMarker
     :cognitect.aws.s3.ListMultipartUploadsRequest/MaxUploads
     :cognitect.aws.s3.ListMultipartUploadsRequest/Delimiter
     :cognitect.aws.s3.ListMultipartUploadsRequest/KeyMarker]))

(s/def :cognitect.aws.s3/QuoteFields (s/spec string? :gen #(s/gen #{"ALWAYS" "ASNEEDED"})))

(s/def :cognitect.aws.s3/SSECustomerAlgorithm string?)

(s/def
  :cognitect.aws.s3/GetObjectLockConfigurationOutput
  (s/keys :opt-un [:cognitect.aws.s3.GetObjectLockConfigurationOutput/ObjectLockConfiguration]))

(s/def :cognitect.aws.s3/CacheControl string?)

(s/def :cognitect.aws.s3/LifecycleRules (s/coll-of :cognitect.aws.s3/LifecycleRule))

(s/def :cognitect.aws.s3/HttpErrorCodeReturnedEquals string?)

(s/def
  :cognitect.aws.s3/OutputSerialization
  (s/keys
    :opt-un
    [:cognitect.aws.s3.OutputSerialization/CSV :cognitect.aws.s3.OutputSerialization/JSON]))

(s/def
  :cognitect.aws.s3/MetricsFilter
  (s/keys
    :opt-un
    [:cognitect.aws.s3.MetricsFilter/And
     :cognitect.aws.s3.MetricsFilter/Prefix
     :cognitect.aws.s3.MetricsFilter/Tag]))

(s/def :cognitect.aws.s3/Description string?)

(s/def :cognitect.aws.s3/Quiet boolean?)

(s/def
  :cognitect.aws.s3/InventoryOptionalFields
  (s/coll-of :cognitect.aws.s3/InventoryOptionalField))

(s/def
  :cognitect.aws.s3/LifecycleRuleAndOperator
  (s/keys
    :opt-un
    [:cognitect.aws.s3.LifecycleRuleAndOperator/Prefix
     :cognitect.aws.s3.LifecycleRuleAndOperator/Tags]))

(s/def
  :cognitect.aws.s3/ListPartsRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.ListPartsRequest/Bucket
     :cognitect.aws.s3.ListPartsRequest/Key
     :cognitect.aws.s3.ListPartsRequest/UploadId]
    :opt-un
    [:cognitect.aws.s3.ListPartsRequest/MaxParts
     :cognitect.aws.s3.ListPartsRequest/PartNumberMarker
     :cognitect.aws.s3.ListPartsRequest/RequestPayer]))

(s/def
  :cognitect.aws.s3/Type
  (s/spec string? :gen #(s/gen #{"CanonicalUser" "AmazonCustomerByEmail" "Group"})))

(s/def
  :cognitect.aws.s3/PutObjectRetentionRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.PutObjectRetentionRequest/Bucket
     :cognitect.aws.s3.PutObjectRetentionRequest/Key]
    :opt-un
    [:cognitect.aws.s3.PutObjectRetentionRequest/VersionId
     :cognitect.aws.s3.PutObjectRetentionRequest/ContentMD5
     :cognitect.aws.s3.PutObjectRetentionRequest/BypassGovernanceRetention
     :cognitect.aws.s3.PutObjectRetentionRequest/Retention
     :cognitect.aws.s3.PutObjectRetentionRequest/RequestPayer]))

(s/def
  :cognitect.aws.s3/SelectObjectContentRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.SelectObjectContentRequest/Bucket
     :cognitect.aws.s3.SelectObjectContentRequest/Key
     :cognitect.aws.s3.SelectObjectContentRequest/Expression
     :cognitect.aws.s3.SelectObjectContentRequest/ExpressionType
     :cognitect.aws.s3.SelectObjectContentRequest/InputSerialization
     :cognitect.aws.s3.SelectObjectContentRequest/OutputSerialization]
    :opt-un
    [:cognitect.aws.s3.SelectObjectContentRequest/SSECustomerKeyMD5
     :cognitect.aws.s3.SelectObjectContentRequest/SSECustomerAlgorithm
     :cognitect.aws.s3.SelectObjectContentRequest/SSECustomerKey
     :cognitect.aws.s3.SelectObjectContentRequest/ScanRange
     :cognitect.aws.s3.SelectObjectContentRequest/RequestProgress]))

(s/def
  :cognitect.aws.s3/DeleteObjectTaggingOutput
  (s/keys :opt-un [:cognitect.aws.s3.DeleteObjectTaggingOutput/VersionId]))

(s/def :cognitect.aws.s3/ExposeHeaders (s/coll-of :cognitect.aws.s3/ExposeHeader))

(s/def :cognitect.aws.s3/End (s/spec int? :gen #(gen/choose Long/MIN_VALUE Long/MAX_VALUE)))

(s/def
  :cognitect.aws.s3/InventoryDestination
  (s/keys :req-un [:cognitect.aws.s3.InventoryDestination/S3BucketDestination]))

(s/def :cognitect.aws.s3/InventoryId string?)

(s/def :cognitect.aws.s3/AnalyticsS3ExportFileFormat (s/spec string? :gen #(s/gen #{"CSV"})))

(s/def :cognitect.aws.s3/KeyCount (s/spec int? :gen #(gen/choose Long/MIN_VALUE Long/MAX_VALUE)))

(s/def
  :cognitect.aws.s3/GetBucketLocationOutput
  (s/keys :opt-un [:cognitect.aws.s3.GetBucketLocationOutput/LocationConstraint]))

(s/def
  :cognitect.aws.s3/GetObjectTaggingRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.GetObjectTaggingRequest/Bucket
     :cognitect.aws.s3.GetObjectTaggingRequest/Key]
    :opt-un
    [:cognitect.aws.s3.GetObjectTaggingRequest/VersionId]))

(s/def
  :cognitect.aws.s3/UploadPartRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.UploadPartRequest/Bucket
     :cognitect.aws.s3.UploadPartRequest/Key
     :cognitect.aws.s3.UploadPartRequest/PartNumber
     :cognitect.aws.s3.UploadPartRequest/UploadId]
    :opt-un
    [:cognitect.aws.s3.UploadPartRequest/SSECustomerKeyMD5
     :cognitect.aws.s3.UploadPartRequest/SSECustomerAlgorithm
     :cognitect.aws.s3.UploadPartRequest/SSECustomerKey
     :cognitect.aws.s3.UploadPartRequest/ContentMD5
     :cognitect.aws.s3.UploadPartRequest/Body
     :cognitect.aws.s3.UploadPartRequest/ContentLength
     :cognitect.aws.s3.UploadPartRequest/RequestPayer]))

(s/def
  :cognitect.aws.s3/HeadObjectOutput
  (s/keys
    :opt-un
    [:cognitect.aws.s3.HeadObjectOutput/MissingMeta
     :cognitect.aws.s3.HeadObjectOutput/ContentEncoding
     :cognitect.aws.s3.HeadObjectOutput/SSECustomerKeyMD5
     :cognitect.aws.s3.HeadObjectOutput/WebsiteRedirectLocation
     :cognitect.aws.s3.HeadObjectOutput/SSECustomerAlgorithm
     :cognitect.aws.s3.HeadObjectOutput/VersionId
     :cognitect.aws.s3.HeadObjectOutput/AcceptRanges
     :cognitect.aws.s3.HeadObjectOutput/ReplicationStatus
     :cognitect.aws.s3.HeadObjectOutput/ServerSideEncryption
     :cognitect.aws.s3.HeadObjectOutput/ContentDisposition
     :cognitect.aws.s3.HeadObjectOutput/ETag
     :cognitect.aws.s3.HeadObjectOutput/ContentType
     :cognitect.aws.s3.HeadObjectOutput/Restore
     :cognitect.aws.s3.HeadObjectOutput/ContentLength
     :cognitect.aws.s3.HeadObjectOutput/LastModified
     :cognitect.aws.s3.HeadObjectOutput/PartsCount
     :cognitect.aws.s3.HeadObjectOutput/ObjectLockLegalHoldStatus
     :cognitect.aws.s3.HeadObjectOutput/Metadata
     :cognitect.aws.s3.HeadObjectOutput/CacheControl
     :cognitect.aws.s3.HeadObjectOutput/ContentLanguage
     :cognitect.aws.s3.HeadObjectOutput/StorageClass
     :cognitect.aws.s3.HeadObjectOutput/DeleteMarker
     :cognitect.aws.s3.HeadObjectOutput/RequestCharged
     :cognitect.aws.s3.HeadObjectOutput/Expires
     :cognitect.aws.s3.HeadObjectOutput/ObjectLockRetainUntilDate
     :cognitect.aws.s3.HeadObjectOutput/SSEKMSKeyId
     :cognitect.aws.s3.HeadObjectOutput/ObjectLockMode
     :cognitect.aws.s3.HeadObjectOutput/Expiration]))

(s/def :cognitect.aws.s3/TaggingDirective (s/spec string? :gen #(s/gen #{"COPY" "REPLACE"})))

(s/def :cognitect.aws.s3/Protocol (s/spec string? :gen #(s/gen #{"https" "http"})))

(s/def
  :cognitect.aws.s3/Error
  (s/keys
    :opt-un
    [:cognitect.aws.s3.Error/VersionId
     :cognitect.aws.s3.Error/Key
     :cognitect.aws.s3.Error/Message
     :cognitect.aws.s3.Error/Code]))

(s/def
  :cognitect.aws.s3/PutBucketLoggingRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.PutBucketLoggingRequest/Bucket
     :cognitect.aws.s3.PutBucketLoggingRequest/BucketLoggingStatus]
    :opt-un
    [:cognitect.aws.s3.PutBucketLoggingRequest/ContentMD5]))

(s/def
  :cognitect.aws.s3/DeleteBucketReplicationRequest
  (s/keys :req-un [:cognitect.aws.s3.DeleteBucketReplicationRequest/Bucket]))

(s/def :cognitect.aws.s3/ResponseCacheControl string?)

(s/def
  :cognitect.aws.s3/GetBucketLifecycleConfigurationRequest
  (s/keys :req-un [:cognitect.aws.s3.GetBucketLifecycleConfigurationRequest/Bucket]))

(s/def :cognitect.aws.s3/ContentType string?)

(s/def :cognitect.aws.s3/StorageClassAnalysisSchemaVersion (s/spec string? :gen #(s/gen #{"V_1"})))

(s/def :cognitect.aws.s3/AcceptRanges string?)

(s/def :cognitect.aws.s3/CopySourceIfMatch string?)

(s/def
  :cognitect.aws.s3/AnalyticsConfiguration
  (s/keys
    :req-un
    [:cognitect.aws.s3.AnalyticsConfiguration/Id
     :cognitect.aws.s3.AnalyticsConfiguration/StorageClassAnalysis]
    :opt-un
    [:cognitect.aws.s3.AnalyticsConfiguration/Filter]))

(s/def :cognitect.aws.s3/ObjectLockRetainUntilDate inst?)

(s/def
  :cognitect.aws.s3/Condition
  (s/keys
    :opt-un
    [:cognitect.aws.s3.Condition/HttpErrorCodeReturnedEquals
     :cognitect.aws.s3.Condition/KeyPrefixEquals]))

(s/def :cognitect.aws.s3/Role string?)

(s/def
  :cognitect.aws.s3/PutObjectLockConfigurationOutput
  (s/keys :opt-un [:cognitect.aws.s3.PutObjectLockConfigurationOutput/RequestCharged]))

(s/def :cognitect.aws.s3/MetricsId string?)

(s/def
  :cognitect.aws.s3/CopyPartResult
  (s/keys
    :opt-un
    [:cognitect.aws.s3.CopyPartResult/ETag :cognitect.aws.s3.CopyPartResult/LastModified]))

(s/def :cognitect.aws.s3/ExpirationStatus (s/spec string? :gen #(s/gen #{"Disabled" "Enabled"})))

(s/def
  :cognitect.aws.s3/Progress
  (s/keys
    :opt-un
    [:cognitect.aws.s3.Progress/BytesReturned
     :cognitect.aws.s3.Progress/BytesScanned
     :cognitect.aws.s3.Progress/BytesProcessed]))

(s/def
  :cognitect.aws.s3/GetBucketPolicyOutput
  (s/keys :opt-un [:cognitect.aws.s3.GetBucketPolicyOutput/Policy]))

(s/def :cognitect.aws.s3/HostName string?)

(s/def
  :cognitect.aws.s3/ListBucketAnalyticsConfigurationsOutput
  (s/keys
    :opt-un
    [:cognitect.aws.s3.ListBucketAnalyticsConfigurationsOutput/IsTruncated
     :cognitect.aws.s3.ListBucketAnalyticsConfigurationsOutput/ContinuationToken
     :cognitect.aws.s3.ListBucketAnalyticsConfigurationsOutput/NextContinuationToken
     :cognitect.aws.s3.ListBucketAnalyticsConfigurationsOutput/AnalyticsConfigurationList]))

(s/def :cognitect.aws.s3/CommonPrefixList (s/coll-of :cognitect.aws.s3/CommonPrefix))

(s/def
  :cognitect.aws.s3/PutObjectLegalHoldRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.PutObjectLegalHoldRequest/Bucket
     :cognitect.aws.s3.PutObjectLegalHoldRequest/Key]
    :opt-un
    [:cognitect.aws.s3.PutObjectLegalHoldRequest/VersionId
     :cognitect.aws.s3.PutObjectLegalHoldRequest/ContentMD5
     :cognitect.aws.s3.PutObjectLegalHoldRequest/LegalHold
     :cognitect.aws.s3.PutObjectLegalHoldRequest/RequestPayer]))

(s/def
  :cognitect.aws.s3/AnalyticsAndOperator
  (s/keys
    :opt-un
    [:cognitect.aws.s3.AnalyticsAndOperator/Prefix :cognitect.aws.s3.AnalyticsAndOperator/Tags]))

(s/def :cognitect.aws.s3/GrantFullControl string?)

(s/def
  :cognitect.aws.s3/Object
  (s/keys
    :opt-un
    [:cognitect.aws.s3.Object/ETag
     :cognitect.aws.s3.Object/Key
     :cognitect.aws.s3.Object/LastModified
     :cognitect.aws.s3.Object/Size
     :cognitect.aws.s3.Object/Owner
     :cognitect.aws.s3.Object/StorageClass]))

(s/def :cognitect.aws.s3/DeletedObjects (s/coll-of :cognitect.aws.s3/DeletedObject))

(s/def :cognitect.aws.s3/ResponseExpires inst?)

(s/def
  :cognitect.aws.s3/NoncurrentVersionExpiration
  (s/keys :opt-un [:cognitect.aws.s3.NoncurrentVersionExpiration/NoncurrentDays]))

(s/def :cognitect.aws.s3/ErrorDocument (s/keys :req-un [:cognitect.aws.s3.ErrorDocument/Key]))

(s/def
  :cognitect.aws.s3/ListObjectsV2Output
  (s/keys
    :opt-un
    [:cognitect.aws.s3.ListObjectsV2Output/KeyCount
     :cognitect.aws.s3.ListObjectsV2Output/Prefix
     :cognitect.aws.s3.ListObjectsV2Output/EncodingType
     :cognitect.aws.s3.ListObjectsV2Output/StartAfter
     :cognitect.aws.s3.ListObjectsV2Output/IsTruncated
     :cognitect.aws.s3.ListObjectsV2Output/CommonPrefixes
     :cognitect.aws.s3.ListObjectsV2Output/Contents
     :cognitect.aws.s3.ListObjectsV2Output/ContinuationToken
     :cognitect.aws.s3.ListObjectsV2Output/Delimiter
     :cognitect.aws.s3.ListObjectsV2Output/NextContinuationToken
     :cognitect.aws.s3.ListObjectsV2Output/Name
     :cognitect.aws.s3.ListObjectsV2Output/MaxKeys]))

(s/def
  :cognitect.aws.s3/MultipartUpload
  (s/keys
    :opt-un
    [:cognitect.aws.s3.MultipartUpload/Key
     :cognitect.aws.s3.MultipartUpload/Initiated
     :cognitect.aws.s3.MultipartUpload/Owner
     :cognitect.aws.s3.MultipartUpload/UploadId
     :cognitect.aws.s3.MultipartUpload/StorageClass
     :cognitect.aws.s3.MultipartUpload/Initiator]))

(s/def
  :cognitect.aws.s3/PutObjectRequest
  (s/keys
    :req-un
    [:cognitect.aws.s3.PutObjectRequest/Bucket :cognitect.aws.s3.PutObjectRequest/Key]
    :opt-un
    [:cognitect.aws.s3.PutObjectRequest/ContentEncoding
     :cognitect.aws.s3.PutObjectRequest/SSECustomerKeyMD5
     :cognitect.aws.s3.PutObjectRequest/WebsiteRedirectLocation
     :cognitect.aws.s3.PutObjectRequest/SSECustomerAlgorithm
     :cognitect.aws.s3.PutObjectRequest/SSECustomerKey
     :cognitect.aws.s3.PutObjectRequest/ContentMD5
     :cognitect.aws.s3.PutObjectRequest/Body
     :cognitect.aws.s3.PutObjectRequest/GrantReadACP
     :cognitect.aws.s3.PutObjectRequest/GrantRead
     :cognitect.aws.s3.PutObjectRequest/ServerSideEncryption
     :cognitect.aws.s3.PutObjectRequest/ContentDisposition
     :cognitect.aws.s3.PutObjectRequest/ContentType
     :cognitect.aws.s3.PutObjectRequest/ACL
     :cognitect.aws.s3.PutObjectRequest/ContentLength
     :cognitect.aws.s3.PutObjectRequest/ObjectLockLegalHoldStatus
     :cognitect.aws.s3.PutObjectRequest/Metadata
     :cognitect.aws.s3.PutObjectRequest/CacheControl
     :cognitect.aws.s3.PutObjectRequest/ContentLanguage
     :cognitect.aws.s3.PutObjectRequest/GrantWriteACP
     :cognitect.aws.s3.PutObjectRequest/GrantFullControl
     :cognitect.aws.s3.PutObjectRequest/Tagging
     :cognitect.aws.s3.PutObjectRequest/SSEKMSEncryptionContext
     :cognitect.aws.s3.PutObjectRequest/StorageClass
     :cognitect.aws.s3.PutObjectRequest/Expires
     :cognitect.aws.s3.PutObjectRequest/ObjectLockRetainUntilDate
     :cognitect.aws.s3.PutObjectRequest/SSEKMSKeyId
     :cognitect.aws.s3.PutObjectRequest/ObjectLockMode
     :cognitect.aws.s3.PutObjectRequest/RequestPayer]))

(s/def
  :cognitect.aws.s3/RestoreRequest
  (s/keys
    :opt-un
    [:cognitect.aws.s3.RestoreRequest/Tier
     :cognitect.aws.s3.RestoreRequest/GlacierJobParameters
     :cognitect.aws.s3.RestoreRequest/Type
     :cognitect.aws.s3.RestoreRequest/Description
     :cognitect.aws.s3.RestoreRequest/SelectParameters
     :cognitect.aws.s3.RestoreRequest/Days
     :cognitect.aws.s3.RestoreRequest/OutputLocation]))

(s/def :cognitect.aws.s3/Location string?)

(s/def :cognitect.aws.s3/FileHeaderInfo (s/spec string? :gen #(s/gen #{"USE" "NONE" "IGNORE"})))

(s/def :cognitect.aws.s3/SSEKMSKeyId string?)

(s/def :cognitect.aws.s3/WebsiteRedirectLocation string?)
